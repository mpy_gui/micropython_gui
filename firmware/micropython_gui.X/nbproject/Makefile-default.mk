#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/micropython_gui.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/micropython_gui.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/system_config/default/bsp/bsp.c ../src/system_config/default/framework/gfx/driver/controller/glcd/src/drv_gfx_glcd_static.c ../src/system_config/default/framework/gfx/driver/processor/nano2d/libnano2D_hal.c ../src/system_config/default/framework/gfx/hal/gfx_display_def.c ../src/system_config/default/framework/gfx/hal/gfx_driver_def.c ../src/system_config/default/framework/gfx/hal/gfx_processor_def.c ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/default/framework/system/memory/ddr/src/sys_memory_ddr_static.c ../src/system_config/default/framework/system/memory/src/sys_memory_static.c ../src/system_config/default/framework/system/ports/src/sys_ports_static.c ../src/system_config/default/system_init.c ../src/system_config/default/system_interrupt.c ../src/system_config/default/system_exceptions.c ../src/system_config/default/system_tasks.c ../src/system_config/default/system_interrupt_a.S ../src/system_config/default/rtos_hooks.c ../src/app.c ../src/main.c ../src/mymodule.c ../src/testmodule.c ../src/hwapp.c ../src/modtouch.c ../src/modio.c ../src/modsys.c ../src/modutime.c ../src/modgfx.c ../src/moddemo.c ../src/modtest.c extmod/virtpin.c extmod/moduselect.c extmod/modutimeq.c extmod/utime_mphal.c ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c ../../../../framework/driver/sdhc/src/drv_sdhc.c ../../../../framework/driver/sdhc/src/drv_sdhc_host.c ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../framework/driver/touch/mtch6301/src/drv_mtch6301.c ../../../../framework/driver/usart/src/dynamic/drv_usart.c ../../../../framework/driver/usart/src/dynamic/drv_usart_read_write.c ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c ../../../../framework/gfx/hal/src/gfx.c ../../../../framework/gfx/hal/src/gfx_color.c ../../../../framework/gfx/hal/src/gfx_context.c ../../../../framework/gfx/hal/src/gfx_default_impl.c ../../../../framework/gfx/hal/src/gfx_display.c ../../../../framework/gfx/hal/src/gfx_driver_interface.c ../../../../framework/gfx/hal/src/gfx_get.c ../../../../framework/gfx/hal/src/gfx_interface.c ../../../../framework/gfx/hal/src/gfx_layer.c ../../../../framework/gfx/hal/src/gfx_pixel_buffer.c ../../../../framework/gfx/hal/src/gfx_processor_interface.c ../../../../framework/gfx/hal/src/gfx_rect.c ../../../../framework/gfx/hal/src/gfx_util.c ../../../../framework/gfx/hal/src/gfx_set.c ../../../../framework/gfx/hal/src/gfx_color_blend.c ../../../../framework/gfx/hal/src/gfx_color_convert.c ../../../../framework/gfx/hal/src/gfx_color_lerp.c ../../../../framework/gfx/hal/src/gfx_color_value.c ../../../../framework/gfx/hal/src/gfx_draw_blit.c ../../../../framework/gfx/hal/src/gfx_draw_circle.c ../../../../framework/gfx/hal/src/gfx_draw_line.c ../../../../framework/gfx/hal/src/gfx_draw_pixel.c ../../../../framework/gfx/hal/src/gfx_draw_rect.c ../../../../framework/gfx/hal/src/gfx_draw_stretchblit.c ../../../../framework/gfx/hal/src/gfx_math.c ../../../../framework/osal/src/osal_freertos.c ../../../../framework/system/console/src/sys_console.c ../../../../framework/system/console/src/sys_console_usb_cdc.c ../../../../framework/system/dma/src/sys_dma.c ../../../../framework/system/fs/src/dynamic/sys_fs.c ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c ../../../../framework/system/fs/fat_fs/src/file_system/ff.c ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c ../../../../framework/system/int/src/sys_int_pic32.c ../../../../framework/system/tmr/src/sys_tmr.c ../../../../framework/system/touch/src/sys_touch.c ../../../../framework/usb/src/dynamic/usb_device.c ../../../../framework/usb/src/dynamic/usb_device_cdc.c ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c ../../../../third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_3.c ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port.c ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port_asm.S ../../../../third_party/rtos/FreeRTOS/Source/croutine.c ../../../../third_party/rtos/FreeRTOS/Source/list.c ../../../../third_party/rtos/FreeRTOS/Source/queue.c ../../../../third_party/rtos/FreeRTOS/Source/tasks.c ../../../../third_party/rtos/FreeRTOS/Source/timers.c ../../../../third_party/rtos/FreeRTOS/Source/event_groups.c lib/libc/string0.c lib/mp-readline/readline.c lib/utils/pyexec.c lib/utils/stdout_helpers.c py/argcheck.c py/asmbase.c py/bc.c py/binary.c py/builtinevex.c py/builtinimport.c py/compile.c py/emitbc.c py/emitcommon.c py/emitglue.c py/frozenmod.c py/gc.c py/lexer.c py/malloc.c py/map.c py/modbuiltins.c py/modmicropython.c py/moduerrno.c py/mpprint.c py/mpstate.c py/mpz.c py/nlrsetjmp.c py/obj.c py/objbool.c py/objboundmeth.c py/objcell.c py/objclosure.c py/objdict.c py/objexcept.c py/objfun.c py/objgenerator.c py/objgetitemiter.c py/objint.c py/objlist.c py/objmap.c py/objmodule.c py/objnone.c py/objobject.c py/objpolyiter.c py/objrange.c py/objsingleton.c py/objstr.c py/objtuple.c py/objtype.c py/objzip.c py/opmethods.c py/parse.c py/parsenum.c py/parsenumbase.c py/qstr.c py/reader.c py/repl.c py/runtime.c py/scope.c py/sequence.c py/smallint.c py/stackctrl.c py/stream.c py/unicode.c py/vm.c py/vstr.c py/modgc.c py/emitnative.c py/runtime_utils.c py/objslice.c py/objarray.c py/modarray.c py/objint_longlong.c uart_core.c _mon_getc.c read.c sbrk.c ../src/dlsbrk.c ../src/dlmalloc.c ../src/sd_card_exec.c _frozen_mpy.c ../src/logbuffer.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/74298950/bsp.o ${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o ${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o ${OBJECTDIR}/_ext/2065038297/gfx_display_def.o ${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o ${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o ${OBJECTDIR}/_ext/1541531744/sys_memory_static.o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ${OBJECTDIR}/_ext/1688732426/system_init.o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o ${OBJECTDIR}/_ext/1688732426/rtos_hooks.o ${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/mymodule.o ${OBJECTDIR}/_ext/1360937237/testmodule.o ${OBJECTDIR}/_ext/1360937237/hwapp.o ${OBJECTDIR}/_ext/1360937237/modtouch.o ${OBJECTDIR}/_ext/1360937237/modio.o ${OBJECTDIR}/_ext/1360937237/modsys.o ${OBJECTDIR}/_ext/1360937237/modutime.o ${OBJECTDIR}/_ext/1360937237/modgfx.o ${OBJECTDIR}/_ext/1360937237/moddemo.o ${OBJECTDIR}/_ext/1360937237/modtest.o ${OBJECTDIR}/extmod/virtpin.o ${OBJECTDIR}/extmod/moduselect.o ${OBJECTDIR}/extmod/modutimeq.o ${OBJECTDIR}/extmod/utime_mphal.o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ${OBJECTDIR}/_ext/507552489/drv_sdhc.o ${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o ${OBJECTDIR}/_ext/260586732/drv_usart.o ${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o ${OBJECTDIR}/_ext/246898221/drv_usbhs.o ${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o ${OBJECTDIR}/_ext/88308223/gfx.o ${OBJECTDIR}/_ext/88308223/gfx_color.o ${OBJECTDIR}/_ext/88308223/gfx_context.o ${OBJECTDIR}/_ext/88308223/gfx_default_impl.o ${OBJECTDIR}/_ext/88308223/gfx_display.o ${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o ${OBJECTDIR}/_ext/88308223/gfx_get.o ${OBJECTDIR}/_ext/88308223/gfx_interface.o ${OBJECTDIR}/_ext/88308223/gfx_layer.o ${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o ${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o ${OBJECTDIR}/_ext/88308223/gfx_rect.o ${OBJECTDIR}/_ext/88308223/gfx_util.o ${OBJECTDIR}/_ext/88308223/gfx_set.o ${OBJECTDIR}/_ext/88308223/gfx_color_blend.o ${OBJECTDIR}/_ext/88308223/gfx_color_convert.o ${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o ${OBJECTDIR}/_ext/88308223/gfx_color_value.o ${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o ${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o ${OBJECTDIR}/_ext/88308223/gfx_draw_line.o ${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o ${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o ${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o ${OBJECTDIR}/_ext/88308223/gfx_math.o ${OBJECTDIR}/_ext/24337685/osal_freertos.o ${OBJECTDIR}/_ext/30809027/sys_console.o ${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o ${OBJECTDIR}/_ext/65930274/sys_dma.o ${OBJECTDIR}/_ext/2104899551/sys_fs.o ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o ${OBJECTDIR}/_ext/66287330/ff.o ${OBJECTDIR}/_ext/2072869785/diskio.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ${OBJECTDIR}/_ext/1264926591/sys_tmr.o ${OBJECTDIR}/_ext/846513563/sys_touch.o ${OBJECTDIR}/_ext/610166344/usb_device.o ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o ${OBJECTDIR}/_ext/1856876499/heap_3.o ${OBJECTDIR}/_ext/12131620/port.o ${OBJECTDIR}/_ext/12131620/port_asm.o ${OBJECTDIR}/_ext/1128951024/croutine.o ${OBJECTDIR}/_ext/1128951024/list.o ${OBJECTDIR}/_ext/1128951024/queue.o ${OBJECTDIR}/_ext/1128951024/tasks.o ${OBJECTDIR}/_ext/1128951024/timers.o ${OBJECTDIR}/_ext/1128951024/event_groups.o ${OBJECTDIR}/lib/libc/string0.o ${OBJECTDIR}/lib/mp-readline/readline.o ${OBJECTDIR}/lib/utils/pyexec.o ${OBJECTDIR}/lib/utils/stdout_helpers.o ${OBJECTDIR}/py/argcheck.o ${OBJECTDIR}/py/asmbase.o ${OBJECTDIR}/py/bc.o ${OBJECTDIR}/py/binary.o ${OBJECTDIR}/py/builtinevex.o ${OBJECTDIR}/py/builtinimport.o ${OBJECTDIR}/py/compile.o ${OBJECTDIR}/py/emitbc.o ${OBJECTDIR}/py/emitcommon.o ${OBJECTDIR}/py/emitglue.o ${OBJECTDIR}/py/frozenmod.o ${OBJECTDIR}/py/gc.o ${OBJECTDIR}/py/lexer.o ${OBJECTDIR}/py/malloc.o ${OBJECTDIR}/py/map.o ${OBJECTDIR}/py/modbuiltins.o ${OBJECTDIR}/py/modmicropython.o ${OBJECTDIR}/py/moduerrno.o ${OBJECTDIR}/py/mpprint.o ${OBJECTDIR}/py/mpstate.o ${OBJECTDIR}/py/mpz.o ${OBJECTDIR}/py/nlrsetjmp.o ${OBJECTDIR}/py/obj.o ${OBJECTDIR}/py/objbool.o ${OBJECTDIR}/py/objboundmeth.o ${OBJECTDIR}/py/objcell.o ${OBJECTDIR}/py/objclosure.o ${OBJECTDIR}/py/objdict.o ${OBJECTDIR}/py/objexcept.o ${OBJECTDIR}/py/objfun.o ${OBJECTDIR}/py/objgenerator.o ${OBJECTDIR}/py/objgetitemiter.o ${OBJECTDIR}/py/objint.o ${OBJECTDIR}/py/objlist.o ${OBJECTDIR}/py/objmap.o ${OBJECTDIR}/py/objmodule.o ${OBJECTDIR}/py/objnone.o ${OBJECTDIR}/py/objobject.o ${OBJECTDIR}/py/objpolyiter.o ${OBJECTDIR}/py/objrange.o ${OBJECTDIR}/py/objsingleton.o ${OBJECTDIR}/py/objstr.o ${OBJECTDIR}/py/objtuple.o ${OBJECTDIR}/py/objtype.o ${OBJECTDIR}/py/objzip.o ${OBJECTDIR}/py/opmethods.o ${OBJECTDIR}/py/parse.o ${OBJECTDIR}/py/parsenum.o ${OBJECTDIR}/py/parsenumbase.o ${OBJECTDIR}/py/qstr.o ${OBJECTDIR}/py/reader.o ${OBJECTDIR}/py/repl.o ${OBJECTDIR}/py/runtime.o ${OBJECTDIR}/py/scope.o ${OBJECTDIR}/py/sequence.o ${OBJECTDIR}/py/smallint.o ${OBJECTDIR}/py/stackctrl.o ${OBJECTDIR}/py/stream.o ${OBJECTDIR}/py/unicode.o ${OBJECTDIR}/py/vm.o ${OBJECTDIR}/py/vstr.o ${OBJECTDIR}/py/modgc.o ${OBJECTDIR}/py/emitnative.o ${OBJECTDIR}/py/runtime_utils.o ${OBJECTDIR}/py/objslice.o ${OBJECTDIR}/py/objarray.o ${OBJECTDIR}/py/modarray.o ${OBJECTDIR}/py/objint_longlong.o ${OBJECTDIR}/uart_core.o ${OBJECTDIR}/_mon_getc.o ${OBJECTDIR}/read.o ${OBJECTDIR}/sbrk.o ${OBJECTDIR}/_ext/1360937237/dlsbrk.o ${OBJECTDIR}/_ext/1360937237/dlmalloc.o ${OBJECTDIR}/_ext/1360937237/sd_card_exec.o ${OBJECTDIR}/_frozen_mpy.o ${OBJECTDIR}/_ext/1360937237/logbuffer.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/74298950/bsp.o.d ${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o.d ${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o.d ${OBJECTDIR}/_ext/2065038297/gfx_display_def.o.d ${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o.d ${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o.d ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d ${OBJECTDIR}/_ext/340578644/sys_devcon.o.d ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d ${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o.d ${OBJECTDIR}/_ext/1541531744/sys_memory_static.o.d ${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d ${OBJECTDIR}/_ext/1688732426/system_init.o.d ${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d ${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d ${OBJECTDIR}/_ext/1688732426/system_tasks.o.d ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.d ${OBJECTDIR}/_ext/1688732426/rtos_hooks.o.d ${OBJECTDIR}/_ext/1360937237/app.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1360937237/mymodule.o.d ${OBJECTDIR}/_ext/1360937237/testmodule.o.d ${OBJECTDIR}/_ext/1360937237/hwapp.o.d ${OBJECTDIR}/_ext/1360937237/modtouch.o.d ${OBJECTDIR}/_ext/1360937237/modio.o.d ${OBJECTDIR}/_ext/1360937237/modsys.o.d ${OBJECTDIR}/_ext/1360937237/modutime.o.d ${OBJECTDIR}/_ext/1360937237/modgfx.o.d ${OBJECTDIR}/_ext/1360937237/moddemo.o.d ${OBJECTDIR}/_ext/1360937237/modtest.o.d ${OBJECTDIR}/extmod/virtpin.o.d ${OBJECTDIR}/extmod/moduselect.o.d ${OBJECTDIR}/extmod/modutimeq.o.d ${OBJECTDIR}/extmod/utime_mphal.o.d ${OBJECTDIR}/_ext/280795049/drv_i2c.o.d ${OBJECTDIR}/_ext/507552489/drv_sdhc.o.d ${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o.d ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d ${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o.d ${OBJECTDIR}/_ext/260586732/drv_usart.o.d ${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o.d ${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d ${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o.d ${OBJECTDIR}/_ext/88308223/gfx.o.d ${OBJECTDIR}/_ext/88308223/gfx_color.o.d ${OBJECTDIR}/_ext/88308223/gfx_context.o.d ${OBJECTDIR}/_ext/88308223/gfx_default_impl.o.d ${OBJECTDIR}/_ext/88308223/gfx_display.o.d ${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o.d ${OBJECTDIR}/_ext/88308223/gfx_get.o.d ${OBJECTDIR}/_ext/88308223/gfx_interface.o.d ${OBJECTDIR}/_ext/88308223/gfx_layer.o.d ${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o.d ${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o.d ${OBJECTDIR}/_ext/88308223/gfx_rect.o.d ${OBJECTDIR}/_ext/88308223/gfx_util.o.d ${OBJECTDIR}/_ext/88308223/gfx_set.o.d ${OBJECTDIR}/_ext/88308223/gfx_color_blend.o.d ${OBJECTDIR}/_ext/88308223/gfx_color_convert.o.d ${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o.d ${OBJECTDIR}/_ext/88308223/gfx_color_value.o.d ${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o.d ${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o.d ${OBJECTDIR}/_ext/88308223/gfx_draw_line.o.d ${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o.d ${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o.d ${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o.d ${OBJECTDIR}/_ext/88308223/gfx_math.o.d ${OBJECTDIR}/_ext/24337685/osal_freertos.o.d ${OBJECTDIR}/_ext/30809027/sys_console.o.d ${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o.d ${OBJECTDIR}/_ext/65930274/sys_dma.o.d ${OBJECTDIR}/_ext/2104899551/sys_fs.o.d ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d ${OBJECTDIR}/_ext/66287330/ff.o.d ${OBJECTDIR}/_ext/2072869785/diskio.o.d ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d ${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d ${OBJECTDIR}/_ext/846513563/sys_touch.o.d ${OBJECTDIR}/_ext/610166344/usb_device.o.d ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d ${OBJECTDIR}/_ext/1856876499/heap_3.o.d ${OBJECTDIR}/_ext/12131620/port.o.d ${OBJECTDIR}/_ext/12131620/port_asm.o.d ${OBJECTDIR}/_ext/1128951024/croutine.o.d ${OBJECTDIR}/_ext/1128951024/list.o.d ${OBJECTDIR}/_ext/1128951024/queue.o.d ${OBJECTDIR}/_ext/1128951024/tasks.o.d ${OBJECTDIR}/_ext/1128951024/timers.o.d ${OBJECTDIR}/_ext/1128951024/event_groups.o.d ${OBJECTDIR}/lib/libc/string0.o.d ${OBJECTDIR}/lib/mp-readline/readline.o.d ${OBJECTDIR}/lib/utils/pyexec.o.d ${OBJECTDIR}/lib/utils/stdout_helpers.o.d ${OBJECTDIR}/py/argcheck.o.d ${OBJECTDIR}/py/asmbase.o.d ${OBJECTDIR}/py/bc.o.d ${OBJECTDIR}/py/binary.o.d ${OBJECTDIR}/py/builtinevex.o.d ${OBJECTDIR}/py/builtinimport.o.d ${OBJECTDIR}/py/compile.o.d ${OBJECTDIR}/py/emitbc.o.d ${OBJECTDIR}/py/emitcommon.o.d ${OBJECTDIR}/py/emitglue.o.d ${OBJECTDIR}/py/frozenmod.o.d ${OBJECTDIR}/py/gc.o.d ${OBJECTDIR}/py/lexer.o.d ${OBJECTDIR}/py/malloc.o.d ${OBJECTDIR}/py/map.o.d ${OBJECTDIR}/py/modbuiltins.o.d ${OBJECTDIR}/py/modmicropython.o.d ${OBJECTDIR}/py/moduerrno.o.d ${OBJECTDIR}/py/mpprint.o.d ${OBJECTDIR}/py/mpstate.o.d ${OBJECTDIR}/py/mpz.o.d ${OBJECTDIR}/py/nlrsetjmp.o.d ${OBJECTDIR}/py/obj.o.d ${OBJECTDIR}/py/objbool.o.d ${OBJECTDIR}/py/objboundmeth.o.d ${OBJECTDIR}/py/objcell.o.d ${OBJECTDIR}/py/objclosure.o.d ${OBJECTDIR}/py/objdict.o.d ${OBJECTDIR}/py/objexcept.o.d ${OBJECTDIR}/py/objfun.o.d ${OBJECTDIR}/py/objgenerator.o.d ${OBJECTDIR}/py/objgetitemiter.o.d ${OBJECTDIR}/py/objint.o.d ${OBJECTDIR}/py/objlist.o.d ${OBJECTDIR}/py/objmap.o.d ${OBJECTDIR}/py/objmodule.o.d ${OBJECTDIR}/py/objnone.o.d ${OBJECTDIR}/py/objobject.o.d ${OBJECTDIR}/py/objpolyiter.o.d ${OBJECTDIR}/py/objrange.o.d ${OBJECTDIR}/py/objsingleton.o.d ${OBJECTDIR}/py/objstr.o.d ${OBJECTDIR}/py/objtuple.o.d ${OBJECTDIR}/py/objtype.o.d ${OBJECTDIR}/py/objzip.o.d ${OBJECTDIR}/py/opmethods.o.d ${OBJECTDIR}/py/parse.o.d ${OBJECTDIR}/py/parsenum.o.d ${OBJECTDIR}/py/parsenumbase.o.d ${OBJECTDIR}/py/qstr.o.d ${OBJECTDIR}/py/reader.o.d ${OBJECTDIR}/py/repl.o.d ${OBJECTDIR}/py/runtime.o.d ${OBJECTDIR}/py/scope.o.d ${OBJECTDIR}/py/sequence.o.d ${OBJECTDIR}/py/smallint.o.d ${OBJECTDIR}/py/stackctrl.o.d ${OBJECTDIR}/py/stream.o.d ${OBJECTDIR}/py/unicode.o.d ${OBJECTDIR}/py/vm.o.d ${OBJECTDIR}/py/vstr.o.d ${OBJECTDIR}/py/modgc.o.d ${OBJECTDIR}/py/emitnative.o.d ${OBJECTDIR}/py/runtime_utils.o.d ${OBJECTDIR}/py/objslice.o.d ${OBJECTDIR}/py/objarray.o.d ${OBJECTDIR}/py/modarray.o.d ${OBJECTDIR}/py/objint_longlong.o.d ${OBJECTDIR}/uart_core.o.d ${OBJECTDIR}/_mon_getc.o.d ${OBJECTDIR}/read.o.d ${OBJECTDIR}/sbrk.o.d ${OBJECTDIR}/_ext/1360937237/dlsbrk.o.d ${OBJECTDIR}/_ext/1360937237/dlmalloc.o.d ${OBJECTDIR}/_ext/1360937237/sd_card_exec.o.d ${OBJECTDIR}/_frozen_mpy.o.d ${OBJECTDIR}/_ext/1360937237/logbuffer.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/74298950/bsp.o ${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o ${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o ${OBJECTDIR}/_ext/2065038297/gfx_display_def.o ${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o ${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o ${OBJECTDIR}/_ext/1541531744/sys_memory_static.o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ${OBJECTDIR}/_ext/1688732426/system_init.o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o ${OBJECTDIR}/_ext/1688732426/rtos_hooks.o ${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/mymodule.o ${OBJECTDIR}/_ext/1360937237/testmodule.o ${OBJECTDIR}/_ext/1360937237/hwapp.o ${OBJECTDIR}/_ext/1360937237/modtouch.o ${OBJECTDIR}/_ext/1360937237/modio.o ${OBJECTDIR}/_ext/1360937237/modsys.o ${OBJECTDIR}/_ext/1360937237/modutime.o ${OBJECTDIR}/_ext/1360937237/modgfx.o ${OBJECTDIR}/_ext/1360937237/moddemo.o ${OBJECTDIR}/_ext/1360937237/modtest.o ${OBJECTDIR}/extmod/virtpin.o ${OBJECTDIR}/extmod/moduselect.o ${OBJECTDIR}/extmod/modutimeq.o ${OBJECTDIR}/extmod/utime_mphal.o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ${OBJECTDIR}/_ext/507552489/drv_sdhc.o ${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o ${OBJECTDIR}/_ext/260586732/drv_usart.o ${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o ${OBJECTDIR}/_ext/246898221/drv_usbhs.o ${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o ${OBJECTDIR}/_ext/88308223/gfx.o ${OBJECTDIR}/_ext/88308223/gfx_color.o ${OBJECTDIR}/_ext/88308223/gfx_context.o ${OBJECTDIR}/_ext/88308223/gfx_default_impl.o ${OBJECTDIR}/_ext/88308223/gfx_display.o ${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o ${OBJECTDIR}/_ext/88308223/gfx_get.o ${OBJECTDIR}/_ext/88308223/gfx_interface.o ${OBJECTDIR}/_ext/88308223/gfx_layer.o ${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o ${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o ${OBJECTDIR}/_ext/88308223/gfx_rect.o ${OBJECTDIR}/_ext/88308223/gfx_util.o ${OBJECTDIR}/_ext/88308223/gfx_set.o ${OBJECTDIR}/_ext/88308223/gfx_color_blend.o ${OBJECTDIR}/_ext/88308223/gfx_color_convert.o ${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o ${OBJECTDIR}/_ext/88308223/gfx_color_value.o ${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o ${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o ${OBJECTDIR}/_ext/88308223/gfx_draw_line.o ${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o ${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o ${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o ${OBJECTDIR}/_ext/88308223/gfx_math.o ${OBJECTDIR}/_ext/24337685/osal_freertos.o ${OBJECTDIR}/_ext/30809027/sys_console.o ${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o ${OBJECTDIR}/_ext/65930274/sys_dma.o ${OBJECTDIR}/_ext/2104899551/sys_fs.o ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o ${OBJECTDIR}/_ext/66287330/ff.o ${OBJECTDIR}/_ext/2072869785/diskio.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ${OBJECTDIR}/_ext/1264926591/sys_tmr.o ${OBJECTDIR}/_ext/846513563/sys_touch.o ${OBJECTDIR}/_ext/610166344/usb_device.o ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o ${OBJECTDIR}/_ext/1856876499/heap_3.o ${OBJECTDIR}/_ext/12131620/port.o ${OBJECTDIR}/_ext/12131620/port_asm.o ${OBJECTDIR}/_ext/1128951024/croutine.o ${OBJECTDIR}/_ext/1128951024/list.o ${OBJECTDIR}/_ext/1128951024/queue.o ${OBJECTDIR}/_ext/1128951024/tasks.o ${OBJECTDIR}/_ext/1128951024/timers.o ${OBJECTDIR}/_ext/1128951024/event_groups.o ${OBJECTDIR}/lib/libc/string0.o ${OBJECTDIR}/lib/mp-readline/readline.o ${OBJECTDIR}/lib/utils/pyexec.o ${OBJECTDIR}/lib/utils/stdout_helpers.o ${OBJECTDIR}/py/argcheck.o ${OBJECTDIR}/py/asmbase.o ${OBJECTDIR}/py/bc.o ${OBJECTDIR}/py/binary.o ${OBJECTDIR}/py/builtinevex.o ${OBJECTDIR}/py/builtinimport.o ${OBJECTDIR}/py/compile.o ${OBJECTDIR}/py/emitbc.o ${OBJECTDIR}/py/emitcommon.o ${OBJECTDIR}/py/emitglue.o ${OBJECTDIR}/py/frozenmod.o ${OBJECTDIR}/py/gc.o ${OBJECTDIR}/py/lexer.o ${OBJECTDIR}/py/malloc.o ${OBJECTDIR}/py/map.o ${OBJECTDIR}/py/modbuiltins.o ${OBJECTDIR}/py/modmicropython.o ${OBJECTDIR}/py/moduerrno.o ${OBJECTDIR}/py/mpprint.o ${OBJECTDIR}/py/mpstate.o ${OBJECTDIR}/py/mpz.o ${OBJECTDIR}/py/nlrsetjmp.o ${OBJECTDIR}/py/obj.o ${OBJECTDIR}/py/objbool.o ${OBJECTDIR}/py/objboundmeth.o ${OBJECTDIR}/py/objcell.o ${OBJECTDIR}/py/objclosure.o ${OBJECTDIR}/py/objdict.o ${OBJECTDIR}/py/objexcept.o ${OBJECTDIR}/py/objfun.o ${OBJECTDIR}/py/objgenerator.o ${OBJECTDIR}/py/objgetitemiter.o ${OBJECTDIR}/py/objint.o ${OBJECTDIR}/py/objlist.o ${OBJECTDIR}/py/objmap.o ${OBJECTDIR}/py/objmodule.o ${OBJECTDIR}/py/objnone.o ${OBJECTDIR}/py/objobject.o ${OBJECTDIR}/py/objpolyiter.o ${OBJECTDIR}/py/objrange.o ${OBJECTDIR}/py/objsingleton.o ${OBJECTDIR}/py/objstr.o ${OBJECTDIR}/py/objtuple.o ${OBJECTDIR}/py/objtype.o ${OBJECTDIR}/py/objzip.o ${OBJECTDIR}/py/opmethods.o ${OBJECTDIR}/py/parse.o ${OBJECTDIR}/py/parsenum.o ${OBJECTDIR}/py/parsenumbase.o ${OBJECTDIR}/py/qstr.o ${OBJECTDIR}/py/reader.o ${OBJECTDIR}/py/repl.o ${OBJECTDIR}/py/runtime.o ${OBJECTDIR}/py/scope.o ${OBJECTDIR}/py/sequence.o ${OBJECTDIR}/py/smallint.o ${OBJECTDIR}/py/stackctrl.o ${OBJECTDIR}/py/stream.o ${OBJECTDIR}/py/unicode.o ${OBJECTDIR}/py/vm.o ${OBJECTDIR}/py/vstr.o ${OBJECTDIR}/py/modgc.o ${OBJECTDIR}/py/emitnative.o ${OBJECTDIR}/py/runtime_utils.o ${OBJECTDIR}/py/objslice.o ${OBJECTDIR}/py/objarray.o ${OBJECTDIR}/py/modarray.o ${OBJECTDIR}/py/objint_longlong.o ${OBJECTDIR}/uart_core.o ${OBJECTDIR}/_mon_getc.o ${OBJECTDIR}/read.o ${OBJECTDIR}/sbrk.o ${OBJECTDIR}/_ext/1360937237/dlsbrk.o ${OBJECTDIR}/_ext/1360937237/dlmalloc.o ${OBJECTDIR}/_ext/1360937237/sd_card_exec.o ${OBJECTDIR}/_frozen_mpy.o ${OBJECTDIR}/_ext/1360937237/logbuffer.o

# Source Files
SOURCEFILES=../src/system_config/default/bsp/bsp.c ../src/system_config/default/framework/gfx/driver/controller/glcd/src/drv_gfx_glcd_static.c ../src/system_config/default/framework/gfx/driver/processor/nano2d/libnano2D_hal.c ../src/system_config/default/framework/gfx/hal/gfx_display_def.c ../src/system_config/default/framework/gfx/hal/gfx_driver_def.c ../src/system_config/default/framework/gfx/hal/gfx_processor_def.c ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/default/framework/system/memory/ddr/src/sys_memory_ddr_static.c ../src/system_config/default/framework/system/memory/src/sys_memory_static.c ../src/system_config/default/framework/system/ports/src/sys_ports_static.c ../src/system_config/default/system_init.c ../src/system_config/default/system_interrupt.c ../src/system_config/default/system_exceptions.c ../src/system_config/default/system_tasks.c ../src/system_config/default/system_interrupt_a.S ../src/system_config/default/rtos_hooks.c ../src/app.c ../src/main.c ../src/mymodule.c ../src/testmodule.c ../src/hwapp.c ../src/modtouch.c ../src/modio.c ../src/modsys.c ../src/modutime.c ../src/modgfx.c ../src/moddemo.c ../src/modtest.c extmod/virtpin.c extmod/moduselect.c extmod/modutimeq.c extmod/utime_mphal.c ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c ../../../../framework/driver/sdhc/src/drv_sdhc.c ../../../../framework/driver/sdhc/src/drv_sdhc_host.c ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../framework/driver/touch/mtch6301/src/drv_mtch6301.c ../../../../framework/driver/usart/src/dynamic/drv_usart.c ../../../../framework/driver/usart/src/dynamic/drv_usart_read_write.c ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c ../../../../framework/gfx/hal/src/gfx.c ../../../../framework/gfx/hal/src/gfx_color.c ../../../../framework/gfx/hal/src/gfx_context.c ../../../../framework/gfx/hal/src/gfx_default_impl.c ../../../../framework/gfx/hal/src/gfx_display.c ../../../../framework/gfx/hal/src/gfx_driver_interface.c ../../../../framework/gfx/hal/src/gfx_get.c ../../../../framework/gfx/hal/src/gfx_interface.c ../../../../framework/gfx/hal/src/gfx_layer.c ../../../../framework/gfx/hal/src/gfx_pixel_buffer.c ../../../../framework/gfx/hal/src/gfx_processor_interface.c ../../../../framework/gfx/hal/src/gfx_rect.c ../../../../framework/gfx/hal/src/gfx_util.c ../../../../framework/gfx/hal/src/gfx_set.c ../../../../framework/gfx/hal/src/gfx_color_blend.c ../../../../framework/gfx/hal/src/gfx_color_convert.c ../../../../framework/gfx/hal/src/gfx_color_lerp.c ../../../../framework/gfx/hal/src/gfx_color_value.c ../../../../framework/gfx/hal/src/gfx_draw_blit.c ../../../../framework/gfx/hal/src/gfx_draw_circle.c ../../../../framework/gfx/hal/src/gfx_draw_line.c ../../../../framework/gfx/hal/src/gfx_draw_pixel.c ../../../../framework/gfx/hal/src/gfx_draw_rect.c ../../../../framework/gfx/hal/src/gfx_draw_stretchblit.c ../../../../framework/gfx/hal/src/gfx_math.c ../../../../framework/osal/src/osal_freertos.c ../../../../framework/system/console/src/sys_console.c ../../../../framework/system/console/src/sys_console_usb_cdc.c ../../../../framework/system/dma/src/sys_dma.c ../../../../framework/system/fs/src/dynamic/sys_fs.c ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c ../../../../framework/system/fs/fat_fs/src/file_system/ff.c ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c ../../../../framework/system/int/src/sys_int_pic32.c ../../../../framework/system/tmr/src/sys_tmr.c ../../../../framework/system/touch/src/sys_touch.c ../../../../framework/usb/src/dynamic/usb_device.c ../../../../framework/usb/src/dynamic/usb_device_cdc.c ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c ../../../../third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_3.c ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port.c ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port_asm.S ../../../../third_party/rtos/FreeRTOS/Source/croutine.c ../../../../third_party/rtos/FreeRTOS/Source/list.c ../../../../third_party/rtos/FreeRTOS/Source/queue.c ../../../../third_party/rtos/FreeRTOS/Source/tasks.c ../../../../third_party/rtos/FreeRTOS/Source/timers.c ../../../../third_party/rtos/FreeRTOS/Source/event_groups.c lib/libc/string0.c lib/mp-readline/readline.c lib/utils/pyexec.c lib/utils/stdout_helpers.c py/argcheck.c py/asmbase.c py/bc.c py/binary.c py/builtinevex.c py/builtinimport.c py/compile.c py/emitbc.c py/emitcommon.c py/emitglue.c py/frozenmod.c py/gc.c py/lexer.c py/malloc.c py/map.c py/modbuiltins.c py/modmicropython.c py/moduerrno.c py/mpprint.c py/mpstate.c py/mpz.c py/nlrsetjmp.c py/obj.c py/objbool.c py/objboundmeth.c py/objcell.c py/objclosure.c py/objdict.c py/objexcept.c py/objfun.c py/objgenerator.c py/objgetitemiter.c py/objint.c py/objlist.c py/objmap.c py/objmodule.c py/objnone.c py/objobject.c py/objpolyiter.c py/objrange.c py/objsingleton.c py/objstr.c py/objtuple.c py/objtype.c py/objzip.c py/opmethods.c py/parse.c py/parsenum.c py/parsenumbase.c py/qstr.c py/reader.c py/repl.c py/runtime.c py/scope.c py/sequence.c py/smallint.c py/stackctrl.c py/stream.c py/unicode.c py/vm.c py/vstr.c py/modgc.c py/emitnative.c py/runtime_utils.c py/objslice.c py/objarray.c py/modarray.c py/objint_longlong.c uart_core.c _mon_getc.c read.c sbrk.c ../src/dlsbrk.c ../src/dlmalloc.c ../src/sd_card_exec.c _frozen_mpy.c ../src/logbuffer.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/micropython_gui.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MZ2064DAG169
MP_LINKER_FILE_OPTION=,--script="linkerscript/p32MZ2064DAG169mod.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default" -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_default=$(CND_CONF)  -legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"." -I"inc",-I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default"
	
${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o: ../src/system_config/default/system_interrupt_a.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.ok ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.d" "${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.d"  -o ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o ../src/system_config/default/system_interrupt_a.S  -DXPRJ_default=$(CND_CONF)  -legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"." -I"inc",-I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default"
	
${OBJECTDIR}/_ext/12131620/port_asm.o: ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port_asm.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/12131620" 
	@${RM} ${OBJECTDIR}/_ext/12131620/port_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/12131620/port_asm.o 
	@${RM} ${OBJECTDIR}/_ext/12131620/port_asm.o.ok ${OBJECTDIR}/_ext/12131620/port_asm.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12131620/port_asm.o.d" "${OBJECTDIR}/_ext/12131620/port_asm.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default" -MMD -MF "${OBJECTDIR}/_ext/12131620/port_asm.o.d"  -o ${OBJECTDIR}/_ext/12131620/port_asm.o ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port_asm.S  -DXPRJ_default=$(CND_CONF)  -legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/12131620/port_asm.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"." -I"inc",-I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default"
	
else
${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default" -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_default=$(CND_CONF)  -legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d",--gdwarf-2 -I"." -I"inc",-I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default"
	
${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o: ../src/system_config/default/system_interrupt_a.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.ok ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.d" "${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.d"  -o ${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o ../src/system_config/default/system_interrupt_a.S  -DXPRJ_default=$(CND_CONF)  -legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1688732426/system_interrupt_a.o.asm.d",--gdwarf-2 -I"." -I"inc",-I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default"
	
${OBJECTDIR}/_ext/12131620/port_asm.o: ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port_asm.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/12131620" 
	@${RM} ${OBJECTDIR}/_ext/12131620/port_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/12131620/port_asm.o 
	@${RM} ${OBJECTDIR}/_ext/12131620/port_asm.o.ok ${OBJECTDIR}/_ext/12131620/port_asm.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12131620/port_asm.o.d" "${OBJECTDIR}/_ext/12131620/port_asm.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default" -MMD -MF "${OBJECTDIR}/_ext/12131620/port_asm.o.d"  -o ${OBJECTDIR}/_ext/12131620/port_asm.o ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port_asm.S  -DXPRJ_default=$(CND_CONF)  -legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/12131620/port_asm.o.asm.d",--gdwarf-2 -I"." -I"inc",-I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/default"
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/74298950/bsp.o: ../src/system_config/default/bsp/bsp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/74298950" 
	@${RM} ${OBJECTDIR}/_ext/74298950/bsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/74298950/bsp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/74298950/bsp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/74298950/bsp.o.d" -o ${OBJECTDIR}/_ext/74298950/bsp.o ../src/system_config/default/bsp/bsp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o: ../src/system_config/default/framework/gfx/driver/controller/glcd/src/drv_gfx_glcd_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/832618739" 
	@${RM} ${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o.d" -o ${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o ../src/system_config/default/framework/gfx/driver/controller/glcd/src/drv_gfx_glcd_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o: ../src/system_config/default/framework/gfx/driver/processor/nano2d/libnano2D_hal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1476874530" 
	@${RM} ${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o.d" -o ${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o ../src/system_config/default/framework/gfx/driver/processor/nano2d/libnano2D_hal.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2065038297/gfx_display_def.o: ../src/system_config/default/framework/gfx/hal/gfx_display_def.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2065038297" 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_display_def.o.d 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_display_def.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2065038297/gfx_display_def.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2065038297/gfx_display_def.o.d" -o ${OBJECTDIR}/_ext/2065038297/gfx_display_def.o ../src/system_config/default/framework/gfx/hal/gfx_display_def.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o: ../src/system_config/default/framework/gfx/hal/gfx_driver_def.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2065038297" 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o.d 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o.d" -o ${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o ../src/system_config/default/framework/gfx/hal/gfx_driver_def.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o: ../src/system_config/default/framework/gfx/hal/gfx_processor_def.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2065038297" 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o.d 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o.d" -o ${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o ../src/system_config/default/framework/gfx/hal/gfx_processor_def.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o: ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/639803181" 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/340578644/sys_devcon.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ../src/system_config/default/framework/system/devcon/src/sys_devcon.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o: ../src/system_config/default/framework/system/memory/ddr/src/sys_memory_ddr_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/494920387" 
	@${RM} ${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o.d" -o ${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o ../src/system_config/default/framework/system/memory/ddr/src/sys_memory_ddr_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1541531744/sys_memory_static.o: ../src/system_config/default/framework/system/memory/src/sys_memory_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1541531744" 
	@${RM} ${OBJECTDIR}/_ext/1541531744/sys_memory_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/1541531744/sys_memory_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1541531744/sys_memory_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1541531744/sys_memory_static.o.d" -o ${OBJECTDIR}/_ext/1541531744/sys_memory_static.o ../src/system_config/default/framework/system/memory/src/sys_memory_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/822048611/sys_ports_static.o: ../src/system_config/default/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/822048611" 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ../src/system_config/default/framework/system/ports/src/sys_ports_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/system_init.o: ../src/system_config/default/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_init.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_init.o ../src/system_config/default/system_init.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/system_interrupt.o: ../src/system_config/default/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ../src/system_config/default/system_interrupt.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/system_exceptions.o: ../src/system_config/default/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ../src/system_config/default/system_exceptions.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/system_tasks.o: ../src/system_config/default/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ../src/system_config/default/system_tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/rtos_hooks.o: ../src/system_config/default/rtos_hooks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/rtos_hooks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/rtos_hooks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/rtos_hooks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/rtos_hooks.o.d" -o ${OBJECTDIR}/_ext/1688732426/rtos_hooks.o ../src/system_config/default/rtos_hooks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/mymodule.o: ../src/mymodule.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/mymodule.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/mymodule.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/mymodule.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/mymodule.o.d" -o ${OBJECTDIR}/_ext/1360937237/mymodule.o ../src/mymodule.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/testmodule.o: ../src/testmodule.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/testmodule.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/testmodule.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/testmodule.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/testmodule.o.d" -o ${OBJECTDIR}/_ext/1360937237/testmodule.o ../src/testmodule.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/hwapp.o: ../src/hwapp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hwapp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hwapp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/hwapp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/hwapp.o.d" -o ${OBJECTDIR}/_ext/1360937237/hwapp.o ../src/hwapp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modtouch.o: ../src/modtouch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modtouch.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modtouch.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modtouch.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modtouch.o.d" -o ${OBJECTDIR}/_ext/1360937237/modtouch.o ../src/modtouch.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modio.o: ../src/modio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modio.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modio.o.d" -o ${OBJECTDIR}/_ext/1360937237/modio.o ../src/modio.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modsys.o: ../src/modsys.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modsys.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modsys.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modsys.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modsys.o.d" -o ${OBJECTDIR}/_ext/1360937237/modsys.o ../src/modsys.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modutime.o: ../src/modutime.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modutime.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modutime.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modutime.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modutime.o.d" -o ${OBJECTDIR}/_ext/1360937237/modutime.o ../src/modutime.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modgfx.o: ../src/modgfx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modgfx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modgfx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modgfx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modgfx.o.d" -o ${OBJECTDIR}/_ext/1360937237/modgfx.o ../src/modgfx.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/moddemo.o: ../src/moddemo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/moddemo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/moddemo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/moddemo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/moddemo.o.d" -o ${OBJECTDIR}/_ext/1360937237/moddemo.o ../src/moddemo.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modtest.o: ../src/modtest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modtest.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modtest.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modtest.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modtest.o.d" -o ${OBJECTDIR}/_ext/1360937237/modtest.o ../src/modtest.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/extmod/virtpin.o: extmod/virtpin.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/extmod" 
	@${RM} ${OBJECTDIR}/extmod/virtpin.o.d 
	@${RM} ${OBJECTDIR}/extmod/virtpin.o 
	@${FIXDEPS} "${OBJECTDIR}/extmod/virtpin.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/extmod/virtpin.o.d" -o ${OBJECTDIR}/extmod/virtpin.o extmod/virtpin.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/extmod/moduselect.o: extmod/moduselect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/extmod" 
	@${RM} ${OBJECTDIR}/extmod/moduselect.o.d 
	@${RM} ${OBJECTDIR}/extmod/moduselect.o 
	@${FIXDEPS} "${OBJECTDIR}/extmod/moduselect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/extmod/moduselect.o.d" -o ${OBJECTDIR}/extmod/moduselect.o extmod/moduselect.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/extmod/modutimeq.o: extmod/modutimeq.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/extmod" 
	@${RM} ${OBJECTDIR}/extmod/modutimeq.o.d 
	@${RM} ${OBJECTDIR}/extmod/modutimeq.o 
	@${FIXDEPS} "${OBJECTDIR}/extmod/modutimeq.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/extmod/modutimeq.o.d" -o ${OBJECTDIR}/extmod/modutimeq.o extmod/modutimeq.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/extmod/utime_mphal.o: extmod/utime_mphal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/extmod" 
	@${RM} ${OBJECTDIR}/extmod/utime_mphal.o.d 
	@${RM} ${OBJECTDIR}/extmod/utime_mphal.o 
	@${FIXDEPS} "${OBJECTDIR}/extmod/utime_mphal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/extmod/utime_mphal.o.d" -o ${OBJECTDIR}/extmod/utime_mphal.o extmod/utime_mphal.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/280795049/drv_i2c.o: ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/280795049" 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o.d 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" -o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/507552489/drv_sdhc.o: ../../../../framework/driver/sdhc/src/drv_sdhc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/507552489" 
	@${RM} ${OBJECTDIR}/_ext/507552489/drv_sdhc.o.d 
	@${RM} ${OBJECTDIR}/_ext/507552489/drv_sdhc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/507552489/drv_sdhc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/507552489/drv_sdhc.o.d" -o ${OBJECTDIR}/_ext/507552489/drv_sdhc.o ../../../../framework/driver/sdhc/src/drv_sdhc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o: ../../../../framework/driver/sdhc/src/drv_sdhc_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/507552489" 
	@${RM} ${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o.d" -o ${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o ../../../../framework/driver/sdhc/src/drv_sdhc_host.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/185269848/drv_tmr.o: ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/185269848" 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o: ../../../../framework/driver/touch/mtch6301/src/drv_mtch6301.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1297154085" 
	@${RM} ${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o.d 
	@${RM} ${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o.d" -o ${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o ../../../../framework/driver/touch/mtch6301/src/drv_mtch6301.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/260586732/drv_usart.o: ../../../../framework/driver/usart/src/dynamic/drv_usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/260586732" 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/260586732/drv_usart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/260586732/drv_usart.o.d" -o ${OBJECTDIR}/_ext/260586732/drv_usart.o ../../../../framework/driver/usart/src/dynamic/drv_usart.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o: ../../../../framework/driver/usart/src/dynamic/drv_usart_read_write.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/260586732" 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o.d 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o.d" -o ${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o ../../../../framework/driver/usart/src/dynamic/drv_usart_read_write.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/246898221/drv_usbhs.o: ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/246898221" 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d" -o ${OBJECTDIR}/_ext/246898221/drv_usbhs.o ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o: ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/246898221" 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o.d" -o ${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx.o: ../../../../framework/gfx/hal/src/gfx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx.o ../../../../framework/gfx/hal/src/gfx.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color.o: ../../../../framework/gfx/hal/src/gfx_color.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color.o ../../../../framework/gfx/hal/src/gfx_color.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_context.o: ../../../../framework/gfx/hal/src/gfx_context.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_context.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_context.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_context.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_context.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_context.o ../../../../framework/gfx/hal/src/gfx_context.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_default_impl.o: ../../../../framework/gfx/hal/src/gfx_default_impl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_default_impl.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_default_impl.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_default_impl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_default_impl.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_default_impl.o ../../../../framework/gfx/hal/src/gfx_default_impl.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_display.o: ../../../../framework/gfx/hal/src/gfx_display.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_display.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_display.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_display.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_display.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_display.o ../../../../framework/gfx/hal/src/gfx_display.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o: ../../../../framework/gfx/hal/src/gfx_driver_interface.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o ../../../../framework/gfx/hal/src/gfx_driver_interface.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_get.o: ../../../../framework/gfx/hal/src/gfx_get.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_get.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_get.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_get.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_get.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_get.o ../../../../framework/gfx/hal/src/gfx_get.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_interface.o: ../../../../framework/gfx/hal/src/gfx_interface.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_interface.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_interface.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_interface.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_interface.o ../../../../framework/gfx/hal/src/gfx_interface.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_layer.o: ../../../../framework/gfx/hal/src/gfx_layer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_layer.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_layer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_layer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_layer.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_layer.o ../../../../framework/gfx/hal/src/gfx_layer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o: ../../../../framework/gfx/hal/src/gfx_pixel_buffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o ../../../../framework/gfx/hal/src/gfx_pixel_buffer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o: ../../../../framework/gfx/hal/src/gfx_processor_interface.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o ../../../../framework/gfx/hal/src/gfx_processor_interface.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_rect.o: ../../../../framework/gfx/hal/src/gfx_rect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_rect.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_rect.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_rect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_rect.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_rect.o ../../../../framework/gfx/hal/src/gfx_rect.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_util.o: ../../../../framework/gfx/hal/src/gfx_util.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_util.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_util.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_util.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_util.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_util.o ../../../../framework/gfx/hal/src/gfx_util.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_set.o: ../../../../framework/gfx/hal/src/gfx_set.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_set.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_set.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_set.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_set.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_set.o ../../../../framework/gfx/hal/src/gfx_set.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color_blend.o: ../../../../framework/gfx/hal/src/gfx_color_blend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_blend.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_blend.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color_blend.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color_blend.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color_blend.o ../../../../framework/gfx/hal/src/gfx_color_blend.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color_convert.o: ../../../../framework/gfx/hal/src/gfx_color_convert.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_convert.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_convert.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color_convert.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color_convert.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color_convert.o ../../../../framework/gfx/hal/src/gfx_color_convert.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o: ../../../../framework/gfx/hal/src/gfx_color_lerp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o ../../../../framework/gfx/hal/src/gfx_color_lerp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color_value.o: ../../../../framework/gfx/hal/src/gfx_color_value.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_value.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_value.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color_value.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color_value.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color_value.o ../../../../framework/gfx/hal/src/gfx_color_value.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o: ../../../../framework/gfx/hal/src/gfx_draw_blit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o ../../../../framework/gfx/hal/src/gfx_draw_blit.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o: ../../../../framework/gfx/hal/src/gfx_draw_circle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o ../../../../framework/gfx/hal/src/gfx_draw_circle.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_line.o: ../../../../framework/gfx/hal/src/gfx_draw_line.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_line.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_line.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_line.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_line.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_line.o ../../../../framework/gfx/hal/src/gfx_draw_line.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o: ../../../../framework/gfx/hal/src/gfx_draw_pixel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o ../../../../framework/gfx/hal/src/gfx_draw_pixel.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o: ../../../../framework/gfx/hal/src/gfx_draw_rect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o ../../../../framework/gfx/hal/src/gfx_draw_rect.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o: ../../../../framework/gfx/hal/src/gfx_draw_stretchblit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o ../../../../framework/gfx/hal/src/gfx_draw_stretchblit.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_math.o: ../../../../framework/gfx/hal/src/gfx_math.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_math.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_math.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_math.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_math.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_math.o ../../../../framework/gfx/hal/src/gfx_math.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/24337685/osal_freertos.o: ../../../../framework/osal/src/osal_freertos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/24337685" 
	@${RM} ${OBJECTDIR}/_ext/24337685/osal_freertos.o.d 
	@${RM} ${OBJECTDIR}/_ext/24337685/osal_freertos.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/24337685/osal_freertos.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/24337685/osal_freertos.o.d" -o ${OBJECTDIR}/_ext/24337685/osal_freertos.o ../../../../framework/osal/src/osal_freertos.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/30809027/sys_console.o: ../../../../framework/system/console/src/sys_console.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/30809027" 
	@${RM} ${OBJECTDIR}/_ext/30809027/sys_console.o.d 
	@${RM} ${OBJECTDIR}/_ext/30809027/sys_console.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/30809027/sys_console.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/30809027/sys_console.o.d" -o ${OBJECTDIR}/_ext/30809027/sys_console.o ../../../../framework/system/console/src/sys_console.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o: ../../../../framework/system/console/src/sys_console_usb_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/30809027" 
	@${RM} ${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o.d" -o ${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o ../../../../framework/system/console/src/sys_console_usb_cdc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/65930274/sys_dma.o: ../../../../framework/system/dma/src/sys_dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/65930274" 
	@${RM} ${OBJECTDIR}/_ext/65930274/sys_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/65930274/sys_dma.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/65930274/sys_dma.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/65930274/sys_dma.o.d" -o ${OBJECTDIR}/_ext/65930274/sys_dma.o ../../../../framework/system/dma/src/sys_dma.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2104899551/sys_fs.o: ../../../../framework/system/fs/src/dynamic/sys_fs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2104899551" 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs.o.d 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2104899551/sys_fs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2104899551/sys_fs.o.d" -o ${OBJECTDIR}/_ext/2104899551/sys_fs.o ../../../../framework/system/fs/src/dynamic/sys_fs.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o: ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2104899551" 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d" -o ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/66287330/ff.o: ../../../../framework/system/fs/fat_fs/src/file_system/ff.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/66287330" 
	@${RM} ${OBJECTDIR}/_ext/66287330/ff.o.d 
	@${RM} ${OBJECTDIR}/_ext/66287330/ff.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/66287330/ff.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/66287330/ff.o.d" -o ${OBJECTDIR}/_ext/66287330/ff.o ../../../../framework/system/fs/fat_fs/src/file_system/ff.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2072869785/diskio.o: ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2072869785" 
	@${RM} ${OBJECTDIR}/_ext/2072869785/diskio.o.d 
	@${RM} ${OBJECTDIR}/_ext/2072869785/diskio.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2072869785/diskio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2072869785/diskio.o.d" -o ${OBJECTDIR}/_ext/2072869785/diskio.o ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1264926591/sys_tmr.o: ../../../../framework/system/tmr/src/sys_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1264926591" 
	@${RM} ${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1264926591/sys_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/1264926591/sys_tmr.o ../../../../framework/system/tmr/src/sys_tmr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/846513563/sys_touch.o: ../../../../framework/system/touch/src/sys_touch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/846513563" 
	@${RM} ${OBJECTDIR}/_ext/846513563/sys_touch.o.d 
	@${RM} ${OBJECTDIR}/_ext/846513563/sys_touch.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/846513563/sys_touch.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/846513563/sys_touch.o.d" -o ${OBJECTDIR}/_ext/846513563/sys_touch.o ../../../../framework/system/touch/src/sys_touch.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/610166344/usb_device.o: ../../../../framework/usb/src/dynamic/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device.o ../../../../framework/usb/src/dynamic/usb_device.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/610166344/usb_device_cdc.o: ../../../../framework/usb/src/dynamic/usb_device_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o ../../../../framework/usb/src/dynamic/usb_device_cdc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o: ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1856876499/heap_3.o: ../../../../third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1856876499" 
	@${RM} ${OBJECTDIR}/_ext/1856876499/heap_3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1856876499/heap_3.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1856876499/heap_3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1856876499/heap_3.o.d" -o ${OBJECTDIR}/_ext/1856876499/heap_3.o ../../../../third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_3.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/12131620/port.o: ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/12131620" 
	@${RM} ${OBJECTDIR}/_ext/12131620/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/12131620/port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12131620/port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/12131620/port.o.d" -o ${OBJECTDIR}/_ext/12131620/port.o ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/croutine.o: ../../../../third_party/rtos/FreeRTOS/Source/croutine.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/croutine.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/croutine.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/croutine.o.d" -o ${OBJECTDIR}/_ext/1128951024/croutine.o ../../../../third_party/rtos/FreeRTOS/Source/croutine.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/list.o: ../../../../third_party/rtos/FreeRTOS/Source/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/list.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/list.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/list.o.d" -o ${OBJECTDIR}/_ext/1128951024/list.o ../../../../third_party/rtos/FreeRTOS/Source/list.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/queue.o: ../../../../third_party/rtos/FreeRTOS/Source/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/queue.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/queue.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/queue.o.d" -o ${OBJECTDIR}/_ext/1128951024/queue.o ../../../../third_party/rtos/FreeRTOS/Source/queue.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/tasks.o: ../../../../third_party/rtos/FreeRTOS/Source/tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/tasks.o.d" -o ${OBJECTDIR}/_ext/1128951024/tasks.o ../../../../third_party/rtos/FreeRTOS/Source/tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/timers.o: ../../../../third_party/rtos/FreeRTOS/Source/timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/timers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/timers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/timers.o.d" -o ${OBJECTDIR}/_ext/1128951024/timers.o ../../../../third_party/rtos/FreeRTOS/Source/timers.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/event_groups.o: ../../../../third_party/rtos/FreeRTOS/Source/event_groups.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/event_groups.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/event_groups.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/event_groups.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/event_groups.o.d" -o ${OBJECTDIR}/_ext/1128951024/event_groups.o ../../../../third_party/rtos/FreeRTOS/Source/event_groups.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/lib/libc/string0.o: lib/libc/string0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/libc" 
	@${RM} ${OBJECTDIR}/lib/libc/string0.o.d 
	@${RM} ${OBJECTDIR}/lib/libc/string0.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/libc/string0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/lib/libc/string0.o.d" -o ${OBJECTDIR}/lib/libc/string0.o lib/libc/string0.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/lib/mp-readline/readline.o: lib/mp-readline/readline.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/mp-readline" 
	@${RM} ${OBJECTDIR}/lib/mp-readline/readline.o.d 
	@${RM} ${OBJECTDIR}/lib/mp-readline/readline.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/mp-readline/readline.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/lib/mp-readline/readline.o.d" -o ${OBJECTDIR}/lib/mp-readline/readline.o lib/mp-readline/readline.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/lib/utils/pyexec.o: lib/utils/pyexec.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/utils" 
	@${RM} ${OBJECTDIR}/lib/utils/pyexec.o.d 
	@${RM} ${OBJECTDIR}/lib/utils/pyexec.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/utils/pyexec.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/lib/utils/pyexec.o.d" -o ${OBJECTDIR}/lib/utils/pyexec.o lib/utils/pyexec.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/lib/utils/stdout_helpers.o: lib/utils/stdout_helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/utils" 
	@${RM} ${OBJECTDIR}/lib/utils/stdout_helpers.o.d 
	@${RM} ${OBJECTDIR}/lib/utils/stdout_helpers.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/utils/stdout_helpers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/lib/utils/stdout_helpers.o.d" -o ${OBJECTDIR}/lib/utils/stdout_helpers.o lib/utils/stdout_helpers.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/argcheck.o: py/argcheck.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/argcheck.o.d 
	@${RM} ${OBJECTDIR}/py/argcheck.o 
	@${FIXDEPS} "${OBJECTDIR}/py/argcheck.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/argcheck.o.d" -o ${OBJECTDIR}/py/argcheck.o py/argcheck.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/asmbase.o: py/asmbase.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/asmbase.o.d 
	@${RM} ${OBJECTDIR}/py/asmbase.o 
	@${FIXDEPS} "${OBJECTDIR}/py/asmbase.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/asmbase.o.d" -o ${OBJECTDIR}/py/asmbase.o py/asmbase.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/bc.o: py/bc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/bc.o.d 
	@${RM} ${OBJECTDIR}/py/bc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/bc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/bc.o.d" -o ${OBJECTDIR}/py/bc.o py/bc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/binary.o: py/binary.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/binary.o.d 
	@${RM} ${OBJECTDIR}/py/binary.o 
	@${FIXDEPS} "${OBJECTDIR}/py/binary.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/binary.o.d" -o ${OBJECTDIR}/py/binary.o py/binary.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/builtinevex.o: py/builtinevex.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/builtinevex.o.d 
	@${RM} ${OBJECTDIR}/py/builtinevex.o 
	@${FIXDEPS} "${OBJECTDIR}/py/builtinevex.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/builtinevex.o.d" -o ${OBJECTDIR}/py/builtinevex.o py/builtinevex.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/builtinimport.o: py/builtinimport.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/builtinimport.o.d 
	@${RM} ${OBJECTDIR}/py/builtinimport.o 
	@${FIXDEPS} "${OBJECTDIR}/py/builtinimport.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/builtinimport.o.d" -o ${OBJECTDIR}/py/builtinimport.o py/builtinimport.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/compile.o: py/compile.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/compile.o.d 
	@${RM} ${OBJECTDIR}/py/compile.o 
	@${FIXDEPS} "${OBJECTDIR}/py/compile.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/compile.o.d" -o ${OBJECTDIR}/py/compile.o py/compile.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/emitbc.o: py/emitbc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/emitbc.o.d 
	@${RM} ${OBJECTDIR}/py/emitbc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/emitbc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/emitbc.o.d" -o ${OBJECTDIR}/py/emitbc.o py/emitbc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/emitcommon.o: py/emitcommon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/emitcommon.o.d 
	@${RM} ${OBJECTDIR}/py/emitcommon.o 
	@${FIXDEPS} "${OBJECTDIR}/py/emitcommon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/emitcommon.o.d" -o ${OBJECTDIR}/py/emitcommon.o py/emitcommon.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/emitglue.o: py/emitglue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/emitglue.o.d 
	@${RM} ${OBJECTDIR}/py/emitglue.o 
	@${FIXDEPS} "${OBJECTDIR}/py/emitglue.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/emitglue.o.d" -o ${OBJECTDIR}/py/emitglue.o py/emitglue.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/frozenmod.o: py/frozenmod.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/frozenmod.o.d 
	@${RM} ${OBJECTDIR}/py/frozenmod.o 
	@${FIXDEPS} "${OBJECTDIR}/py/frozenmod.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/frozenmod.o.d" -o ${OBJECTDIR}/py/frozenmod.o py/frozenmod.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/gc.o: py/gc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/gc.o.d 
	@${RM} ${OBJECTDIR}/py/gc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/gc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/gc.o.d" -o ${OBJECTDIR}/py/gc.o py/gc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/lexer.o: py/lexer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/lexer.o.d 
	@${RM} ${OBJECTDIR}/py/lexer.o 
	@${FIXDEPS} "${OBJECTDIR}/py/lexer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/lexer.o.d" -o ${OBJECTDIR}/py/lexer.o py/lexer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/malloc.o: py/malloc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/malloc.o.d 
	@${RM} ${OBJECTDIR}/py/malloc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/malloc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/malloc.o.d" -o ${OBJECTDIR}/py/malloc.o py/malloc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/map.o: py/map.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/map.o.d 
	@${RM} ${OBJECTDIR}/py/map.o 
	@${FIXDEPS} "${OBJECTDIR}/py/map.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/map.o.d" -o ${OBJECTDIR}/py/map.o py/map.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/modbuiltins.o: py/modbuiltins.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/modbuiltins.o.d 
	@${RM} ${OBJECTDIR}/py/modbuiltins.o 
	@${FIXDEPS} "${OBJECTDIR}/py/modbuiltins.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/modbuiltins.o.d" -o ${OBJECTDIR}/py/modbuiltins.o py/modbuiltins.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/modmicropython.o: py/modmicropython.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/modmicropython.o.d 
	@${RM} ${OBJECTDIR}/py/modmicropython.o 
	@${FIXDEPS} "${OBJECTDIR}/py/modmicropython.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/modmicropython.o.d" -o ${OBJECTDIR}/py/modmicropython.o py/modmicropython.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/moduerrno.o: py/moduerrno.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/moduerrno.o.d 
	@${RM} ${OBJECTDIR}/py/moduerrno.o 
	@${FIXDEPS} "${OBJECTDIR}/py/moduerrno.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/moduerrno.o.d" -o ${OBJECTDIR}/py/moduerrno.o py/moduerrno.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/mpprint.o: py/mpprint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/mpprint.o.d 
	@${RM} ${OBJECTDIR}/py/mpprint.o 
	@${FIXDEPS} "${OBJECTDIR}/py/mpprint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/mpprint.o.d" -o ${OBJECTDIR}/py/mpprint.o py/mpprint.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/mpstate.o: py/mpstate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/mpstate.o.d 
	@${RM} ${OBJECTDIR}/py/mpstate.o 
	@${FIXDEPS} "${OBJECTDIR}/py/mpstate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/mpstate.o.d" -o ${OBJECTDIR}/py/mpstate.o py/mpstate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/mpz.o: py/mpz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/mpz.o.d 
	@${RM} ${OBJECTDIR}/py/mpz.o 
	@${FIXDEPS} "${OBJECTDIR}/py/mpz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/mpz.o.d" -o ${OBJECTDIR}/py/mpz.o py/mpz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/nlrsetjmp.o: py/nlrsetjmp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/nlrsetjmp.o.d 
	@${RM} ${OBJECTDIR}/py/nlrsetjmp.o 
	@${FIXDEPS} "${OBJECTDIR}/py/nlrsetjmp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/nlrsetjmp.o.d" -o ${OBJECTDIR}/py/nlrsetjmp.o py/nlrsetjmp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/obj.o: py/obj.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/obj.o.d 
	@${RM} ${OBJECTDIR}/py/obj.o 
	@${FIXDEPS} "${OBJECTDIR}/py/obj.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/obj.o.d" -o ${OBJECTDIR}/py/obj.o py/obj.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objbool.o: py/objbool.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objbool.o.d 
	@${RM} ${OBJECTDIR}/py/objbool.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objbool.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objbool.o.d" -o ${OBJECTDIR}/py/objbool.o py/objbool.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objboundmeth.o: py/objboundmeth.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objboundmeth.o.d 
	@${RM} ${OBJECTDIR}/py/objboundmeth.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objboundmeth.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objboundmeth.o.d" -o ${OBJECTDIR}/py/objboundmeth.o py/objboundmeth.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objcell.o: py/objcell.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objcell.o.d 
	@${RM} ${OBJECTDIR}/py/objcell.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objcell.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objcell.o.d" -o ${OBJECTDIR}/py/objcell.o py/objcell.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objclosure.o: py/objclosure.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objclosure.o.d 
	@${RM} ${OBJECTDIR}/py/objclosure.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objclosure.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objclosure.o.d" -o ${OBJECTDIR}/py/objclosure.o py/objclosure.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objdict.o: py/objdict.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objdict.o.d 
	@${RM} ${OBJECTDIR}/py/objdict.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objdict.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objdict.o.d" -o ${OBJECTDIR}/py/objdict.o py/objdict.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objexcept.o: py/objexcept.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objexcept.o.d 
	@${RM} ${OBJECTDIR}/py/objexcept.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objexcept.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objexcept.o.d" -o ${OBJECTDIR}/py/objexcept.o py/objexcept.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objfun.o: py/objfun.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objfun.o.d 
	@${RM} ${OBJECTDIR}/py/objfun.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objfun.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objfun.o.d" -o ${OBJECTDIR}/py/objfun.o py/objfun.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objgenerator.o: py/objgenerator.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objgenerator.o.d 
	@${RM} ${OBJECTDIR}/py/objgenerator.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objgenerator.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objgenerator.o.d" -o ${OBJECTDIR}/py/objgenerator.o py/objgenerator.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objgetitemiter.o: py/objgetitemiter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objgetitemiter.o.d 
	@${RM} ${OBJECTDIR}/py/objgetitemiter.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objgetitemiter.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objgetitemiter.o.d" -o ${OBJECTDIR}/py/objgetitemiter.o py/objgetitemiter.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objint.o: py/objint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objint.o.d 
	@${RM} ${OBJECTDIR}/py/objint.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objint.o.d" -o ${OBJECTDIR}/py/objint.o py/objint.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objlist.o: py/objlist.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objlist.o.d 
	@${RM} ${OBJECTDIR}/py/objlist.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objlist.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objlist.o.d" -o ${OBJECTDIR}/py/objlist.o py/objlist.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objmap.o: py/objmap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objmap.o.d 
	@${RM} ${OBJECTDIR}/py/objmap.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objmap.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objmap.o.d" -o ${OBJECTDIR}/py/objmap.o py/objmap.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objmodule.o: py/objmodule.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objmodule.o.d 
	@${RM} ${OBJECTDIR}/py/objmodule.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objmodule.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objmodule.o.d" -o ${OBJECTDIR}/py/objmodule.o py/objmodule.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objnone.o: py/objnone.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objnone.o.d 
	@${RM} ${OBJECTDIR}/py/objnone.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objnone.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objnone.o.d" -o ${OBJECTDIR}/py/objnone.o py/objnone.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objobject.o: py/objobject.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objobject.o.d 
	@${RM} ${OBJECTDIR}/py/objobject.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objobject.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objobject.o.d" -o ${OBJECTDIR}/py/objobject.o py/objobject.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objpolyiter.o: py/objpolyiter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objpolyiter.o.d 
	@${RM} ${OBJECTDIR}/py/objpolyiter.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objpolyiter.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objpolyiter.o.d" -o ${OBJECTDIR}/py/objpolyiter.o py/objpolyiter.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objrange.o: py/objrange.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objrange.o.d 
	@${RM} ${OBJECTDIR}/py/objrange.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objrange.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objrange.o.d" -o ${OBJECTDIR}/py/objrange.o py/objrange.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objsingleton.o: py/objsingleton.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objsingleton.o.d 
	@${RM} ${OBJECTDIR}/py/objsingleton.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objsingleton.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objsingleton.o.d" -o ${OBJECTDIR}/py/objsingleton.o py/objsingleton.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objstr.o: py/objstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objstr.o.d 
	@${RM} ${OBJECTDIR}/py/objstr.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objstr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objstr.o.d" -o ${OBJECTDIR}/py/objstr.o py/objstr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objtuple.o: py/objtuple.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objtuple.o.d 
	@${RM} ${OBJECTDIR}/py/objtuple.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objtuple.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objtuple.o.d" -o ${OBJECTDIR}/py/objtuple.o py/objtuple.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objtype.o: py/objtype.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objtype.o.d 
	@${RM} ${OBJECTDIR}/py/objtype.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objtype.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objtype.o.d" -o ${OBJECTDIR}/py/objtype.o py/objtype.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objzip.o: py/objzip.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objzip.o.d 
	@${RM} ${OBJECTDIR}/py/objzip.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objzip.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objzip.o.d" -o ${OBJECTDIR}/py/objzip.o py/objzip.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/opmethods.o: py/opmethods.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/opmethods.o.d 
	@${RM} ${OBJECTDIR}/py/opmethods.o 
	@${FIXDEPS} "${OBJECTDIR}/py/opmethods.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/opmethods.o.d" -o ${OBJECTDIR}/py/opmethods.o py/opmethods.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/parse.o: py/parse.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/parse.o.d 
	@${RM} ${OBJECTDIR}/py/parse.o 
	@${FIXDEPS} "${OBJECTDIR}/py/parse.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/parse.o.d" -o ${OBJECTDIR}/py/parse.o py/parse.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/parsenum.o: py/parsenum.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/parsenum.o.d 
	@${RM} ${OBJECTDIR}/py/parsenum.o 
	@${FIXDEPS} "${OBJECTDIR}/py/parsenum.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/parsenum.o.d" -o ${OBJECTDIR}/py/parsenum.o py/parsenum.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/parsenumbase.o: py/parsenumbase.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/parsenumbase.o.d 
	@${RM} ${OBJECTDIR}/py/parsenumbase.o 
	@${FIXDEPS} "${OBJECTDIR}/py/parsenumbase.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/parsenumbase.o.d" -o ${OBJECTDIR}/py/parsenumbase.o py/parsenumbase.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/qstr.o: py/qstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/qstr.o.d 
	@${RM} ${OBJECTDIR}/py/qstr.o 
	@${FIXDEPS} "${OBJECTDIR}/py/qstr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/qstr.o.d" -o ${OBJECTDIR}/py/qstr.o py/qstr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/reader.o: py/reader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/reader.o.d 
	@${RM} ${OBJECTDIR}/py/reader.o 
	@${FIXDEPS} "${OBJECTDIR}/py/reader.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/reader.o.d" -o ${OBJECTDIR}/py/reader.o py/reader.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/repl.o: py/repl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/repl.o.d 
	@${RM} ${OBJECTDIR}/py/repl.o 
	@${FIXDEPS} "${OBJECTDIR}/py/repl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/repl.o.d" -o ${OBJECTDIR}/py/repl.o py/repl.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/runtime.o: py/runtime.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/runtime.o.d 
	@${RM} ${OBJECTDIR}/py/runtime.o 
	@${FIXDEPS} "${OBJECTDIR}/py/runtime.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/runtime.o.d" -o ${OBJECTDIR}/py/runtime.o py/runtime.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/scope.o: py/scope.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/scope.o.d 
	@${RM} ${OBJECTDIR}/py/scope.o 
	@${FIXDEPS} "${OBJECTDIR}/py/scope.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/scope.o.d" -o ${OBJECTDIR}/py/scope.o py/scope.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/sequence.o: py/sequence.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/sequence.o.d 
	@${RM} ${OBJECTDIR}/py/sequence.o 
	@${FIXDEPS} "${OBJECTDIR}/py/sequence.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/sequence.o.d" -o ${OBJECTDIR}/py/sequence.o py/sequence.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/smallint.o: py/smallint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/smallint.o.d 
	@${RM} ${OBJECTDIR}/py/smallint.o 
	@${FIXDEPS} "${OBJECTDIR}/py/smallint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/smallint.o.d" -o ${OBJECTDIR}/py/smallint.o py/smallint.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/stackctrl.o: py/stackctrl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/stackctrl.o.d 
	@${RM} ${OBJECTDIR}/py/stackctrl.o 
	@${FIXDEPS} "${OBJECTDIR}/py/stackctrl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/stackctrl.o.d" -o ${OBJECTDIR}/py/stackctrl.o py/stackctrl.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/stream.o: py/stream.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/stream.o.d 
	@${RM} ${OBJECTDIR}/py/stream.o 
	@${FIXDEPS} "${OBJECTDIR}/py/stream.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/stream.o.d" -o ${OBJECTDIR}/py/stream.o py/stream.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/unicode.o: py/unicode.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/unicode.o.d 
	@${RM} ${OBJECTDIR}/py/unicode.o 
	@${FIXDEPS} "${OBJECTDIR}/py/unicode.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/unicode.o.d" -o ${OBJECTDIR}/py/unicode.o py/unicode.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/vm.o: py/vm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/vm.o.d 
	@${RM} ${OBJECTDIR}/py/vm.o 
	@${FIXDEPS} "${OBJECTDIR}/py/vm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/vm.o.d" -o ${OBJECTDIR}/py/vm.o py/vm.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/vstr.o: py/vstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/vstr.o.d 
	@${RM} ${OBJECTDIR}/py/vstr.o 
	@${FIXDEPS} "${OBJECTDIR}/py/vstr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/vstr.o.d" -o ${OBJECTDIR}/py/vstr.o py/vstr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/modgc.o: py/modgc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/modgc.o.d 
	@${RM} ${OBJECTDIR}/py/modgc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/modgc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/modgc.o.d" -o ${OBJECTDIR}/py/modgc.o py/modgc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/emitnative.o: py/emitnative.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/emitnative.o.d 
	@${RM} ${OBJECTDIR}/py/emitnative.o 
	@${FIXDEPS} "${OBJECTDIR}/py/emitnative.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/emitnative.o.d" -o ${OBJECTDIR}/py/emitnative.o py/emitnative.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/runtime_utils.o: py/runtime_utils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/runtime_utils.o.d 
	@${RM} ${OBJECTDIR}/py/runtime_utils.o 
	@${FIXDEPS} "${OBJECTDIR}/py/runtime_utils.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/runtime_utils.o.d" -o ${OBJECTDIR}/py/runtime_utils.o py/runtime_utils.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objslice.o: py/objslice.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objslice.o.d 
	@${RM} ${OBJECTDIR}/py/objslice.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objslice.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objslice.o.d" -o ${OBJECTDIR}/py/objslice.o py/objslice.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objarray.o: py/objarray.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objarray.o.d 
	@${RM} ${OBJECTDIR}/py/objarray.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objarray.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objarray.o.d" -o ${OBJECTDIR}/py/objarray.o py/objarray.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/modarray.o: py/modarray.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/modarray.o.d 
	@${RM} ${OBJECTDIR}/py/modarray.o 
	@${FIXDEPS} "${OBJECTDIR}/py/modarray.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/modarray.o.d" -o ${OBJECTDIR}/py/modarray.o py/modarray.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objint_longlong.o: py/objint_longlong.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objint_longlong.o.d 
	@${RM} ${OBJECTDIR}/py/objint_longlong.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objint_longlong.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objint_longlong.o.d" -o ${OBJECTDIR}/py/objint_longlong.o py/objint_longlong.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/uart_core.o: uart_core.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/uart_core.o.d 
	@${RM} ${OBJECTDIR}/uart_core.o 
	@${FIXDEPS} "${OBJECTDIR}/uart_core.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/uart_core.o.d" -o ${OBJECTDIR}/uart_core.o uart_core.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_mon_getc.o: _mon_getc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/_mon_getc.o.d 
	@${RM} ${OBJECTDIR}/_mon_getc.o 
	@${FIXDEPS} "${OBJECTDIR}/_mon_getc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_mon_getc.o.d" -o ${OBJECTDIR}/_mon_getc.o _mon_getc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/read.o: read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/read.o.d 
	@${RM} ${OBJECTDIR}/read.o 
	@${FIXDEPS} "${OBJECTDIR}/read.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/read.o.d" -o ${OBJECTDIR}/read.o read.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/sbrk.o: sbrk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sbrk.o.d 
	@${RM} ${OBJECTDIR}/sbrk.o 
	@${FIXDEPS} "${OBJECTDIR}/sbrk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/sbrk.o.d" -o ${OBJECTDIR}/sbrk.o sbrk.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/dlsbrk.o: ../src/dlsbrk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/dlsbrk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/dlsbrk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/dlsbrk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/dlsbrk.o.d" -o ${OBJECTDIR}/_ext/1360937237/dlsbrk.o ../src/dlsbrk.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/dlmalloc.o: ../src/dlmalloc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/dlmalloc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/dlmalloc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/dlmalloc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/dlmalloc.o.d" -o ${OBJECTDIR}/_ext/1360937237/dlmalloc.o ../src/dlmalloc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/sd_card_exec.o: ../src/sd_card_exec.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/sd_card_exec.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/sd_card_exec.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/sd_card_exec.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/sd_card_exec.o.d" -o ${OBJECTDIR}/_ext/1360937237/sd_card_exec.o ../src/sd_card_exec.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_frozen_mpy.o: _frozen_mpy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/_frozen_mpy.o.d 
	@${RM} ${OBJECTDIR}/_frozen_mpy.o 
	@${FIXDEPS} "${OBJECTDIR}/_frozen_mpy.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_frozen_mpy.o.d" -o ${OBJECTDIR}/_frozen_mpy.o _frozen_mpy.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/logbuffer.o: ../src/logbuffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/logbuffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/logbuffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/logbuffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/logbuffer.o.d" -o ${OBJECTDIR}/_ext/1360937237/logbuffer.o ../src/logbuffer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
else
${OBJECTDIR}/_ext/74298950/bsp.o: ../src/system_config/default/bsp/bsp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/74298950" 
	@${RM} ${OBJECTDIR}/_ext/74298950/bsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/74298950/bsp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/74298950/bsp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/74298950/bsp.o.d" -o ${OBJECTDIR}/_ext/74298950/bsp.o ../src/system_config/default/bsp/bsp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o: ../src/system_config/default/framework/gfx/driver/controller/glcd/src/drv_gfx_glcd_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/832618739" 
	@${RM} ${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o.d" -o ${OBJECTDIR}/_ext/832618739/drv_gfx_glcd_static.o ../src/system_config/default/framework/gfx/driver/controller/glcd/src/drv_gfx_glcd_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o: ../src/system_config/default/framework/gfx/driver/processor/nano2d/libnano2D_hal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1476874530" 
	@${RM} ${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o.d" -o ${OBJECTDIR}/_ext/1476874530/libnano2D_hal.o ../src/system_config/default/framework/gfx/driver/processor/nano2d/libnano2D_hal.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2065038297/gfx_display_def.o: ../src/system_config/default/framework/gfx/hal/gfx_display_def.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2065038297" 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_display_def.o.d 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_display_def.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2065038297/gfx_display_def.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2065038297/gfx_display_def.o.d" -o ${OBJECTDIR}/_ext/2065038297/gfx_display_def.o ../src/system_config/default/framework/gfx/hal/gfx_display_def.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o: ../src/system_config/default/framework/gfx/hal/gfx_driver_def.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2065038297" 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o.d 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o.d" -o ${OBJECTDIR}/_ext/2065038297/gfx_driver_def.o ../src/system_config/default/framework/gfx/hal/gfx_driver_def.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o: ../src/system_config/default/framework/gfx/hal/gfx_processor_def.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2065038297" 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o.d 
	@${RM} ${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o.d" -o ${OBJECTDIR}/_ext/2065038297/gfx_processor_def.o ../src/system_config/default/framework/gfx/hal/gfx_processor_def.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o: ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/639803181" 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/340578644/sys_devcon.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ../src/system_config/default/framework/system/devcon/src/sys_devcon.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o: ../src/system_config/default/framework/system/memory/ddr/src/sys_memory_ddr_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/494920387" 
	@${RM} ${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o.d" -o ${OBJECTDIR}/_ext/494920387/sys_memory_ddr_static.o ../src/system_config/default/framework/system/memory/ddr/src/sys_memory_ddr_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1541531744/sys_memory_static.o: ../src/system_config/default/framework/system/memory/src/sys_memory_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1541531744" 
	@${RM} ${OBJECTDIR}/_ext/1541531744/sys_memory_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/1541531744/sys_memory_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1541531744/sys_memory_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1541531744/sys_memory_static.o.d" -o ${OBJECTDIR}/_ext/1541531744/sys_memory_static.o ../src/system_config/default/framework/system/memory/src/sys_memory_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/822048611/sys_ports_static.o: ../src/system_config/default/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/822048611" 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ../src/system_config/default/framework/system/ports/src/sys_ports_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/system_init.o: ../src/system_config/default/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_init.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_init.o ../src/system_config/default/system_init.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/system_interrupt.o: ../src/system_config/default/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ../src/system_config/default/system_interrupt.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/system_exceptions.o: ../src/system_config/default/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ../src/system_config/default/system_exceptions.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/system_tasks.o: ../src/system_config/default/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ../src/system_config/default/system_tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1688732426/rtos_hooks.o: ../src/system_config/default/rtos_hooks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/rtos_hooks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/rtos_hooks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/rtos_hooks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1688732426/rtos_hooks.o.d" -o ${OBJECTDIR}/_ext/1688732426/rtos_hooks.o ../src/system_config/default/rtos_hooks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/mymodule.o: ../src/mymodule.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/mymodule.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/mymodule.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/mymodule.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/mymodule.o.d" -o ${OBJECTDIR}/_ext/1360937237/mymodule.o ../src/mymodule.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/testmodule.o: ../src/testmodule.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/testmodule.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/testmodule.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/testmodule.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/testmodule.o.d" -o ${OBJECTDIR}/_ext/1360937237/testmodule.o ../src/testmodule.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/hwapp.o: ../src/hwapp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hwapp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hwapp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/hwapp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/hwapp.o.d" -o ${OBJECTDIR}/_ext/1360937237/hwapp.o ../src/hwapp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modtouch.o: ../src/modtouch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modtouch.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modtouch.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modtouch.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modtouch.o.d" -o ${OBJECTDIR}/_ext/1360937237/modtouch.o ../src/modtouch.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modio.o: ../src/modio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modio.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modio.o.d" -o ${OBJECTDIR}/_ext/1360937237/modio.o ../src/modio.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modsys.o: ../src/modsys.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modsys.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modsys.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modsys.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modsys.o.d" -o ${OBJECTDIR}/_ext/1360937237/modsys.o ../src/modsys.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modutime.o: ../src/modutime.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modutime.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modutime.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modutime.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modutime.o.d" -o ${OBJECTDIR}/_ext/1360937237/modutime.o ../src/modutime.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modgfx.o: ../src/modgfx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modgfx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modgfx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modgfx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modgfx.o.d" -o ${OBJECTDIR}/_ext/1360937237/modgfx.o ../src/modgfx.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/moddemo.o: ../src/moddemo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/moddemo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/moddemo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/moddemo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/moddemo.o.d" -o ${OBJECTDIR}/_ext/1360937237/moddemo.o ../src/moddemo.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/modtest.o: ../src/modtest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modtest.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/modtest.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/modtest.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/modtest.o.d" -o ${OBJECTDIR}/_ext/1360937237/modtest.o ../src/modtest.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/extmod/virtpin.o: extmod/virtpin.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/extmod" 
	@${RM} ${OBJECTDIR}/extmod/virtpin.o.d 
	@${RM} ${OBJECTDIR}/extmod/virtpin.o 
	@${FIXDEPS} "${OBJECTDIR}/extmod/virtpin.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/extmod/virtpin.o.d" -o ${OBJECTDIR}/extmod/virtpin.o extmod/virtpin.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/extmod/moduselect.o: extmod/moduselect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/extmod" 
	@${RM} ${OBJECTDIR}/extmod/moduselect.o.d 
	@${RM} ${OBJECTDIR}/extmod/moduselect.o 
	@${FIXDEPS} "${OBJECTDIR}/extmod/moduselect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/extmod/moduselect.o.d" -o ${OBJECTDIR}/extmod/moduselect.o extmod/moduselect.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/extmod/modutimeq.o: extmod/modutimeq.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/extmod" 
	@${RM} ${OBJECTDIR}/extmod/modutimeq.o.d 
	@${RM} ${OBJECTDIR}/extmod/modutimeq.o 
	@${FIXDEPS} "${OBJECTDIR}/extmod/modutimeq.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/extmod/modutimeq.o.d" -o ${OBJECTDIR}/extmod/modutimeq.o extmod/modutimeq.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/extmod/utime_mphal.o: extmod/utime_mphal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/extmod" 
	@${RM} ${OBJECTDIR}/extmod/utime_mphal.o.d 
	@${RM} ${OBJECTDIR}/extmod/utime_mphal.o 
	@${FIXDEPS} "${OBJECTDIR}/extmod/utime_mphal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/extmod/utime_mphal.o.d" -o ${OBJECTDIR}/extmod/utime_mphal.o extmod/utime_mphal.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/280795049/drv_i2c.o: ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/280795049" 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o.d 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" -o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/507552489/drv_sdhc.o: ../../../../framework/driver/sdhc/src/drv_sdhc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/507552489" 
	@${RM} ${OBJECTDIR}/_ext/507552489/drv_sdhc.o.d 
	@${RM} ${OBJECTDIR}/_ext/507552489/drv_sdhc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/507552489/drv_sdhc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/507552489/drv_sdhc.o.d" -o ${OBJECTDIR}/_ext/507552489/drv_sdhc.o ../../../../framework/driver/sdhc/src/drv_sdhc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o: ../../../../framework/driver/sdhc/src/drv_sdhc_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/507552489" 
	@${RM} ${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o.d" -o ${OBJECTDIR}/_ext/507552489/drv_sdhc_host.o ../../../../framework/driver/sdhc/src/drv_sdhc_host.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/185269848/drv_tmr.o: ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/185269848" 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o: ../../../../framework/driver/touch/mtch6301/src/drv_mtch6301.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1297154085" 
	@${RM} ${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o.d 
	@${RM} ${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o.d" -o ${OBJECTDIR}/_ext/1297154085/drv_mtch6301.o ../../../../framework/driver/touch/mtch6301/src/drv_mtch6301.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/260586732/drv_usart.o: ../../../../framework/driver/usart/src/dynamic/drv_usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/260586732" 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/260586732/drv_usart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/260586732/drv_usart.o.d" -o ${OBJECTDIR}/_ext/260586732/drv_usart.o ../../../../framework/driver/usart/src/dynamic/drv_usart.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o: ../../../../framework/driver/usart/src/dynamic/drv_usart_read_write.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/260586732" 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o.d 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o.d" -o ${OBJECTDIR}/_ext/260586732/drv_usart_read_write.o ../../../../framework/driver/usart/src/dynamic/drv_usart_read_write.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/246898221/drv_usbhs.o: ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/246898221" 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d" -o ${OBJECTDIR}/_ext/246898221/drv_usbhs.o ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o: ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/246898221" 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o.d" -o ${OBJECTDIR}/_ext/246898221/drv_usbhs_device.o ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx.o: ../../../../framework/gfx/hal/src/gfx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx.o ../../../../framework/gfx/hal/src/gfx.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color.o: ../../../../framework/gfx/hal/src/gfx_color.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color.o ../../../../framework/gfx/hal/src/gfx_color.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_context.o: ../../../../framework/gfx/hal/src/gfx_context.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_context.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_context.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_context.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_context.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_context.o ../../../../framework/gfx/hal/src/gfx_context.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_default_impl.o: ../../../../framework/gfx/hal/src/gfx_default_impl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_default_impl.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_default_impl.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_default_impl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_default_impl.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_default_impl.o ../../../../framework/gfx/hal/src/gfx_default_impl.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_display.o: ../../../../framework/gfx/hal/src/gfx_display.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_display.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_display.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_display.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_display.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_display.o ../../../../framework/gfx/hal/src/gfx_display.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o: ../../../../framework/gfx/hal/src/gfx_driver_interface.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_driver_interface.o ../../../../framework/gfx/hal/src/gfx_driver_interface.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_get.o: ../../../../framework/gfx/hal/src/gfx_get.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_get.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_get.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_get.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_get.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_get.o ../../../../framework/gfx/hal/src/gfx_get.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_interface.o: ../../../../framework/gfx/hal/src/gfx_interface.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_interface.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_interface.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_interface.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_interface.o ../../../../framework/gfx/hal/src/gfx_interface.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_layer.o: ../../../../framework/gfx/hal/src/gfx_layer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_layer.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_layer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_layer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_layer.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_layer.o ../../../../framework/gfx/hal/src/gfx_layer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o: ../../../../framework/gfx/hal/src/gfx_pixel_buffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_pixel_buffer.o ../../../../framework/gfx/hal/src/gfx_pixel_buffer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o: ../../../../framework/gfx/hal/src/gfx_processor_interface.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_processor_interface.o ../../../../framework/gfx/hal/src/gfx_processor_interface.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_rect.o: ../../../../framework/gfx/hal/src/gfx_rect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_rect.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_rect.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_rect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_rect.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_rect.o ../../../../framework/gfx/hal/src/gfx_rect.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_util.o: ../../../../framework/gfx/hal/src/gfx_util.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_util.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_util.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_util.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_util.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_util.o ../../../../framework/gfx/hal/src/gfx_util.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_set.o: ../../../../framework/gfx/hal/src/gfx_set.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_set.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_set.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_set.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_set.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_set.o ../../../../framework/gfx/hal/src/gfx_set.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color_blend.o: ../../../../framework/gfx/hal/src/gfx_color_blend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_blend.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_blend.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color_blend.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color_blend.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color_blend.o ../../../../framework/gfx/hal/src/gfx_color_blend.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color_convert.o: ../../../../framework/gfx/hal/src/gfx_color_convert.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_convert.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_convert.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color_convert.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color_convert.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color_convert.o ../../../../framework/gfx/hal/src/gfx_color_convert.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o: ../../../../framework/gfx/hal/src/gfx_color_lerp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color_lerp.o ../../../../framework/gfx/hal/src/gfx_color_lerp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_color_value.o: ../../../../framework/gfx/hal/src/gfx_color_value.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_value.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_color_value.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_color_value.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_color_value.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_color_value.o ../../../../framework/gfx/hal/src/gfx_color_value.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o: ../../../../framework/gfx/hal/src/gfx_draw_blit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_blit.o ../../../../framework/gfx/hal/src/gfx_draw_blit.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o: ../../../../framework/gfx/hal/src/gfx_draw_circle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_circle.o ../../../../framework/gfx/hal/src/gfx_draw_circle.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_line.o: ../../../../framework/gfx/hal/src/gfx_draw_line.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_line.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_line.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_line.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_line.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_line.o ../../../../framework/gfx/hal/src/gfx_draw_line.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o: ../../../../framework/gfx/hal/src/gfx_draw_pixel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_pixel.o ../../../../framework/gfx/hal/src/gfx_draw_pixel.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o: ../../../../framework/gfx/hal/src/gfx_draw_rect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_rect.o ../../../../framework/gfx/hal/src/gfx_draw_rect.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o: ../../../../framework/gfx/hal/src/gfx_draw_stretchblit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_draw_stretchblit.o ../../../../framework/gfx/hal/src/gfx_draw_stretchblit.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/88308223/gfx_math.o: ../../../../framework/gfx/hal/src/gfx_math.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/88308223" 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_math.o.d 
	@${RM} ${OBJECTDIR}/_ext/88308223/gfx_math.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/88308223/gfx_math.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/88308223/gfx_math.o.d" -o ${OBJECTDIR}/_ext/88308223/gfx_math.o ../../../../framework/gfx/hal/src/gfx_math.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/24337685/osal_freertos.o: ../../../../framework/osal/src/osal_freertos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/24337685" 
	@${RM} ${OBJECTDIR}/_ext/24337685/osal_freertos.o.d 
	@${RM} ${OBJECTDIR}/_ext/24337685/osal_freertos.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/24337685/osal_freertos.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/24337685/osal_freertos.o.d" -o ${OBJECTDIR}/_ext/24337685/osal_freertos.o ../../../../framework/osal/src/osal_freertos.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/30809027/sys_console.o: ../../../../framework/system/console/src/sys_console.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/30809027" 
	@${RM} ${OBJECTDIR}/_ext/30809027/sys_console.o.d 
	@${RM} ${OBJECTDIR}/_ext/30809027/sys_console.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/30809027/sys_console.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/30809027/sys_console.o.d" -o ${OBJECTDIR}/_ext/30809027/sys_console.o ../../../../framework/system/console/src/sys_console.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o: ../../../../framework/system/console/src/sys_console_usb_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/30809027" 
	@${RM} ${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o.d" -o ${OBJECTDIR}/_ext/30809027/sys_console_usb_cdc.o ../../../../framework/system/console/src/sys_console_usb_cdc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/65930274/sys_dma.o: ../../../../framework/system/dma/src/sys_dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/65930274" 
	@${RM} ${OBJECTDIR}/_ext/65930274/sys_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/65930274/sys_dma.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/65930274/sys_dma.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/65930274/sys_dma.o.d" -o ${OBJECTDIR}/_ext/65930274/sys_dma.o ../../../../framework/system/dma/src/sys_dma.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2104899551/sys_fs.o: ../../../../framework/system/fs/src/dynamic/sys_fs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2104899551" 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs.o.d 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2104899551/sys_fs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2104899551/sys_fs.o.d" -o ${OBJECTDIR}/_ext/2104899551/sys_fs.o ../../../../framework/system/fs/src/dynamic/sys_fs.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o: ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2104899551" 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d" -o ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/66287330/ff.o: ../../../../framework/system/fs/fat_fs/src/file_system/ff.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/66287330" 
	@${RM} ${OBJECTDIR}/_ext/66287330/ff.o.d 
	@${RM} ${OBJECTDIR}/_ext/66287330/ff.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/66287330/ff.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/66287330/ff.o.d" -o ${OBJECTDIR}/_ext/66287330/ff.o ../../../../framework/system/fs/fat_fs/src/file_system/ff.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/2072869785/diskio.o: ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2072869785" 
	@${RM} ${OBJECTDIR}/_ext/2072869785/diskio.o.d 
	@${RM} ${OBJECTDIR}/_ext/2072869785/diskio.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2072869785/diskio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/2072869785/diskio.o.d" -o ${OBJECTDIR}/_ext/2072869785/diskio.o ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1264926591/sys_tmr.o: ../../../../framework/system/tmr/src/sys_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1264926591" 
	@${RM} ${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1264926591/sys_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/1264926591/sys_tmr.o ../../../../framework/system/tmr/src/sys_tmr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/846513563/sys_touch.o: ../../../../framework/system/touch/src/sys_touch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/846513563" 
	@${RM} ${OBJECTDIR}/_ext/846513563/sys_touch.o.d 
	@${RM} ${OBJECTDIR}/_ext/846513563/sys_touch.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/846513563/sys_touch.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/846513563/sys_touch.o.d" -o ${OBJECTDIR}/_ext/846513563/sys_touch.o ../../../../framework/system/touch/src/sys_touch.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/610166344/usb_device.o: ../../../../framework/usb/src/dynamic/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device.o ../../../../framework/usb/src/dynamic/usb_device.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/610166344/usb_device_cdc.o: ../../../../framework/usb/src/dynamic/usb_device_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o ../../../../framework/usb/src/dynamic/usb_device_cdc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o: ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1856876499/heap_3.o: ../../../../third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1856876499" 
	@${RM} ${OBJECTDIR}/_ext/1856876499/heap_3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1856876499/heap_3.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1856876499/heap_3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1856876499/heap_3.o.d" -o ${OBJECTDIR}/_ext/1856876499/heap_3.o ../../../../third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_3.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/12131620/port.o: ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/12131620" 
	@${RM} ${OBJECTDIR}/_ext/12131620/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/12131620/port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12131620/port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/12131620/port.o.d" -o ${OBJECTDIR}/_ext/12131620/port.o ../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ/port.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/croutine.o: ../../../../third_party/rtos/FreeRTOS/Source/croutine.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/croutine.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/croutine.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/croutine.o.d" -o ${OBJECTDIR}/_ext/1128951024/croutine.o ../../../../third_party/rtos/FreeRTOS/Source/croutine.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/list.o: ../../../../third_party/rtos/FreeRTOS/Source/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/list.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/list.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/list.o.d" -o ${OBJECTDIR}/_ext/1128951024/list.o ../../../../third_party/rtos/FreeRTOS/Source/list.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/queue.o: ../../../../third_party/rtos/FreeRTOS/Source/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/queue.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/queue.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/queue.o.d" -o ${OBJECTDIR}/_ext/1128951024/queue.o ../../../../third_party/rtos/FreeRTOS/Source/queue.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/tasks.o: ../../../../third_party/rtos/FreeRTOS/Source/tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/tasks.o.d" -o ${OBJECTDIR}/_ext/1128951024/tasks.o ../../../../third_party/rtos/FreeRTOS/Source/tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/timers.o: ../../../../third_party/rtos/FreeRTOS/Source/timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/timers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/timers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/timers.o.d" -o ${OBJECTDIR}/_ext/1128951024/timers.o ../../../../third_party/rtos/FreeRTOS/Source/timers.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1128951024/event_groups.o: ../../../../third_party/rtos/FreeRTOS/Source/event_groups.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1128951024" 
	@${RM} ${OBJECTDIR}/_ext/1128951024/event_groups.o.d 
	@${RM} ${OBJECTDIR}/_ext/1128951024/event_groups.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1128951024/event_groups.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1128951024/event_groups.o.d" -o ${OBJECTDIR}/_ext/1128951024/event_groups.o ../../../../third_party/rtos/FreeRTOS/Source/event_groups.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/lib/libc/string0.o: lib/libc/string0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/libc" 
	@${RM} ${OBJECTDIR}/lib/libc/string0.o.d 
	@${RM} ${OBJECTDIR}/lib/libc/string0.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/libc/string0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/lib/libc/string0.o.d" -o ${OBJECTDIR}/lib/libc/string0.o lib/libc/string0.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/lib/mp-readline/readline.o: lib/mp-readline/readline.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/mp-readline" 
	@${RM} ${OBJECTDIR}/lib/mp-readline/readline.o.d 
	@${RM} ${OBJECTDIR}/lib/mp-readline/readline.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/mp-readline/readline.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/lib/mp-readline/readline.o.d" -o ${OBJECTDIR}/lib/mp-readline/readline.o lib/mp-readline/readline.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/lib/utils/pyexec.o: lib/utils/pyexec.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/utils" 
	@${RM} ${OBJECTDIR}/lib/utils/pyexec.o.d 
	@${RM} ${OBJECTDIR}/lib/utils/pyexec.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/utils/pyexec.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/lib/utils/pyexec.o.d" -o ${OBJECTDIR}/lib/utils/pyexec.o lib/utils/pyexec.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/lib/utils/stdout_helpers.o: lib/utils/stdout_helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/utils" 
	@${RM} ${OBJECTDIR}/lib/utils/stdout_helpers.o.d 
	@${RM} ${OBJECTDIR}/lib/utils/stdout_helpers.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/utils/stdout_helpers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/lib/utils/stdout_helpers.o.d" -o ${OBJECTDIR}/lib/utils/stdout_helpers.o lib/utils/stdout_helpers.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/argcheck.o: py/argcheck.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/argcheck.o.d 
	@${RM} ${OBJECTDIR}/py/argcheck.o 
	@${FIXDEPS} "${OBJECTDIR}/py/argcheck.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/argcheck.o.d" -o ${OBJECTDIR}/py/argcheck.o py/argcheck.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/asmbase.o: py/asmbase.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/asmbase.o.d 
	@${RM} ${OBJECTDIR}/py/asmbase.o 
	@${FIXDEPS} "${OBJECTDIR}/py/asmbase.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/asmbase.o.d" -o ${OBJECTDIR}/py/asmbase.o py/asmbase.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/bc.o: py/bc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/bc.o.d 
	@${RM} ${OBJECTDIR}/py/bc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/bc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/bc.o.d" -o ${OBJECTDIR}/py/bc.o py/bc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/binary.o: py/binary.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/binary.o.d 
	@${RM} ${OBJECTDIR}/py/binary.o 
	@${FIXDEPS} "${OBJECTDIR}/py/binary.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/binary.o.d" -o ${OBJECTDIR}/py/binary.o py/binary.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/builtinevex.o: py/builtinevex.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/builtinevex.o.d 
	@${RM} ${OBJECTDIR}/py/builtinevex.o 
	@${FIXDEPS} "${OBJECTDIR}/py/builtinevex.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/builtinevex.o.d" -o ${OBJECTDIR}/py/builtinevex.o py/builtinevex.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/builtinimport.o: py/builtinimport.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/builtinimport.o.d 
	@${RM} ${OBJECTDIR}/py/builtinimport.o 
	@${FIXDEPS} "${OBJECTDIR}/py/builtinimport.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/builtinimport.o.d" -o ${OBJECTDIR}/py/builtinimport.o py/builtinimport.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/compile.o: py/compile.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/compile.o.d 
	@${RM} ${OBJECTDIR}/py/compile.o 
	@${FIXDEPS} "${OBJECTDIR}/py/compile.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/compile.o.d" -o ${OBJECTDIR}/py/compile.o py/compile.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/emitbc.o: py/emitbc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/emitbc.o.d 
	@${RM} ${OBJECTDIR}/py/emitbc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/emitbc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/emitbc.o.d" -o ${OBJECTDIR}/py/emitbc.o py/emitbc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/emitcommon.o: py/emitcommon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/emitcommon.o.d 
	@${RM} ${OBJECTDIR}/py/emitcommon.o 
	@${FIXDEPS} "${OBJECTDIR}/py/emitcommon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/emitcommon.o.d" -o ${OBJECTDIR}/py/emitcommon.o py/emitcommon.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/emitglue.o: py/emitglue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/emitglue.o.d 
	@${RM} ${OBJECTDIR}/py/emitglue.o 
	@${FIXDEPS} "${OBJECTDIR}/py/emitglue.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/emitglue.o.d" -o ${OBJECTDIR}/py/emitglue.o py/emitglue.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/frozenmod.o: py/frozenmod.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/frozenmod.o.d 
	@${RM} ${OBJECTDIR}/py/frozenmod.o 
	@${FIXDEPS} "${OBJECTDIR}/py/frozenmod.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/frozenmod.o.d" -o ${OBJECTDIR}/py/frozenmod.o py/frozenmod.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/gc.o: py/gc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/gc.o.d 
	@${RM} ${OBJECTDIR}/py/gc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/gc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/gc.o.d" -o ${OBJECTDIR}/py/gc.o py/gc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/lexer.o: py/lexer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/lexer.o.d 
	@${RM} ${OBJECTDIR}/py/lexer.o 
	@${FIXDEPS} "${OBJECTDIR}/py/lexer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/lexer.o.d" -o ${OBJECTDIR}/py/lexer.o py/lexer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/malloc.o: py/malloc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/malloc.o.d 
	@${RM} ${OBJECTDIR}/py/malloc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/malloc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/malloc.o.d" -o ${OBJECTDIR}/py/malloc.o py/malloc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/map.o: py/map.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/map.o.d 
	@${RM} ${OBJECTDIR}/py/map.o 
	@${FIXDEPS} "${OBJECTDIR}/py/map.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/map.o.d" -o ${OBJECTDIR}/py/map.o py/map.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/modbuiltins.o: py/modbuiltins.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/modbuiltins.o.d 
	@${RM} ${OBJECTDIR}/py/modbuiltins.o 
	@${FIXDEPS} "${OBJECTDIR}/py/modbuiltins.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/modbuiltins.o.d" -o ${OBJECTDIR}/py/modbuiltins.o py/modbuiltins.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/modmicropython.o: py/modmicropython.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/modmicropython.o.d 
	@${RM} ${OBJECTDIR}/py/modmicropython.o 
	@${FIXDEPS} "${OBJECTDIR}/py/modmicropython.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/modmicropython.o.d" -o ${OBJECTDIR}/py/modmicropython.o py/modmicropython.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/moduerrno.o: py/moduerrno.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/moduerrno.o.d 
	@${RM} ${OBJECTDIR}/py/moduerrno.o 
	@${FIXDEPS} "${OBJECTDIR}/py/moduerrno.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/moduerrno.o.d" -o ${OBJECTDIR}/py/moduerrno.o py/moduerrno.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/mpprint.o: py/mpprint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/mpprint.o.d 
	@${RM} ${OBJECTDIR}/py/mpprint.o 
	@${FIXDEPS} "${OBJECTDIR}/py/mpprint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/mpprint.o.d" -o ${OBJECTDIR}/py/mpprint.o py/mpprint.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/mpstate.o: py/mpstate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/mpstate.o.d 
	@${RM} ${OBJECTDIR}/py/mpstate.o 
	@${FIXDEPS} "${OBJECTDIR}/py/mpstate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/mpstate.o.d" -o ${OBJECTDIR}/py/mpstate.o py/mpstate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/mpz.o: py/mpz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/mpz.o.d 
	@${RM} ${OBJECTDIR}/py/mpz.o 
	@${FIXDEPS} "${OBJECTDIR}/py/mpz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/mpz.o.d" -o ${OBJECTDIR}/py/mpz.o py/mpz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/nlrsetjmp.o: py/nlrsetjmp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/nlrsetjmp.o.d 
	@${RM} ${OBJECTDIR}/py/nlrsetjmp.o 
	@${FIXDEPS} "${OBJECTDIR}/py/nlrsetjmp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/nlrsetjmp.o.d" -o ${OBJECTDIR}/py/nlrsetjmp.o py/nlrsetjmp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/obj.o: py/obj.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/obj.o.d 
	@${RM} ${OBJECTDIR}/py/obj.o 
	@${FIXDEPS} "${OBJECTDIR}/py/obj.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/obj.o.d" -o ${OBJECTDIR}/py/obj.o py/obj.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objbool.o: py/objbool.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objbool.o.d 
	@${RM} ${OBJECTDIR}/py/objbool.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objbool.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objbool.o.d" -o ${OBJECTDIR}/py/objbool.o py/objbool.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objboundmeth.o: py/objboundmeth.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objboundmeth.o.d 
	@${RM} ${OBJECTDIR}/py/objboundmeth.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objboundmeth.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objboundmeth.o.d" -o ${OBJECTDIR}/py/objboundmeth.o py/objboundmeth.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objcell.o: py/objcell.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objcell.o.d 
	@${RM} ${OBJECTDIR}/py/objcell.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objcell.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objcell.o.d" -o ${OBJECTDIR}/py/objcell.o py/objcell.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objclosure.o: py/objclosure.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objclosure.o.d 
	@${RM} ${OBJECTDIR}/py/objclosure.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objclosure.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objclosure.o.d" -o ${OBJECTDIR}/py/objclosure.o py/objclosure.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objdict.o: py/objdict.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objdict.o.d 
	@${RM} ${OBJECTDIR}/py/objdict.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objdict.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objdict.o.d" -o ${OBJECTDIR}/py/objdict.o py/objdict.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objexcept.o: py/objexcept.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objexcept.o.d 
	@${RM} ${OBJECTDIR}/py/objexcept.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objexcept.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objexcept.o.d" -o ${OBJECTDIR}/py/objexcept.o py/objexcept.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objfun.o: py/objfun.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objfun.o.d 
	@${RM} ${OBJECTDIR}/py/objfun.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objfun.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objfun.o.d" -o ${OBJECTDIR}/py/objfun.o py/objfun.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objgenerator.o: py/objgenerator.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objgenerator.o.d 
	@${RM} ${OBJECTDIR}/py/objgenerator.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objgenerator.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objgenerator.o.d" -o ${OBJECTDIR}/py/objgenerator.o py/objgenerator.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objgetitemiter.o: py/objgetitemiter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objgetitemiter.o.d 
	@${RM} ${OBJECTDIR}/py/objgetitemiter.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objgetitemiter.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objgetitemiter.o.d" -o ${OBJECTDIR}/py/objgetitemiter.o py/objgetitemiter.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objint.o: py/objint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objint.o.d 
	@${RM} ${OBJECTDIR}/py/objint.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objint.o.d" -o ${OBJECTDIR}/py/objint.o py/objint.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objlist.o: py/objlist.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objlist.o.d 
	@${RM} ${OBJECTDIR}/py/objlist.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objlist.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objlist.o.d" -o ${OBJECTDIR}/py/objlist.o py/objlist.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objmap.o: py/objmap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objmap.o.d 
	@${RM} ${OBJECTDIR}/py/objmap.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objmap.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objmap.o.d" -o ${OBJECTDIR}/py/objmap.o py/objmap.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objmodule.o: py/objmodule.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objmodule.o.d 
	@${RM} ${OBJECTDIR}/py/objmodule.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objmodule.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objmodule.o.d" -o ${OBJECTDIR}/py/objmodule.o py/objmodule.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objnone.o: py/objnone.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objnone.o.d 
	@${RM} ${OBJECTDIR}/py/objnone.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objnone.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objnone.o.d" -o ${OBJECTDIR}/py/objnone.o py/objnone.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objobject.o: py/objobject.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objobject.o.d 
	@${RM} ${OBJECTDIR}/py/objobject.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objobject.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objobject.o.d" -o ${OBJECTDIR}/py/objobject.o py/objobject.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objpolyiter.o: py/objpolyiter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objpolyiter.o.d 
	@${RM} ${OBJECTDIR}/py/objpolyiter.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objpolyiter.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objpolyiter.o.d" -o ${OBJECTDIR}/py/objpolyiter.o py/objpolyiter.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objrange.o: py/objrange.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objrange.o.d 
	@${RM} ${OBJECTDIR}/py/objrange.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objrange.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objrange.o.d" -o ${OBJECTDIR}/py/objrange.o py/objrange.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objsingleton.o: py/objsingleton.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objsingleton.o.d 
	@${RM} ${OBJECTDIR}/py/objsingleton.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objsingleton.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objsingleton.o.d" -o ${OBJECTDIR}/py/objsingleton.o py/objsingleton.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objstr.o: py/objstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objstr.o.d 
	@${RM} ${OBJECTDIR}/py/objstr.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objstr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objstr.o.d" -o ${OBJECTDIR}/py/objstr.o py/objstr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objtuple.o: py/objtuple.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objtuple.o.d 
	@${RM} ${OBJECTDIR}/py/objtuple.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objtuple.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objtuple.o.d" -o ${OBJECTDIR}/py/objtuple.o py/objtuple.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objtype.o: py/objtype.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objtype.o.d 
	@${RM} ${OBJECTDIR}/py/objtype.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objtype.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objtype.o.d" -o ${OBJECTDIR}/py/objtype.o py/objtype.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objzip.o: py/objzip.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objzip.o.d 
	@${RM} ${OBJECTDIR}/py/objzip.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objzip.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objzip.o.d" -o ${OBJECTDIR}/py/objzip.o py/objzip.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/opmethods.o: py/opmethods.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/opmethods.o.d 
	@${RM} ${OBJECTDIR}/py/opmethods.o 
	@${FIXDEPS} "${OBJECTDIR}/py/opmethods.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/opmethods.o.d" -o ${OBJECTDIR}/py/opmethods.o py/opmethods.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/parse.o: py/parse.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/parse.o.d 
	@${RM} ${OBJECTDIR}/py/parse.o 
	@${FIXDEPS} "${OBJECTDIR}/py/parse.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/parse.o.d" -o ${OBJECTDIR}/py/parse.o py/parse.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/parsenum.o: py/parsenum.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/parsenum.o.d 
	@${RM} ${OBJECTDIR}/py/parsenum.o 
	@${FIXDEPS} "${OBJECTDIR}/py/parsenum.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/parsenum.o.d" -o ${OBJECTDIR}/py/parsenum.o py/parsenum.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/parsenumbase.o: py/parsenumbase.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/parsenumbase.o.d 
	@${RM} ${OBJECTDIR}/py/parsenumbase.o 
	@${FIXDEPS} "${OBJECTDIR}/py/parsenumbase.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/parsenumbase.o.d" -o ${OBJECTDIR}/py/parsenumbase.o py/parsenumbase.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/qstr.o: py/qstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/qstr.o.d 
	@${RM} ${OBJECTDIR}/py/qstr.o 
	@${FIXDEPS} "${OBJECTDIR}/py/qstr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/qstr.o.d" -o ${OBJECTDIR}/py/qstr.o py/qstr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/reader.o: py/reader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/reader.o.d 
	@${RM} ${OBJECTDIR}/py/reader.o 
	@${FIXDEPS} "${OBJECTDIR}/py/reader.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/reader.o.d" -o ${OBJECTDIR}/py/reader.o py/reader.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/repl.o: py/repl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/repl.o.d 
	@${RM} ${OBJECTDIR}/py/repl.o 
	@${FIXDEPS} "${OBJECTDIR}/py/repl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/repl.o.d" -o ${OBJECTDIR}/py/repl.o py/repl.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/runtime.o: py/runtime.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/runtime.o.d 
	@${RM} ${OBJECTDIR}/py/runtime.o 
	@${FIXDEPS} "${OBJECTDIR}/py/runtime.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/runtime.o.d" -o ${OBJECTDIR}/py/runtime.o py/runtime.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/scope.o: py/scope.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/scope.o.d 
	@${RM} ${OBJECTDIR}/py/scope.o 
	@${FIXDEPS} "${OBJECTDIR}/py/scope.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/scope.o.d" -o ${OBJECTDIR}/py/scope.o py/scope.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/sequence.o: py/sequence.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/sequence.o.d 
	@${RM} ${OBJECTDIR}/py/sequence.o 
	@${FIXDEPS} "${OBJECTDIR}/py/sequence.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/sequence.o.d" -o ${OBJECTDIR}/py/sequence.o py/sequence.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/smallint.o: py/smallint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/smallint.o.d 
	@${RM} ${OBJECTDIR}/py/smallint.o 
	@${FIXDEPS} "${OBJECTDIR}/py/smallint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/smallint.o.d" -o ${OBJECTDIR}/py/smallint.o py/smallint.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/stackctrl.o: py/stackctrl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/stackctrl.o.d 
	@${RM} ${OBJECTDIR}/py/stackctrl.o 
	@${FIXDEPS} "${OBJECTDIR}/py/stackctrl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/stackctrl.o.d" -o ${OBJECTDIR}/py/stackctrl.o py/stackctrl.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/stream.o: py/stream.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/stream.o.d 
	@${RM} ${OBJECTDIR}/py/stream.o 
	@${FIXDEPS} "${OBJECTDIR}/py/stream.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/stream.o.d" -o ${OBJECTDIR}/py/stream.o py/stream.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/unicode.o: py/unicode.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/unicode.o.d 
	@${RM} ${OBJECTDIR}/py/unicode.o 
	@${FIXDEPS} "${OBJECTDIR}/py/unicode.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/unicode.o.d" -o ${OBJECTDIR}/py/unicode.o py/unicode.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/vm.o: py/vm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/vm.o.d 
	@${RM} ${OBJECTDIR}/py/vm.o 
	@${FIXDEPS} "${OBJECTDIR}/py/vm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/vm.o.d" -o ${OBJECTDIR}/py/vm.o py/vm.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/vstr.o: py/vstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/vstr.o.d 
	@${RM} ${OBJECTDIR}/py/vstr.o 
	@${FIXDEPS} "${OBJECTDIR}/py/vstr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/vstr.o.d" -o ${OBJECTDIR}/py/vstr.o py/vstr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/modgc.o: py/modgc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/modgc.o.d 
	@${RM} ${OBJECTDIR}/py/modgc.o 
	@${FIXDEPS} "${OBJECTDIR}/py/modgc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/modgc.o.d" -o ${OBJECTDIR}/py/modgc.o py/modgc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/emitnative.o: py/emitnative.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/emitnative.o.d 
	@${RM} ${OBJECTDIR}/py/emitnative.o 
	@${FIXDEPS} "${OBJECTDIR}/py/emitnative.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/emitnative.o.d" -o ${OBJECTDIR}/py/emitnative.o py/emitnative.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/runtime_utils.o: py/runtime_utils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/runtime_utils.o.d 
	@${RM} ${OBJECTDIR}/py/runtime_utils.o 
	@${FIXDEPS} "${OBJECTDIR}/py/runtime_utils.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/runtime_utils.o.d" -o ${OBJECTDIR}/py/runtime_utils.o py/runtime_utils.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objslice.o: py/objslice.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objslice.o.d 
	@${RM} ${OBJECTDIR}/py/objslice.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objslice.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objslice.o.d" -o ${OBJECTDIR}/py/objslice.o py/objslice.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objarray.o: py/objarray.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objarray.o.d 
	@${RM} ${OBJECTDIR}/py/objarray.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objarray.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objarray.o.d" -o ${OBJECTDIR}/py/objarray.o py/objarray.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/modarray.o: py/modarray.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/modarray.o.d 
	@${RM} ${OBJECTDIR}/py/modarray.o 
	@${FIXDEPS} "${OBJECTDIR}/py/modarray.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/modarray.o.d" -o ${OBJECTDIR}/py/modarray.o py/modarray.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/py/objint_longlong.o: py/objint_longlong.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/py" 
	@${RM} ${OBJECTDIR}/py/objint_longlong.o.d 
	@${RM} ${OBJECTDIR}/py/objint_longlong.o 
	@${FIXDEPS} "${OBJECTDIR}/py/objint_longlong.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/py/objint_longlong.o.d" -o ${OBJECTDIR}/py/objint_longlong.o py/objint_longlong.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/uart_core.o: uart_core.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/uart_core.o.d 
	@${RM} ${OBJECTDIR}/uart_core.o 
	@${FIXDEPS} "${OBJECTDIR}/uart_core.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/uart_core.o.d" -o ${OBJECTDIR}/uart_core.o uart_core.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_mon_getc.o: _mon_getc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/_mon_getc.o.d 
	@${RM} ${OBJECTDIR}/_mon_getc.o 
	@${FIXDEPS} "${OBJECTDIR}/_mon_getc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_mon_getc.o.d" -o ${OBJECTDIR}/_mon_getc.o _mon_getc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/read.o: read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/read.o.d 
	@${RM} ${OBJECTDIR}/read.o 
	@${FIXDEPS} "${OBJECTDIR}/read.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/read.o.d" -o ${OBJECTDIR}/read.o read.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/sbrk.o: sbrk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sbrk.o.d 
	@${RM} ${OBJECTDIR}/sbrk.o 
	@${FIXDEPS} "${OBJECTDIR}/sbrk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/sbrk.o.d" -o ${OBJECTDIR}/sbrk.o sbrk.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/dlsbrk.o: ../src/dlsbrk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/dlsbrk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/dlsbrk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/dlsbrk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/dlsbrk.o.d" -o ${OBJECTDIR}/_ext/1360937237/dlsbrk.o ../src/dlsbrk.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/dlmalloc.o: ../src/dlmalloc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/dlmalloc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/dlmalloc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/dlmalloc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/dlmalloc.o.d" -o ${OBJECTDIR}/_ext/1360937237/dlmalloc.o ../src/dlmalloc.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/sd_card_exec.o: ../src/sd_card_exec.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/sd_card_exec.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/sd_card_exec.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/sd_card_exec.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/sd_card_exec.o.d" -o ${OBJECTDIR}/_ext/1360937237/sd_card_exec.o ../src/sd_card_exec.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_frozen_mpy.o: _frozen_mpy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/_frozen_mpy.o.d 
	@${RM} ${OBJECTDIR}/_frozen_mpy.o 
	@${FIXDEPS} "${OBJECTDIR}/_frozen_mpy.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_frozen_mpy.o.d" -o ${OBJECTDIR}/_frozen_mpy.o _frozen_mpy.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
${OBJECTDIR}/_ext/1360937237/logbuffer.o: ../src/logbuffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/logbuffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/logbuffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/logbuffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"." -I"inc" -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -I"../src/system_config/default/bsp" -I"../../../../third_party/rtos/FreeRTOS/Source/include" -I"../../../../third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -MMD -MF "${OBJECTDIR}/_ext/1360937237/logbuffer.o.d" -o ${OBJECTDIR}/_ext/1360937237/logbuffer.o ../src/logbuffer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  --std=gnu99 -fgnu89-inline
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/micropython_gui.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/gfx/driver/processor/nano2d/libnano2d.a ../../../../bin/framework/peripheral/PIC32MZ2064DAG169_peripherals.a  linkerscript/p32MZ2064DAG169mod.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/micropython_gui.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../../../../bin/framework/gfx/driver/processor/nano2d/libnano2d.a ../../../../bin/framework/peripheral/PIC32MZ2064DAG169_peripherals.a      -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x27F   -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_PK3=1,--defsym=_min_heap_size=102400,--defsym=_min_stack_size=1024,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/micropython_gui.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/gfx/driver/processor/nano2d/libnano2d.a ../../../../bin/framework/peripheral/PIC32MZ2064DAG169_peripherals.a linkerscript/p32MZ2064DAG169mod.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/micropython_gui.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../../../../bin/framework/gfx/driver/processor/nano2d/libnano2d.a ../../../../bin/framework/peripheral/PIC32MZ2064DAG169_peripherals.a      -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=102400,--defsym=_min_stack_size=1024,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}/xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/micropython_gui.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
