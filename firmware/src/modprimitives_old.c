/*
 File Created by Module and Class Generator for Micropython
 Author: Adam Lang 
 More information about Micropython modules: 
 https://forum.micropython.org/viewtopic.php?t=3274
 http://micropython-dev-docs.readthedocs.io/en/latest/adding-module.html
*/

#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
#include "system_definitions.h"
#include "gfx/hal/inc/gfx_context.h"
#include "gfx/driver/processor/nano2d/libnano2D.h"

void mytestfunc(void){
    #define SIZE 50
    uint32_t val, val2, res;  
    GFX_PixelBuffer* mybuf;
    GFX_PixelBuffer* mybuf2;
    GFX_Rect* myrect;
    GFX_Rect* myrect2;
    //PLIB_GLCD_LayerDisable(0, 2);
    //PLIB_GLCD_LayerDisable(0, 1);
    mybuf = framebuf_calloc(1, sizeof(GFX_PixelBuffer));
    if (mybuf == NULL)
        printf("\nerror in malloc1!");
    printf("\n\rA");        
    mybuf2 = framebuf_calloc(1, sizeof(GFX_PixelBuffer));
    if (mybuf2 == NULL)
        printf("\nerror in malloc1!");
    printf("\n\rB");   
    myrect = framebuf_calloc(1, sizeof(GFX_Rect));
    if (myrect == NULL)
        printf("\nerror in malloc2!");
    printf("\n\rC");
    myrect2 = framebuf_calloc(1, sizeof(GFX_Rect));
    if (myrect2 == NULL)
        printf("\nerror in malloc2!");
    printf("\n\rD");
    myrect->height = SIZE;
    myrect->width = SIZE;
    myrect->x = 0;
    myrect->y = 0;
    
    myrect2->height = SIZE;
    myrect2->width = SIZE;
    myrect2->x = 0;
    myrect2->y = 0;

    GFX_Color * buf_add;
    GFX_Color * buf_add2;
    
    buf_add = framebuf_calloc(SIZE*SIZE, sizeof(GFX_Color));
    printf("\n\rF");
    if (buf_add == NULL)
        printf("\nerror in malloc GFX Color!");
    printf("\n\rG");
   buf_add2 = framebuf_calloc (SIZE*SIZE, sizeof(GFX_Color));
    if (buf_add2 == NULL)
        printf("\nerror in malloc GFX Color!");
    printf("\n\rH");
    res = GFX_PixelBufferCreate(SIZE, SIZE, GFX_COLOR_MODE_RGBA_8888 , buf_add, mybuf);
    if (res != GFX_SUCCESS )
        printf("\nerror in GFX_PixelBufferCreate!");
     printf("\n\rI");
  res = GFX_PixelBufferCreate(SIZE, SIZE, GFX_COLOR_MODE_RGBA_8888 , buf_add2, mybuf2);
    if (res != GFX_SUCCESS )
       printf("\nerror in GFX_PixelBufferCreate!");
    res = GFX_PixelBufferAreaFill(mybuf,  myrect , GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, GFX_COLOR_LIME));
    if (res != GFX_SUCCESS )
         printf("\nerror in GFX_PixelBufferAreaFillA!");
   printf("\n\rJ");
     
    res = GFX_PixelBufferAreaFill(mybuf2,  myrect2 ,GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, GFX_COLOR_WHITE));
    if (res != GFX_SUCCESS )
         printf("\nerror in GFX_PixelBufferAreaFillB!");
    
            // res = GFX_DrawBlit(mybuf,0,0,SIZE,SIZE,x,y);
           // res = GFX_DrawBlit(mybuf2,0,0,SIZE,SIZE,x,y);    
    
    uint32_t tmr;
    int x,y; 
    GFX_PipelineMode * testmode;
    
    
    printf("\n\rmodprimitives");
 #if 1    
    testmode = GFX_PIPELINE_GCUGPU;
    GFX_Set(GFXF_LAYER_ACTIVE, 0);
    GFX_Set(GFXF_DRAW_PIPELINE_MODE, testmode);
    GFX_Begin();
    
    tmr = SYS_TMR_TickCountGet();
    for ( x = 0; x < 10; x++){
      //  printf("x: %i ", x);
        for( y=0; y < 272-SIZE; y++){
            res = GFX_DrawBlit(mybuf2,0,0,SIZE,SIZE,x,y); 
            res = GFX_DrawBlit(mybuf,0,0,SIZE,SIZE,x,y);
            
            //GFX_DrawStretchBlit(mybuf, 0, 0, SIZE, SIZE, x, y, SIZE, SIZE);
            //GFX_DrawStretchBlit(mybuf2, 0, 0, SIZE, SIZE, x, y, SIZE, SIZE);   
        }
    } 
    printf("GFX_DrawBlit Ticks: %i with pipeline mode: %i", SYS_TMR_TickCountGet() - tmr,  testmode);
    GFX_End();
#endif    
    framebuf_free(buf_add); 
   // framebuf_free(buf_add2); 
    framebuf_free(mybuf);
   // framebuf_free(mybuf2);
    framebuf_free(myrect);
   // framebuf_free(myrect2);
   
    
    //framebuf_free(buf_add2);
    printf("\nhere");
#if 0    
    testmode = GFX_PIPELINE_GCUGPU;
    GFX_Set(GFXF_DRAW_PIPELINE_MODE, testmode);
    tmr = SYS_TMR_TickCountGet();
    int i,x2, col,w,h, y2;
    for (i = 0; i< 30; i++){
        GFX_Begin();
        x = rand() % 480;
        x2= x;
        while (x2 == x)
           x2 = rand() % 480; 
        
        y = rand() % 272;
        y2 = y;
        while (y2 == y)
            y2 = rand() % 272;
        col = rand() % 17;
        w = rand() % 200;
        h = rand() % 100;
        GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));
        GFX_DrawLine( x, y, x2, y2);
        GFX_DrawRect(x,y, w,h);
        w = rand() % 200;
        //circle can not be drawn out of display bounds
        if ( x+w > 479 || x-w < 1 || y+w > 271 || y-w < 1)
            w = GFX_Mini( GFX_Mini(479-x, x-1), GFX_Mini(271-y,y-1));
        
        col = rand() % 17;
        GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));            
        GFX_DrawCircle(x, y, w);  
        GFX_End();
    }
    printf("GFX_DrawLine GFX_DrawRect Ticks: %i with pipeline mode: %i", SYS_TMR_TickCountGet() - tmr,  testmode);
    
 #endif   
}




















typedef struct{
    uint32_t x;
    uint32_t y;
    uint32_t w;
    uint32_t h;
} rectData_t;

//Framebuffer
uint32_t * buf1;
uint32_t * buf2;
bool swapPending;
uint32_t * renderBuf;

//N2D drawBlit Test
GFX_PixelBuffer gfxbuf1;
n2d_buffer_t src_buffer, dest_buffer;
GFX_Rect n2d_dst_rect, n2d_src_rect;
uint32_t n2d_tmr;

void initN2DFramebuffers(void){
    static int count = 0;
    if (1){
        // craft source buffer descriptor
        
        src_buffer.width = 480;
        src_buffer.height = 272;
        src_buffer.stride = 1920;
        src_buffer.format = N2D_RGBA8888;
        src_buffer.orientation = N2D_0;
        src_buffer.handle = NULL;
        src_buffer.memory = buf1;     
        src_buffer.gpu = KVA_TO_PA(buf1);      
        printf("\n\rsrc_buffer memory: %x", src_buffer.memory);
        printf("\n\rsrc_buffer gpu: %x", src_buffer.gpu);
        printf("\n\rif: %i", IS_KVA1(buf1));
        // craft dest buffer descriptor
        dest_buffer.width = 480;
        dest_buffer.height = 272;
        dest_buffer.stride = 1920;
        dest_buffer.format = N2D_RGBA8888;
        dest_buffer.orientation = N2D_0;
        dest_buffer.handle = NULL;
        dest_buffer.memory = buf2;
        dest_buffer.gpu = KVA_TO_PA(buf2);
        printf("\n\rdest_buffer memory: %x", dest_buffer.memory);
        printf("\n\rdest_buffer gpu: %x", dest_buffer.gpu);
        n2d_dst_rect.x = 0;
        n2d_dst_rect.y = 0;
        n2d_dst_rect.width = 480;
        n2d_dst_rect.height = 272;
        n2d_src_rect.x = 0;
        n2d_src_rect.y = 0;
        n2d_src_rect.width = 480;
        n2d_src_rect.height = 272;      
        n2d_point_t np1, np2, np3, np4;
        np1.x = 10;
        np1.y = 10;
    
        np2.x = 200;
        np2.y = 200;
        n2d_line(&src_buffer,
             np1,
             np2,
             N2D_NULL,
             0xFF00FFFF,
             N2D_BLEND_NONE);
        
        np3.x = 20;
        np3.y = 20;
    
        np4.x = 220;
        np4.y = 220;
        n2d_line(&src_buffer,
             np3,
             np4,
             N2D_NULL,
             0xFF00FFFF,
             N2D_BLEND_NONE);
        
        GLCDL0BADDR = KVA_TO_PA(src_buffer.memory);
        
        
    }
    if (0){

        
        n2d_tmr = SYS_TMR_TickCountGet();
        //GFX_Set(GFXF_DRAW_PIPELINE_MODE, GFX_PIPELINE_GCUGPU);
        int ret = n2d_blit(&dest_buffer,
                 (n2d_rectangle_t*)&n2d_dst_rect,
                 &src_buffer,
                 (n2d_rectangle_t*)&n2d_src_rect,
                 N2D_BLEND_NONE);
        printf("\n\rNano2D DrawBlit Ticks: %i, ret = %i", SYS_TMR_TickCountGet() - n2d_tmr, ret);
    }
    if (0){
        
        n2d_tmr = SYS_TMR_TickCountGet();
        //GFX_Set(GFXF_DRAW_PIPELINE_MODE, GFX_PIPELINE_GCU);
        memcpy(buf1, buf2, 480*272 * sizeof(GFX_Color));
        printf("\n\rmemcpy DrawBlit Ticks: %i", SYS_TMR_TickCountGet() - n2d_tmr);
        
    }  
    count++;
}


void initFramebuffers(void){
    dlfree(buf2);
    dlfree(buf1);
    buf1 = framebuf_calloc(480*272 * sizeof(GFX_Color));   
    buf2 = framebuf_calloc(480*272 , sizeof(GFX_Color));   
    /*
    printf("GLCDL0STRIDE %x \n\r", GLCDL0STRIDE);
    printf("GLCDL0SIZE %x \n\r", GLCDL0SIZE);
    printf("GLCDRES %x \n\r", GLCDRES);
    printf("GLCDL0BADDR %x \n\r", GLCDL0BADDR);
    printf("GLCDL0RES %x \n\r", GLCDL0RES);
    printf("GLCDL0MODE %x \n\r", GLCDL0MODE);
    printf("GLCDL0START %x \n\r", GLCDL0START);    
    */
    if (buf2== NULL || buf1 == NULL)
        printf("Error initFramebuffers");
    swapPending = false;
    renderBuf = buf1;
}


void swapBuffer(void){   
    if (!swapPending || !buf1 || !buf2)
        return;

    if (GLCDL0BADDR == KVA_TO_PA(buf2)){
        GLCDL0BADDR = KVA_TO_PA(buf1);
        renderBuf = buf2;
    }else{
        GLCDL0BADDR = KVA_TO_PA(buf2);
        renderBuf = buf1;
    }
    swapPending = false;
}

// VIRTUAL FUNCTIONS
// virtual function setcolor
STATIC mp_obj_t primitives_setcolor(mp_obj_t colorval) {
    // TODO: Insert function code
    
    
    
    colorval = MP_OBJ_SMALL_INT_VALUE(colorval);
    GFX_Begin();
    GFX_Set(GFXF_DRAW_COLOR, colorval);
    GFX_End();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(primitives_setcolor_obj, primitives_setcolor);

// virtual function drawRect
STATIC mp_obj_t primitives_drawRect(mp_obj_t x, mp_obj_t y, mp_obj_t w, mp_obj_t h) {
    // TODO: Insert function code
    x = (int32_t) MP_OBJ_SMALL_INT_VALUE(x);
    y = (int32_t) MP_OBJ_SMALL_INT_VALUE(y);
    w = (int32_t) MP_OBJ_SMALL_INT_VALUE(w);
    h = (int32_t) MP_OBJ_SMALL_INT_VALUE(h);    
    GFX_Begin();
    /*
    GFX_DrawState drawstate;
    drawstate.color = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, GFX_COLOR_RED);
    drawstate.colorMode = GFX_COLOR_MODE_RGBA_8888;
    drawstate.thickness = 1;
    GFX_Rect fill;
    fill.x = x;
    fill.y = y;
    fill.height = h;
    fill.width = w;
    
    cpuDrawRect_Line(&fill, &drawstate);
    */

    GFX_DrawRect(x,y,w,h);
    GFX_End();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_4(primitives_drawRect_obj, primitives_drawRect);


void mydrawRect(int x,int y,int w,int h){
    uint32_t xpos, ypos;
    static int col = 1;
    col = (15);    
    for (ypos = 0; ypos < h; ypos++){
        renderBuf[(y+ypos)*480 + x] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col); 
        renderBuf[(y+ypos)*480 + x+w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col );
        renderBuf[(y+ypos)*480 + x] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col); 
        renderBuf[(y+ypos)*480 + x+w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col );
        
    }
    for (xpos = 0; xpos < w; xpos++){
        
        renderBuf[y*480 + x + xpos] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col); 
        renderBuf[(y+h)*480 + x + xpos] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col );
        renderBuf[y*480 + x + xpos] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col); 
        renderBuf[(y+h)*480 + x + xpos] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col );
    }
   
    
        //buf2[(y+ypos) * 480 + x ] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col+1);
       //buf2[(y+ypos) * 480 + x + w ] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col+1);
    
    //GLCDL0BADDR = KVA_TO_PA(buf2);
}


// virtual function drawLine
STATIC mp_obj_t primitives_drawLine(mp_obj_t x1, mp_obj_t y1, mp_obj_t y2, mp_obj_t x2) {
    // TODO: Insert function code
    
    
#if 1
    int m,n;
    for (m = 10; m < 400; m++){
        for (n = 10; n < 200; n++){
            
            mydrawRect(m,n,50,50);
            //GFX_Begin();
            //GFX_DrawRect(m,n,20,20);
            //GFX_End();
        }
    }
    swapPending = true;
#endif
 
#if 0

    static int col = 1;
    col = (col+1)%17;
    int w,h,m,n;
    bool switchbuf = false;
    GLCDL0BADDR = KVA_TO_PA(buf2);
    for (w = 0; w < 480; w++){
        for (h = 0; h < 272; h++){           
            buf2[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col+1);
            if (w == 0 || w == 479 || h == 0 || h == 271)
                buf2[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , col);
        }
    }
    
    
#endif     
            
#if 0
            if (switchbuf){
                //show buf2
                GLCDL0BADDR = KVA_TO_PA(buf2);
                //prepare buf1 and draw rectangle
                //memcpy(buf1, buf2, 480*272 * sizeof(GFX_Color));
                buf1[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_BLUE);
                switchbuf = false;
            }else{
                //show buf1
                GLCDL0BADDR = KVA_TO_PA(buf1);
                //prepare buf1 and draw rectangle
               //memcpy(buf2, buf1, 480*272 * sizeof(GFX_Color));
                buf2[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_BLUE);
                switchbuf = true;
            }
#endif
  //      }
   // }
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR(primitives_drawLine_obj, 4, primitives_drawLine);

// virtual function drawCircle
STATIC mp_obj_t primitives_drawCircle(mp_obj_t x, mp_obj_t y, mp_obj_t r) {
    // TODO: Insert function code
   mytestfunc();
    
    //initFramebuffers();
    
    //initN2DFramebuffers();
    
    
    
    
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_3(primitives_drawCircle_obj, primitives_drawCircle);

// END OF VIRTUAL FUNCTIONS

// MODULE GLOBAL TABLE (all classes, all static methods)
STATIC const mp_map_elem_t primitives_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_primitives) },
    { MP_OBJ_NEW_QSTR(MP_QSTR_setcolor), (mp_obj_t)&primitives_setcolor_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_drawRect), (mp_obj_t)&primitives_drawRect_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_drawLine), (mp_obj_t)&primitives_drawLine_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_drawCircle), (mp_obj_t)&primitives_drawCircle_obj},
};
STATIC MP_DEFINE_CONST_DICT(mp_module_primitives_globals, primitives_globals_table);


// ADD MODULE TO MODULE GLOBALS (do not edit)
const mp_obj_module_t mp_module_primitives = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_primitives_globals,
};
