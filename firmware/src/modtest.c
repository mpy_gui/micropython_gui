/*
 File Created by Module and Class Generator for Micropython
 Author: Adam Lang 
 More information about Micropython modules: 
 https://forum.micropython.org/viewtopic.php?t=3274
 http://micropython-dev-docs.readthedocs.io/en/latest/adding-module.html
*/

#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
#include "system_definitions.h"

// CLASSES
// class Test
const mp_obj_type_t test_TestObj_type;
typedef struct _test_Test_obj_t {
    // base represents some basic information, like type
    mp_obj_base_t base;
    // a member created by us
    int val_a;
    int val_b;
} test_Test_obj_t;

mp_obj_t test_Test_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *args) {
    // this checks the number of arguments
    // on error -> raise python exception
    mp_arg_check_num(n_args, n_kw, 2, 2, true);
    // create a new object of our C-struct type
    test_Test_obj_t *self = m_new_obj(test_Test_obj_t);
    // give it a type
    self->base.type = &test_TestObj_type;
    self->val_a = mp_obj_get_int(args[0]);
    self->val_b = mp_obj_get_int(args[1]);
    return MP_OBJ_FROM_PTR(self);
}

STATIC void test_Test_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind) {
    test_Test_obj_t *self = MP_OBJ_TO_PTR(self_in);
    printf("\n\rclass: Test, variable: val_a, type: int, value: %u", self->val_a);
    printf("\n\rclass: Test, variable: val_b, type: int, value: %u", self->val_b);
}

// class Test, method change_val_a
STATIC mp_obj_t test_Test_change_val_a(mp_obj_t self_in, mp_obj_t val) {
    test_Test_obj_t *self = MP_OBJ_TO_PTR(self_in);
    // TODO: INSERT CODE OF CLASS METHOD
    self->val_a = MP_OBJ_SMALL_INT_VALUE(val);

    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_2(test_Test_change_val_a_obj, test_Test_change_val_a);

// map class Test methods
STATIC const mp_rom_map_elem_t test_Test_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR_change_val_a), MP_ROM_PTR(&test_Test_change_val_a_obj) },
};
STATIC MP_DEFINE_CONST_DICT(test_Test_locals_dict, test_Test_locals_dict_table);

const mp_obj_type_t test_TestObj_type = {
    { &mp_type_type },
    .name = MP_QSTR_TestObj,
    .print = test_Test_print,
    .make_new = test_Test_make_new,
    .locals_dict = (mp_obj_dict_t*)&test_Test_locals_dict,
};
// end of class Test


// END OF CLASSES


// VIRTUAL FUNCTIONS
// virtual function testfunc1
STATIC mp_obj_t test_testfunc1(mp_obj_t arg1, mp_obj_t arg2) {
    // TODO: Insert function code
    printf("\n\rtestfunc1 called with arguments: %i and %i\n\r", MP_OBJ_SMALL_INT_VALUE(arg1), MP_OBJ_SMALL_INT_VALUE(arg2));

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(test_testfunc1_obj, test_testfunc1);

// virtual function testfunc2
STATIC mp_obj_t test_testfunc2(mp_obj_t arg1) {
    // TODO: Insert function code
    printf("\n\rtestfunc2 called with argument: %i\n\r", MP_OBJ_SMALL_INT_VALUE(arg1));

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(test_testfunc2_obj, test_testfunc2);

// END OF VIRTUAL FUNCTIONS

// MODULE GLOBAL TABLE (all classes, all static methods)
STATIC const mp_map_elem_t test_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_test) },
    { MP_OBJ_NEW_QSTR(MP_QSTR_testfunc1), (mp_obj_t)&test_testfunc1_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_testfunc2), (mp_obj_t)&test_testfunc2_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_TestObj), (mp_obj_t)&test_TestObj_type },
};
STATIC MP_DEFINE_CONST_DICT(mp_module_test_globals, test_globals_table);


// ADD MODULE TO MODULE GLOBALS (do not edit)
const mp_obj_module_t mp_module_test = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_test_globals,
};



/*
// TODO: Paste following code into qstrdefsport.h

Q(test)
Q(TestObj)
Q(change_val_a)
Q(testfunc1)
Q(testfunc2)

*/

/*
// TODO: Paste following code into mpconfigport.h

// included Modules:

    extern const struct _mp_obj_module_t mp_module_test;

// #define MICROPY_PORT_BUILTIN_MODULES

    { MP_OBJ_NEW_QSTR(MP_QSTR_test), (mp_obj_t)&mp_module_test },

*/