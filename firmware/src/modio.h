#ifndef _MODIO_H    /* Guard against multiple inclusion */
#define _MODIO_H

#ifdef __cplusplus
extern "C" {
#endif
#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
#include "system_definitions.h"

//--------------- Global Module definitions  ---------------------
struct isr_enabled_t{
    int touch;
};
struct isr_enabled_t isr_enabled = { .touch = false};

//--------------- Touch global definitions --------------------
#define TOUCHDATA_RINGBUF_SIZE 16
void touch_mpy_irq_handler(void);
//linked list with callback functions
typedef struct callbackFunc{
    mp_obj_t callbackfunc;
    //bool callbackflag;
    struct callbackFunc * next;
}callbackFunc_t;
callbackFunc_t * touch_head; 

typedef struct{
    uint16_t x;
    uint16_t y;
    uint16_t touchevt;    
} touchData_t;

typedef struct {
    touchData_t * buffer;
    int head;
    int tail;
    int maxLen;
} circBuf_t;

touchData_t touchData [32];
circBuf_t circBuf;

int circBufPop(circBuf_t *c, touchData_t *data);

void callbackTest(int x, int y, int evt);

    
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */


