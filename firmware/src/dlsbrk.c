/* 
 * dlsbrk.c: sbrk emulation for use with dlmalloc.
 */
#include <string.h>         //include string.h for memset
#include <sys/types.h> /* ssize_t */


static uint8_t * curbrk = 0;

// calculation of the first address (minva):
// according to system_config.h the framebuffer layers need the memory until
// adress: GFX_GLCD_LAYER2_DBL_BASEADDR  0xA8753000 and Framebuffer Size 0x177000 which is the miva Address 0x88ca000
// memory of ddr2 which can be allocated: 0x89FFFFFF - 0x88ca000 = 23 MB
// Note: If the obtained address from dlmalloc is used with nano2d, it has to be converted into a kseg1 (uncached) pointer
// using the macro __PIC32_UNCACHED_PTR, the allignment for Framebuffers to display is 0x177000
static uint8_t * minva = (uint8_t *) 0x888CA000; 		//points to the start of ddr2 sdram in kseg0 (virtual address);
static uint8_t * maxva = (uint8_t *) 0x89FFFFFF; 		//points to the end of ddr2 sdram in kseg0 (virtual address);

//static uint8_t * minva = (uint8_t *) 0xA8000000 ; 		//points to the start of ddr2 sdram in kseg1 (virtual address);
//static uint8_t * maxva = (uint8_t *) 0xA9FFFFFF ; 		//points to the end of ddr2 sdram in kseg1 (virtual address);

void *
_dlsbrk (int n)
{
    uint8_t * newbrk;
	uint8_t * p;


    if (!curbrk)
		curbrk  = minva;

    p = curbrk;
    newbrk = curbrk + n;
    if (n > 0) {
	if (newbrk > maxva) {
	    return (void *)-1;
	}
    }
    curbrk = newbrk;
    return p;
}

void *
dlsbrk (int n)
{
    void *p;

    p = _dlsbrk(n);

    /* sbrk defined to return zeroed pages */
    if ((n > 0) && (p != (void *)-1))
	memset (p, 0, n);

    return p;
}
