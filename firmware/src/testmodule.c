#include <stdint.h>
#include <unistd.h>

#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
#include "gfx/hal/inc/gfx_common.h"
#include "gfx/hal/inc/gfx_draw_rect.h"
#include "gfx/hal/inc/gfx_draw.h"
#include "gfx/hal/gfx.h"
#include "stdlib.h"
#include "gfx/hal/inc/gfx_math.h"
#include "system_definitions.h"

#include "gfx/hal/inc/gfx_default_impl.h"
#include "gfx/driver/controller/glcd/drv_gfx_glcd_static.h"

#if 0
    struct BMessage *pxRxBMessage;
    void myVSYNCCallback (void){

        if( xQueueReceive( xQueueFramebuf, &( pxRxBMessage ), ( TickType_t ) 0 ) )
        {      
            Nop();
            PLIB_GLCD_LayerBaseAddressSet(GLCD_ID_0, 0, pxRxBMessage->cur_framebuffer);
        }    
    }
#endif

extern void* framebuf_alloc(size_t);
extern void framebuf_free(void*);
extern void * framebuf_calloc(size_t , size_t);

void draw_overlay(uint32_t * ptr){
    int w,h;
    //draw overlay
    for (w = 0; w< 480; w++){
        for (h = 0; h< 272; h++){
            if (h == 0 || h == 136 || h == 271 ){
                ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_LIGHTGRAY); 
            }else if (w == 0 || w == 479 || w == 239){
                ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_LIGHTGRAY);
            }  
        }  
    }  
    
}

void fill_field(uint32_t * ptr, uint8_t fieldnum){
    int w,h;
    for (w = 0; w< 480; w++){
        for (h = 0; h< 272; h++){
                ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_BLUE); 
        }  
    }   
    static GFX_Color color = GFX_COLOR_BLACK;
    
    switch (fieldnum){
        case 1:
            for (w = 0; w< 240; w++){
                for (h = 0; h< 136; h++){
                    ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , color); 
                }  
            } 
            break;
        case 2:
            for (w = 240; w< 480; w++){
                for (h = 0; h< 136; h++){
                    ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , color); 
                }  
            } 
            break;
        case 3:
            for (w = 240; w< 480; w++){
                for (h = 136; h< 272; h++){
                    ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , color); 
                }  
            } 
            break;
        case 4:
            for (w = 0; w< 240; w++){
                for (h =  136; h< 272; h++){
                    ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , color); 
                }  
            } 
            break;
        default:
            printf("D");
            break;                 
    }
    color = (color + 1) % GFX_COLOR_LAST;
    draw_overlay(ptr);
}

STATIC mp_obj_t testmodule_gfxtest(mp_obj_t arg1, mp_obj_t arg2) {
    
//    Pipeline modes:
//    GFX_PIPELINE_SOFTWARE = 0,
//    GFX_PIPELINE_GCU      = 1,
//    GFX_PIPELINE_GPU      = 2,
//    GFX_PIPELINE_GCUGPU   = 3   
    
    int mode = MP_OBJ_SMALL_INT_VALUE(arg2);  
    int testprog = MP_OBJ_SMALL_INT_VALUE(arg1);    
    int x, x2;
    int y, y2;
    int w;
    int h;
    int col;
    int i;
    bool cached;
    i = 0;
    uint32_t res; 
    uint32_t * ptr1 = framebuf_alloc(480*272 * sizeof(GFX_Color));
    uint32_t * ptr2 = framebuf_alloc(480*272 * sizeof(GFX_Color));
    uint32_t * ptr3 = framebuf_alloc(480*272 * sizeof(GFX_Color));
    GFX_Set(GFXF_LAYER_ACTIVE, 0);
    GFX_Set(GFXF_DRAW_MODE, GFX_DRAW_LINE); 
    #define state_INIT 1
    #define state_A 2
    #define state_B 3
    #define state_C 4    
    #define state_D 5   
    uint8_t state = state_INIT;
    uint32_t * tmp_addr = (uint32_t*) GLCDL0BADDR;
    uint32_t tmr; 
    GFX_PipelineMode pipelinemode = mode;

    switch (testprog){        
        //four blinking rectangles demonstrate framebuffer mem allocation and
        //double buffering
        case 1:
            
            for (i = 0; i< 50; i++){
                switch (state){
                    case state_INIT:
                        fill_field(ptr1, 1);
                        state = state_A;
                        break;
                    case state_A:
                        GLCDL0BADDR = KVA_TO_PA(ptr1);
                        fill_field(ptr2, 2);
                        state = state_B;
                        break;
                    case state_B:
                        GLCDL0BADDR = KVA_TO_PA(ptr2);
                        fill_field(ptr1, 3);
                        state = state_C;
                        break;            
                    case state_C:
                        GLCDL0BADDR = KVA_TO_PA(ptr1);
                        fill_field(ptr2, 4);
                        state = state_D;                         
                        break;            
                    case state_D:
                        GLCDL0BADDR = KVA_TO_PA(ptr2);
                        fill_field(ptr1, 1);
                        state = state_A;                
                        break;
                    default:
                        printf("D");
                        break;
                } 
            }
            GLCDL0BADDR = (int)tmp_addr;    

        break;
        
        //draw blit performance test
        case 2:  
            //To acheive higher speed using nano2d the buffer containing the color
            //data must be uncached. But uncached buffer adresses result in less
            //performance when using cpudrawblit. If the buffer is cached, nano2d
            //drawblit is not supported by the hardware and the cpudrawblit function
            //is called internally
            cached = false;
            
            printf("\n\r Draw Blit test");
            #define SIZE 50
            GFX_PixelBuffer* mybuf = dlmalloc(sizeof(GFX_PixelBuffer));
            if (mybuf == NULL)
                printf("\nerror in malloc1!");

            GFX_PixelBuffer* mybuf2 = dlmalloc(sizeof(GFX_PixelBuffer));
            if (mybuf2 == NULL)
                printf("\nerror in malloc1!");

            GFX_Rect* myrect = dlmalloc(sizeof(GFX_Rect));
            if (myrect == NULL)
                printf("\nerror in malloc2!");

            GFX_Rect* myrect2 = dlmalloc(sizeof(GFX_Rect));
            if (myrect2 == NULL)
                printf("\nerror in malloc2!");

            myrect->height = SIZE;
            myrect->width = SIZE;
            myrect->x = 0;
            myrect->y = 0;

            myrect2->height = SIZE;
            myrect2->width = SIZE;
            myrect2->x = 0;
            myrect2->y = 0;

            GFX_Color *buf_add;
            GFX_Color *buf_add2;
            if (cached){
                buf_add = dlmalloc (SIZE*SIZE* sizeof(GFX_Color));
                buf_add2 = dlmalloc (SIZE*SIZE* sizeof(GFX_Color));
            }else{
                buf_add = KVA0_TO_KVA1(dlmalloc (SIZE*SIZE* sizeof(GFX_Color)));
                buf_add2 = KVA0_TO_KVA1(dlmalloc (SIZE*SIZE* sizeof(GFX_Color)));              
            }
            
            if (buf_add == NULL)
                printf("\nerror in malloc GFX Color!");
            
            if (buf_add2 == NULL)
                printf("\nerror in malloc GFX Color!");

            res = GFX_PixelBufferCreate(SIZE, SIZE, GFX_COLOR_MODE_RGBA_8888 , buf_add, mybuf);
            if (res != GFX_SUCCESS )
                printf("\nerror in GFX_PixelBufferCreate!");

            res = GFX_PixelBufferCreate(SIZE, SIZE, GFX_COLOR_MODE_RGBA_8888 , buf_add2, mybuf2);
            if (res != GFX_SUCCESS )
                printf("\nerror in GFX_PixelBufferCreate!");
            res = GFX_PixelBufferAreaFill(mybuf,  myrect ,GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, GFX_COLOR_LIME));
            if (res != GFX_SUCCESS )
                 printf("\nerror in GFX_PixelBufferAreaFill!");

            res = GFX_PixelBufferAreaFill(mybuf2,  myrect2 ,GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, GFX_COLOR_WHITE));
            if (res != GFX_SUCCESS )
                 printf("\nerror in GFX_PixelBufferAreaFill!");

            GFX_Set(GFXF_DRAW_PIPELINE_MODE, pipelinemode);
            
            GFX_Begin();
            tmr = SYS_TMR_TickCountGet();
            for ( x = 0; x < 10; x++){
                for( y=0; y < 272-SIZE; y++){
                    res = GFX_DrawBlit(mybuf,0,0,SIZE,SIZE,x,y);
                    res = GFX_DrawBlit(mybuf2,0,0,SIZE,SIZE,x,y);
                    if (res != GFX_SUCCESS ){
                        printf("\nerror in GFX_DrawBlit!");
                        break;
                    }       
                }
            }
            GFX_End();
            printf("\n\r Ticks: %i using pipeline mode: %i\n\r", SYS_TMR_TickCountGet() - tmr, pipelinemode );
        break;
        
        // random lines
        case 3:
            printf("\n\rRandom Lines");      
            GFX_Set(GFXF_DRAW_PIPELINE_MODE, pipelinemode);            
            tmr = SYS_TMR_TickCountGet();           
            for (i = 0; i< 300; i++){
                GFX_Begin();
                x = rand() % 480;
                x2 = x;
                while (x2 == x)
                   x2 = rand() % 480; 

                y = rand() % 272;
                y2 = y;
                while (y2 == y)
                    y2 = rand() % 272;
                col = rand() % 17;

                GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));
                GFX_DrawLine( x, y, x2, y2);

                w = rand() % 200; 
                GFX_End();
            }
            printf("\n\r Ticks: %i using pipeline mode: %i\n\r",SYS_TMR_TickCountGet() - tmr, pipelinemode );
        break;
        // random rectangles
        case 4:
            printf("\n\rRandom Rectangles");      
            GFX_Set(GFXF_DRAW_PIPELINE_MODE, pipelinemode);            
            tmr = SYS_TMR_TickCountGet();           
            for (i = 0; i< 300; i++){
                GFX_Begin();
                x = rand() % 480;
                x2= x;
                while (x2 == x)
                   x2 = rand() % 480; 
                y = rand() % 272;
                y2 = y;
                while (y2 == y)
                    y2 = rand() % 272;
                col = rand() % 17;
                w = rand() % 200;
                h = rand() % 100;
                GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));
                GFX_DrawRect(x,y, w,h);  
                GFX_End();
            }
            printf("\n\r Ticks: %i using pipeline mode: %i\n\r",SYS_TMR_TickCountGet() - tmr, pipelinemode );
        break;
        //random circles
        case 5:
            printf("\n\rRandom Rectangle, Lines and Circles");      
            GFX_Set(GFXF_DRAW_PIPELINE_MODE, pipelinemode);           
            tmr = SYS_TMR_TickCountGet();           
            for (i = 0; i< 300; i++){
                GFX_Begin();
                x = rand() % 480;
                y = rand() % 272;
                col = rand() % 17;
                GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));
                w = rand() % 200;
                //circle could not be drawn out of display bounds
                if ( x+w > 479 || x-w < 1 || y+w > 271 || y-w < 1)
                    w = GFX_Mini( GFX_Mini(479-x, x-1), GFX_Mini(271-y,y-1));         
                GFX_DrawCircle(x, y, w);  
                GFX_End();
            }
            printf("\n\r Ticks: %i using pipeline mode: %i\n\r",SYS_TMR_TickCountGet() - tmr, pipelinemode );
            break;
        default:
            printf("\n\r Please chose a test from 1-5 with pipeline mode 0-3\n\r");
            break;
            
    }

    framebuf_free(ptr1);
    framebuf_free(ptr2);
    framebuf_free(ptr3);
//  printf("\r\nGLCDL0STRIDE %x",GLCDL0STRIDE);
//  printf("\r\nGLCDL0RES %x",GLCDL0RES);
//  printf("\r\nGLCDL0SIZE %x",GLCDL0SIZE);
//  printf("\r\nGLCDL0START %x",GLCDL0START);
//  printf ("\n\rGLCDL0MODE %x", GLCDL0MODE);
//  printf("\n\rframebuffer GLCDL0BADDR: %x\n\r", GLCDL0BADDR);
//  Set background color Blue
//  GLCDBGCOLOR = 0xF000;
  return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(testmodule_gfxtest_obj, testmodule_gfxtest);

STATIC const mp_map_elem_t testmodule_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_testmodule) },
    { MP_OBJ_NEW_QSTR(MP_QSTR_gfxtest), (mp_obj_t)&testmodule_gfxtest_obj },
};

STATIC MP_DEFINE_CONST_DICT (
    mp_module_testmodule_globals,
    testmodule_globals_table
);

const mp_obj_module_t mp_module_testmodule = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_testmodule_globals,
};