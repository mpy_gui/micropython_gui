
#ifndef _LOGBUFFER_H    /* Guard against multiple inclusion */
#define _LOGBUFFER_H

#define LOGBUFSIZE 512

int logbuf_reset();
int logbuf_putc(char ch);
char logbuf_getc();




#endif /* _LOGBUFFER_H */
