/*
 File Created by Module and Class Generator for Micropython
 Author: Adam Lang 
 More information about Micropython modules: 
 https://forum.micropython.org/viewtopic.php?t=3274
 http://micropython-dev-docs.readthedocs.io/en/latest/adding-module.html
*/

#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
#include "system_definitions.h"
#include "py/objint.h"

#include "../framework/gfx/driver/processor/nano2d/libnano2D.h"
#include "../framework/gfx/driver/processor/nano2d/libnano2D_types.h"
//      --------------
//      MPY GFX Module
//      --------------
//      Functions:
//      mp_obj_t gfx_set_color(mp_obj_t colorval)
//      mp_obj_t gfx_get_color() 
//      mp_obj_t gfx_randint(mp_obj_t from, mp_obj_t to)
//      mp_obj_t gfx_fill_screen(mp_obj_t col)
//      mp_obj_t gfx_set_background_color(mp_obj_t col)
//      mp_obj_t gfx_set_thickness(mp_obj_t value)
//      mp_obj_t gfx_get_thickness()
//      mp_obj_t gfx_draw_circle(mp_obj_t x, mp_obj_t y, mp_obj_t radius) 
//      mp_obj_t gfx_draw_line(mp_obj_t x1, mp_obj_t y1, mp_obj_t x2, mp_obj_t y2) 
//      mp_obj_t gfx_draw_pixel(mp_obj_t x, mp_obj_t y)
//      gfx_draw_rectangle(mp_obj_t x, mp_obj_t y, mp_obj_t width, mp_obj_t height)

// VIRTUAL FUNCTIONS
/*
// virtual function set_color
STATIC mp_obj_t gfx_set_color(mp_int_t colorval) {
    //Color format is RGBA, eg. red = 0xFF0000FF (alpha channel has to be FF to be visible)
    int col = mp_obj_get_int(colorval);
    GFX_Begin();
    GFX_Set(GFXF_DRAW_COLOR,col);
    GFX_End();    
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(gfx_set_color_obj, gfx_set_color);
*/
extern GFX_Color defDrawColorGet(void);

void* framebuf_alloc(size_t bytes){
    int framebuf_alignment = 0x177000;
    return __PIC32_UNCACHED_PTR(dlmemalign(framebuf_alignment, bytes));
}

void framebuf_free(void* addr){
    dlfree(__PIC32_CACHED_PTR(addr));
}

void * framebuf_calloc(size_t n_elements, size_t elem_size){
    void* mem;
    size_t req = 0;
    if (n_elements != 0) {
        req = n_elements * elem_size;
        if (((n_elements | elem_size) & ~(size_t)0xffff) &&
            (req / n_elements != elem_size))
                req = (~(size_t)0); /* force downstream failure on overflow */
    }
  mem = framebuf_alloc(req);
  if (mem != 0 )
    memset(mem, 0, req);
  return mem;
}  

// virtual function set_color
STATIC mp_obj_t gfx_set_color(mp_int_t colorval) {
    //Color format is RGBA, eg. red = 0xFF0000FF (alpha channel has to be FF to be visible)
    //but as parameter in micropython the function accepts 0xRRGGBB as value
    //alpha channel will be set to 100%
    unsigned int col = mp_obj_get_int(colorval);
    col = col << 8 | 0xff;
    GFX_Begin();
    GFX_Set(GFXF_DRAW_COLOR,col);
    GFX_End();    
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(gfx_set_color_obj, gfx_set_color);

// virtual function get_color
STATIC mp_obj_t gfx_get_color() {
    unsigned int col = defDrawColorGet();
    col = col >> 8;
    mp_obj_t ret = mp_obj_new_int(col);
    return ret;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(gfx_get_color_obj, gfx_get_color);

// virtual function randint
STATIC mp_obj_t gfx_randint(mp_obj_t from, mp_obj_t to) {
    from = MP_OBJ_SMALL_INT_VALUE(from);
    to = MP_OBJ_SMALL_INT_VALUE(to);
    uint32_t val = rand() % (to + 1 - from) + from;
    return MP_OBJ_NEW_SMALL_INT(val);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(gfx_randint_obj, gfx_randint);


STATIC mp_obj_t gfx_fill_screen(mp_obj_t col) {
    col = MP_OBJ_SMALL_INT_VALUE(col);
    int val = (int) col;
    GFX_Begin();
    GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, val));
    GFX_End(); 
    int x,y;
    GFX_Begin();
    for (x = 0; x< 480; x++){
        for (y =0; y < 272; y++){
            GFX_DrawPixel(x,y);  
        }
    }
    GFX_End();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(gfx_fill_screen_obj, gfx_fill_screen);

STATIC mp_obj_t gfx_set_background_color(mp_obj_t col) {
    col = MP_OBJ_SMALL_INT_VALUE(col);
    int val = (int) col;
    //Set background color
    GLCDBGCOLOR = val;
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(gfx_set_background_color_obj, gfx_set_background_color);


// virtual function set_thickness
STATIC mp_obj_t gfx_set_thickness(mp_obj_t value) {
    value = MP_OBJ_SMALL_INT_VALUE(value);
    int val = (int) value;
    GFX_Begin();
    defDrawThicknessSet(val);
    GFX_End();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(gfx_set_thickness_obj, gfx_set_thickness);

// virtual function get_thickness
STATIC mp_obj_t gfx_get_thickness() {
    pyexec_frozen_module("frozentest.py");
    uint32_t val = defDrawThicknessGet();
    return MP_OBJ_NEW_SMALL_INT(val);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(gfx_get_thickness_obj, gfx_get_thickness);

// virtual function draw_circle
STATIC mp_obj_t gfx_draw_circle(mp_obj_t x, mp_obj_t y, mp_obj_t radius) {
    x = MP_OBJ_SMALL_INT_VALUE(x);
    y = MP_OBJ_SMALL_INT_VALUE(y);
    radius = MP_OBJ_SMALL_INT_VALUE(radius);  
    //check bounds
    if ( (int)x - (int)radius < 0 || (int)x + (int)radius > 479 || (int)y - (int)radius < 0 || (int)y + (int)radius > 271)
        return mp_const_none;   
    GFX_Begin();
    GFX_DrawCircle(x, y, radius);
    GFX_End();    
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_3(gfx_draw_circle_obj, gfx_draw_circle);

// virtual function draw_line
STATIC mp_obj_t gfx_draw_line(mp_obj_t x1, mp_obj_t y1, mp_obj_t x2, mp_obj_t y2) {
    x1 = MP_OBJ_SMALL_INT_VALUE(x1);
    y1 = MP_OBJ_SMALL_INT_VALUE(y1);
    x2 = MP_OBJ_SMALL_INT_VALUE(x2);  
    y2 = MP_OBJ_SMALL_INT_VALUE(y2);
    GFX_Begin();
    GFX_DrawLine(x1, y1, x2, y2);
    GFX_End();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_4(gfx_draw_line_obj, gfx_draw_line);

// virtual function draw_pixel
STATIC mp_obj_t gfx_draw_pixel(mp_obj_t x, mp_obj_t y) {
    x = MP_OBJ_SMALL_INT_VALUE(x);
    y = MP_OBJ_SMALL_INT_VALUE(y);
    GFX_Begin();
    GFX_DrawPixel(x,y);
    GFX_End();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(gfx_draw_pixel_obj, gfx_draw_pixel);

// virtual function draw_rectangle
STATIC mp_obj_t gfx_draw_rectangle(mp_obj_t x, mp_obj_t y, mp_obj_t width, mp_obj_t height) {
    x = MP_OBJ_SMALL_INT_VALUE(x);
    y = MP_OBJ_SMALL_INT_VALUE(y);
    width = MP_OBJ_SMALL_INT_VALUE(width);  
    height = MP_OBJ_SMALL_INT_VALUE(height);
    GFX_Begin();
    GFX_DrawRect(x, y, width, height);
    GFX_End();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_4(gfx_draw_rectangle_obj, gfx_draw_rectangle);


// virtual function draw_rectangle
STATIC mp_obj_t gfx_draw_rectangle_fill(mp_obj_t x, mp_obj_t y, mp_obj_t width, mp_obj_t height) {
    x = MP_OBJ_SMALL_INT_VALUE(x);
    y = MP_OBJ_SMALL_INT_VALUE(y);
    width =  MP_OBJ_SMALL_INT_VALUE(width);  
    height = MP_OBJ_SMALL_INT_VALUE(height);
    n2d_buffer_t src_buffer, dest_buffer;
    //GFX_Rect dest_rect, srcRect;
    GFX_Color * gfx_col_buf;   
    unsigned int col = defDrawColorGet();
    gfx_col_buf = __PIC32_UNCACHED_PTR(dlmalloc ( (int)width * sizeof(GFX_Color)));
    //printf("\n\r width: %i height: %i, x: %i, y: %i", width, height, x,y);
    for (int w = 0; w < (int)width; w++)
        gfx_col_buf[(int)w] = col; 
    
    for (int h = 0; h < (int)height; h++)
        memcpy(PA_TO_KVA1(GLCDL0BADDR + (int)x * sizeof(GFX_Color)+ (h+(int)y)*480*sizeof(GFX_Color)), gfx_col_buf, (int) width * sizeof(GFX_Color));
    
    dlfree(__PIC32_CACHED_PTR(gfx_col_buf));
    
#if 0    
    // craft source buffer descriptor
    src_buffer.width = width;
    src_buffer.height = height;
    src_buffer.stride = (int)width * GFX_ColorInfo[N2D_RGBA8888].size;
    printf("\n\rstride: %i", src_buffer.stride);
    src_buffer.format = N2D_RGBA8888;
    src_buffer.orientation = N2D_0;
    src_buffer.handle = NULL;
    src_buffer.memory = gfx_col_buf;
    src_buffer.gpu = KVA_TO_PA(gfx_col_buf);
       
    // craft dest buffer descriptor
    dest_buffer.width = width;
    dest_buffer.height = height;
    dest_buffer.stride = (int)width * GFX_ColorInfo[N2D_RGBA8888].size;
    dest_buffer.format = N2D_RGBA8888;
    dest_buffer.orientation = N2D_0;
    dest_buffer.handle = NULL;
    dest_buffer.memory = PA_TO_KVA1(GLCDL0BADDR);
    dest_buffer.gpu = GLCDL0BADDR;
    
    if(IS_KVA1(src_buffer.memory) == GFX_FALSE)
        printf("\n\rERR_A");
    if (GFX_COLOR_MODE_IS_INDEX(dest_buffer.format) == GFX_TRUE)
        printf("\n\rERR_B");
    if (GFX_COLOR_MODE_IS_INDEX(src_buffer.format) == GFX_TRUE)
        printf("\n\rERROR HERE");
    
    dest_rect.x = x;
    dest_rect.y = y;
    dest_rect.width = width;
    dest_rect.height = height;
    int ret = n2d_blit(&dest_buffer,
             (n2d_rectangle_t*)&dest_rect,
             &src_buffer,
             (n2d_rectangle_t*)&srcRect,
             N2D_BLEND_NONE);
    printf("\r\nret: %i",ret);
#endif
    //gfx_col_buf[(int)h*(int)width + (int)w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_MAGENTA);

    //memcpy(PA_TO_KVA1(GLCDL0BADDR), gfx_col_buf, (int)width* (int)height* sizeof(GFX_Color));
    //GLCDL0BADDR = src_buffer.gpu;
    
#if 0    
    GFX_Begin();
    GFX_PixelBuffer* gfx_pixbuf;
    gfx_pixbuf = dlmalloc(sizeof(GFX_PixelBuffer));
    if (gfx_pixbuf == NULL)
        return mp_const_none;
    GFX_Rect* myrect;
    myrect = dlmalloc(sizeof(GFX_Rect));
    if (myrect == NULL){
        printf("\n\rERR_1");
        dlfree(gfx_pixbuf);
        return mp_const_none;
    }
    myrect->height = height;
    myrect->width = width;
    myrect->x = x;
    myrect->y = y;
    GFX_Color * gfx_col_buf;    
    gfx_col_buf = dlmalloc ( (int)width * (int)height * sizeof(GFX_Color));
    if (gfx_col_buf == NULL){
        printf("\n\rERR_2");
        dlfree(gfx_pixbuf);
        dlfree(myrect);
        return mp_const_none;
    }
    int res = GFX_PixelBufferCreate((int)width, (int)height, GFX_COLOR_MODE_RGBA_8888 , gfx_col_buf, gfx_pixbuf);
    if (res != GFX_SUCCESS ){
        printf("\n\rERR_3");
        dlfree(gfx_pixbuf);
        dlfree(gfx_col_buf);
        dlfree(myrect);
        return mp_const_none;
    }
    GFX_Color color = defDrawColorGet();
    res = GFX_PixelBufferAreaFill(gfx_pixbuf,  myrect , color);
    //res = GFX_PixelBufferAreaFill(gfx_pixbuf,  myrect ,GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, GFX_COLOR_RED));
    if (res != GFX_SUCCESS ){
        printf("\n\rERR_4, x: %i y:%i w:%i h:%i col:%x", x, y, (int)width, (int)height, color);
        dlfree(gfx_pixbuf);
        dlfree(gfx_col_buf);
        dlfree(myrect);
        return mp_const_none;
    }
    GFX_End();
    
    GFX_Set(GFXF_DRAW_PIPELINE_MODE, GFX_PIPELINE_GCUGPU);
    GFX_Begin();
    res = GFX_DrawBlit(gfx_pixbuf,0,0,width,height,x,y);
    if (res != GFX_SUCCESS )
        printf("\n\rERR_5");
    GFX_End();
    GFX_Set(GFXF_DRAW_PIPELINE_MODE, GFX_PIPELINE_GCU);    
    dlfree(gfx_pixbuf);
    dlfree(gfx_col_buf);
    dlfree(myrect);
#endif
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_4(gfx_draw_rectangle_fill_obj, gfx_draw_rectangle_fill);



// END OF VIRTUAL FUNCTIONS

// MODULE GLOBAL TABLE (all classes, all static methods)
STATIC const mp_map_elem_t gfx_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_gfx) },
    { MP_OBJ_NEW_QSTR(MP_QSTR_set_color), (mp_obj_t)&gfx_set_color_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_get_color), (mp_obj_t)&gfx_get_color_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_set_thickness), (mp_obj_t)&gfx_set_thickness_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_get_thickness), (mp_obj_t)&gfx_get_thickness_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_draw_circle), (mp_obj_t)&gfx_draw_circle_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_draw_line), (mp_obj_t)&gfx_draw_line_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_draw_pixel), (mp_obj_t)&gfx_draw_pixel_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_draw_rectangle), (mp_obj_t)&gfx_draw_rectangle_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_draw_rectangle_fill), (mp_obj_t)&gfx_draw_rectangle_fill_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_randint), (mp_obj_t)&gfx_randint_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_fill_screen), (mp_obj_t)&gfx_fill_screen_obj}, 
    { MP_OBJ_NEW_QSTR(MP_QSTR_set_background_color), (mp_obj_t)&gfx_set_background_color_obj}, 
};
STATIC MP_DEFINE_CONST_DICT(mp_module_gfx_globals, gfx_globals_table);

// ADD MODULE TO MODULE GLOBALS (do not edit)
const mp_obj_module_t mp_module_gfx = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_gfx_globals,
};



/*
// TODO: Paste following code into qstrdefsport.h

Q(gfx)
Q(set_color)
Q(get_color)
Q(set_thickness)
Q(get_thickness)
Q(draw_circle)
Q(draw_line)
Q(draw_pixel)
Q(draw_rectangle)

*/

/*
// TODO: Paste following code into mpconfigport.h

// included Modules:

    extern const struct _mp_obj_module_t mp_module_gfx;

// #define MICROPY_PORT_BUILTIN_MODULES

    { MP_OBJ_NEW_QSTR(MP_QSTR_gfx), (mp_obj_t)&mp_module_gfx },

*/