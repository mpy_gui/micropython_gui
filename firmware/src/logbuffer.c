
#include "logbuffer.h"
#include "system_config/default/system_definitions.h"


char logbuffer[LOGBUFSIZE];
static int writePos = 0;
static int available = 0;
static int capacity = LOGBUFSIZE;

int logbuf_reset()
{
    if ( xSemaphoreTake( xLogbufSem, ( TickType_t )1 ) == pdTRUE ){
        memset(logbuffer, '\0', LOGBUFSIZE);
        writePos = 0;
        available = 0;
        xSemaphoreGive(xLogbufSem);
        return 0;
    }else{
        //semaphore not obtained
        return -1;
    }
}

int logbuf_putc(char ch) {
    int ret = -1;
    if ( xSemaphoreTake( xLogbufSem, ( TickType_t )1 ) == pdTRUE ){
        if(available < capacity){
            if(writePos >= capacity){
                writePos = 0;
            }
            logbuffer[writePos] = ch;
            writePos++;
            available++;
            ret = 0;
        }
        xSemaphoreGive(xLogbufSem);
    }else{
        //semaphore not obtained
        //printf("=");
    }
    return ret;
}

char logbuf_getc() {
    char ret = -1;
    if ( xSemaphoreTake( xReplSem, ( TickType_t )1 ) == pdTRUE ){
        if(available != 0){
            int nextSlot = writePos - available;
                if(nextSlot < 0){
                    nextSlot += capacity;
                }
                ret = logbuffer[nextSlot];
                available--;
        }
			//now release Semaphore to give access to shared ringbuffer
			xSemaphoreGive(xReplSem);
		}else{
            //semaphore not obtained
            //printf("_");
        }
    return ret;
}