/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    hwapp.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

#include "system_definitions.h"


#include "py/runtime.h"


// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "hwapp.h"
#include "modio.h"
#include "py/obj.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

HWAPP_DATA hwappData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void HWAPP_Initialize ( void )

  Remarks:
    See prototype in hwapp.h.
 */
static int prevVSYNCFLAG;

void HWAPP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    hwappData.state = HWAPP_STATE_INIT;

    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}


/******************************************************************************
  Function:
    void HWAPP_Tasks ( void )

  Remarks:
    See prototype in hwapp.h.
 */
//struct AMessage *pxRxedMessage;
     
extern void sd_card_switch_func(void);

void HWAPP_Tasks ( void )
{

    /* Check the application's current state. */
    switch ( hwappData.state )
    {
        /* Application's initial state. */
        case HWAPP_STATE_INIT:
        {
            bool appInitialized = true;       
            if (appInitialized)
            {            
                hwappData.state = HWAPP_STATE_SERVICE_TASKS;
            }
            break;
        }

        case HWAPP_STATE_SERVICE_TASKS:
        {
            if (BSP_SwitchStateGet(BSP_SWITCH_S1) == BSP_SWITCH_STATE_PRESSED)
                sd_card_switch_func();
 
#ifdef LEDTEST
            static int led_pos = 0;

            if (led_pos)
            {                    
                switch (pxRxedMessage->ucData[--led_pos] - 48){
                    case 1:
                        BSP_LEDToggle(BSP_LED_1);
                        break;
                    case 2:
                        BSP_LEDToggle(BSP_LED_2);
                        break;
                    case 3:
                        BSP_LEDToggle(BSP_LED_3);
                        break;
                    default:
                        break;        
                }           
            }
            else if( xQueue != 0 )
            {
                // Receive a message on the created queue.  Block for 10 ticks if a
                // message is not immediately available.
                if( xQueueReceive( xQueue, &( pxRxedMessage ), ( TickType_t ) 10 ) )
                {
                    printf("Received: %s \n\r",pxRxedMessage->ucData);
                    led_pos = strlen(pxRxedMessage->ucData);     
                    /*
                    if (pxRxedMessage->ucMessageID == 1){
                            BSP_LEDToggle(BSP_LED_1);
                    }
                    printf("\nTicks after: %i", SYS_TMR_TickCountGet());
                    */        
                }
                
            }
            
#endif
            
#if 0
            //Handle Touch events
            if (isr_enabled.touch){
                callbackFunc_t * current = touch_head;     
                touchData_t tmptouchData;
        NEXT_DATASET:
                isr_enabled.touch = false;
                if (!circBufPop(&circBuf, &tmptouchData)){
                    isr_enabled.touch = true;
                    //element from buffer obtained
                    //now call all listeners with this dataset
                     while (current != NULL){
                        if (current->callbackflag){
                            current->callbackflag = false;
                            //mp_call_function_0(current->callbackfunc);
                            //printf("\n\rX: %i Y: %i E: %i", tmptouchData.x, tmptouchData.y, tmptouchData.touchevt);  
                            mp_call_function_2(MP_OBJ_FROM_PTR(current->callbackfunc), MP_OBJ_NEW_SMALL_INT(tmptouchData.x), MP_OBJ_NEW_SMALL_INT(tmptouchData.y));    
                        }
                        current = current->next;
                     }
                    goto NEXT_DATASET;
                }else{
                    //buffer is empty
                    isr_enabled.touch  = true;              
                }                               
            }
#endif 
#if 0            
            if ( !GLCDSTATbits.ACTIVE && GLCDSTATbits.ACTIVE != prevVSYNCFLAG)
                swapBuffer();
            prevVSYNCFLAG = GLCDSTATbits.ACTIVE;
#endif            
            
        break; 
        }
      
        /* TODO: implement your application state machine.*/
        

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}

 

/*******************************************************************************
 End of File
 */
