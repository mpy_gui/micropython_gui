#include "../../../../framework/system/fs/sys_fs.h"
#include "system_definitions.h"
#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "lib/utils/pyexec.h"


bool sd_card_initfs (void){
    if(SYS_FS_Mount("/dev/mmcblka1", "/sd", FAT, 0, NULL) == 0){
        //mounting successful
        return true;
    }else{
        return false;
    }   
}

void sd_card_release(void){
    SYS_FS_Unmount("/sd");
}

void sd_card_exec_main(void){
    static bool mp_initialized;
    sd_card_release();
    if (!sd_card_initfs()){
        return;
    }
    printf("\n\rRunning main.py from sd card... \n\r\n\r");
    const char *main_py;
    main_py = "main.py";
    pyexec_file(main_py);
    printf("\n\rFILE EXEC FINISHED\n\r");
    Nop();
}

void sd_card_switch_func(void){
    static bool toggle = false;
    if (!toggle){
        sd_card_exec_main();
        toggle = true;
    }else{
        sd_card_release();
        toggle = false;
    }
}

