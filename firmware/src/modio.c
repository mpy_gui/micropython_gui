/*
 File Created by Module and Class Generator for Micropython
 Author: Adam Lang 
 More information about Micropython modules: 
 https://forum.micropython.org/viewtopic.php?t=3274
 http://micropython-dev-docs.readthedocs.io/en/latest/adding-module.html
*/

#include "modio.h"
#include "py/gc.h"
//#include "../src/../framework/system/touch/src/sys_touch_local.h"
//#include "../src/../framework/system/touch/sys_touch.h"
#include <system/touch/sys_touch.h>
#include <system/touch/src/sys_touch_local.h>
#include "system_config/default/system_definitions.h"
//To make a function call:
//mp_call_function_0(current->func);
callbackFunc_t * touch_head; 
extern int32_t GFX_Mini (int32_t , int32_t);

void callbackTest(int x, int y, int evt){
    int w = 20;
    if ((evt == EVENT_PRESS || evt == EVENT_STILLPRESS) && x+w < 479 && x-w > 1 && y+w < 271 && y-w > 1){
        GFX_Begin();
        int col = 3;
        //int col = rand() % 17;
        GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));
        w = GFX_Mini( GFX_Mini(479-x, x-1), GFX_Mini(271-y,y-1));
        GFX_DrawCircle(x, y, w);  
        GFX_End();
    }
    if ((evt == EVENT_MOVE) && x+w < 479 && x-w > 1 && y+w < 271 && y-w > 1){
        GFX_Begin();
        int col = 7;
        //int col = rand() % 17;
        GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));
        w = GFX_Mini( GFX_Mini(479-x, x-1), GFX_Mini(271-y,y-1));
        GFX_DrawCircle(x, y, w);  
        GFX_End();
    }
    /*
    if ((evt == EVENT_RELEASE)){
        GFX_Begin();
        int col = 1;
        //int col = rand() % 17;
        GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));
        for (int i = 0; i< 480; i++)
            GFX_DrawLine(i, 0, i, 272);  
        GFX_End();
    }
    */
}

int circBufPush(circBuf_t *c, touchData_t data)
{
    // next is where head will point to after this write.
    int next = c->head + 1;
    if (next >= c->maxLen)
        next = 0;

    if (next == c->tail) // check if circular buffer is full
        return -1;       // and return with an error.

    c->buffer[c->head] = data; // Load data and then move
    c->head = next;            // head to next data offset.
    return 0;  // return success to indicate successful push.
}

int circBufPop(circBuf_t *c, touchData_t *data)
{
    // if the head isn't ahead of the tail, we don't have any characters
    if (c->head == c->tail) // check if circular buffer is empty
        return -1;          // and return with an error

    // next is where tail will point to after this read.
    int next = c->tail + 1;
    if(next >= c->maxLen)
        next = 0;

    *data = c->buffer[c->tail]; // Read data and then move
    c->tail = next;             // tail to next data offset.
    return 0;  // return success to indicate successful push.
}
/* This is the driver instance object array. */
extern SYS_TOUCH_OBJ gSysTouchObj[SYS_TOUCH_INSTANCES_NUMBER] ;

void touch_mpy_irq_handler(){
    SYS_TOUCH_OBJ *dObj = &gSysTouchObj[0];
	if (isr_enabled.touch){
		if (dObj->touchMsg->param0 != EVENT_INVALID){         
            if ( xSemaphoreTake( xTouchSem, ( TickType_t )1 ) == pdTRUE ){
                //push value of touch event into ringbuffer
                touchData_t tmptouchData;
                tmptouchData.touchevt = dObj->touchMsg->param0;
                tmptouchData.x = dObj->touchMsg->param1;
                tmptouchData.y = dObj->touchMsg->param2;
                if (circBufPush(&circBuf, tmptouchData) != -1){
                    //push element to buffer successful
                }else{                
                    //buffer was full
                    //printf("*");
                }
                xSemaphoreGive(xTouchSem);
            }else{
                //semaphore not obtained
                //printf("=");
            }
        }
	}
}

static mp_obj_t appendCallback(callbackFunc_t * head, mp_obj_t funct){
    if (head == NULL){
        printf("\n\rError: appendCallback was called with NULL pointer as head");
        return mp_const_none;
    }
    callbackFunc_t* current = head;
    callbackFunc_t* newNode;
    newNode = malloc(sizeof(callbackFunc_t));
    newNode->callbackfunc = funct;
    //newNode->callbackflag = false;
    newNode->next = NULL;
    while(current->next != NULL){
        current = current->next;
    }
    current->next = newNode; 
}    

// VIRTUAL FUNCTIONS
// virtual function registerTouchCallback
STATIC mp_obj_t io_registerTouchCallback(mp_obj_t funct) {
    if(touch_head == NULL){
        //first call, create head pointer
        touch_head = malloc(sizeof(callbackFunc_t));
        //printf("\n\rappending func at addr: %x", funct);
        touch_head->callbackfunc = funct; 
        //touch_head->callbackflag = false;
        touch_head->next = NULL;
        
        //init ringbuffer
        circBuf.buffer = &touchData[0];
        circBuf.head = 0;
        circBuf.tail = 0;
        circBuf.maxLen = TOUCHDATA_RINGBUF_SIZE;
        
    }else{
        appendCallback(touch_head, funct);
    }
   return mp_const_none;        
};
STATIC MP_DEFINE_CONST_FUN_OBJ_1(io_registerTouchCallback_obj, io_registerTouchCallback);

//this function is called periodically from the python code irqPoll
STATIC mp_obj_t io_execTouchCallbacks(void) {
	if (isr_enabled.touch == true){
		touchData_t tmptouchData;
		bool processElement = false;
		if ( xSemaphoreTake( xTouchSem, ( TickType_t )1 ) == pdTRUE ){
			if (circBufPop(&circBuf, &tmptouchData) != -1){
				//obtained one element from ringbuffer
				//element is saved in tmptouchData
				processElement = true;
			}else{
				//ringbuffer is empty
			}
			//now release Semaphore to give access to shared ringbuffer
			xSemaphoreGive(xTouchSem);
		}else{
            //semaphore not obtained
            //printf("_");
        }
		if (processElement){
			//do something with obtained dataset (call all callbacks with this dataset)
			callbackFunc_t * current = touch_head;             
            //pack arguments            
            mp_obj_t touch_args[3];
            touch_args[0] = MP_OBJ_NEW_SMALL_INT(tmptouchData.x);
            touch_args[1] = MP_OBJ_NEW_SMALL_INT(tmptouchData.y);
            touch_args[2] = MP_OBJ_NEW_SMALL_INT(tmptouchData.touchevt);      
			while (current != NULL){           
                if (touch_args[2] != EVENT_INVALID){
                    nlr_buf_t nlr;
                    if (nlr_push(&nlr) == 0) {
                        mp_call_function_n_kw(MP_OBJ_FROM_PTR(current->callbackfunc),3,0, touch_args);
                        nlr_pop();
                    } else {
                        mp_obj_print_exception(&mp_plat_print, MP_OBJ_FROM_PTR(nlr.ret_val));
                    }
                }
                current = current->next;
			}			
		}
	}else{
		//do nothing since touch was not enabled
	}
	return mp_const_none;
};
STATIC MP_DEFINE_CONST_FUN_OBJ_0(io_execTouchCallbacks_obj, io_execTouchCallbacks);


// virtual function unregisterTouchCallback
STATIC mp_obj_t io_unregisterTouchCallback(mp_obj_t funct) {
    // TODO: Insert function code
    printf("\n\rFunction not implemented");
    return mp_const_none;
};
STATIC MP_DEFINE_CONST_FUN_OBJ_1(io_unregisterTouchCallback_obj, io_unregisterTouchCallback);

// virtual function enableTouch
STATIC mp_obj_t io_enableTouch() {
    isr_enabled.touch = true;
    //printf("\n\renabled: %i", isr_enabled.touch);
    return mp_const_none;   
};
STATIC MP_DEFINE_CONST_FUN_OBJ_0(io_enableTouch_obj, io_enableTouch);

// virtual function disableTouch
STATIC mp_obj_t io_disableTouch() {
    isr_enabled.touch = false;
    //printf("\n\renabled: %i", isr_enabled.touch);
    return mp_const_none;
};
STATIC MP_DEFINE_CONST_FUN_OBJ_0(io_disableTouch_obj, io_disableTouch);
// END OF VIRTUAL FUNCTIONS

// MODULE GLOBAL TABLE (all classes, all static methods)
STATIC const mp_map_elem_t io_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_io) },
    { MP_OBJ_NEW_QSTR(MP_QSTR_registerTouchCallback), (mp_obj_t)&io_registerTouchCallback_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_unregisterTouchCallback), (mp_obj_t)&io_unregisterTouchCallback_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_enableTouch), (mp_obj_t)&io_enableTouch_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_disableTouch), (mp_obj_t)&io_disableTouch_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_execTouchCallbacks), (mp_obj_t)&io_execTouchCallbacks_obj},
    
};
STATIC MP_DEFINE_CONST_DICT(mp_module_io_globals, io_globals_table);

// ADD MODULE TO MODULE GLOBALS (do not edit)
const mp_obj_module_t mp_module_io = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_io_globals,
};