/*
 File Created by Module and Class Generator for Micropython
 Author: Adam Lang 
 More information about Micropython modules: 
 https://forum.micropython.org/viewtopic.php?t=3274
 http://micropython-dev-docs.readthedocs.io/en/latest/adding-module.html
*/
#if 0
#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
#include "system_definitions.h"

void (*glob_callback)(void) = NULL;


void touch_mpy_irq_handler(void){
    printf("\nmpy_touch_irq\n");
    if (glob_callback != NULL)
        glob_callback();
}

void myTouchFunc(){
    /*
    SYS_TOUCH_OBJ* dObj = (SYS_TOUCH_OBJ*) sysObj.sysTouchObject0;      
    SYS_INPUT_DEVICE_EVENT evt_type = dObj->touchMsg->param0;
    int x = dObj->touchMsg->param1;
    int y = dObj->touchMsg->param2;
    int w = 20;
    if (evt_type == EVENT_PRESS && x+w < 479 && x-w > 1 && y+w < 271 && y-w > 1){
        GFX_Begin();
        int col = rand() % 17;
        GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));
        w = GFX_Mini( GFX_Mini(479-x, x-1), GFX_Mini(271-y,y-1));
        GFX_DrawCircle(x, y, w);  
        GFX_End();
    }
 */
    printf("\n\rTOUCH!");
}


// CLASSES
// class touch
const mp_obj_type_t touch_touchObj_type;
typedef struct _touch_touch_obj_t {
    // base represents some basic information, like type
    mp_obj_base_t base;
    // a member created by us
    int param0;
    int param1;
    int param2;
    mp_obj_t callback;
} touch_touch_obj_t;

mp_obj_t touch_touch_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *args) {
    // this checks the number of arguments
    // on error -> raise python exception
    //mp_arg_check_num(n_args, n_kw, 4, 4, true);
    // create a new object of our C-struct type
    touch_touch_obj_t *self = m_new_obj(touch_touch_obj_t);
    // give it a type
    self->base.type = &touch_touchObj_type;
    self->param0 = 0;
    self->param1 = 0;
    self->param2 = 0;
    self->callback = NULL;
    return MP_OBJ_FROM_PTR(self);
}

STATIC void touch_touch_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind) {
    touch_touch_obj_t *self = MP_OBJ_TO_PTR(self_in);
    printf("\n\rclass: touch, variable: param0, type: int, value: %u", self->param0);
    printf("\n\rclass: touch, variable: param1, type: int, value: %u", self->param1);
    printf("\n\rclass: touch, variable: param2, type: int, value: %u", self->param2);
    printf("\n\rclass: touch, variable: callback, type: mp_obj_t, value: %u", self->callback);
}

// class touch, method setcallback
STATIC mp_obj_t touch_touch_setcallback(mp_obj_t self_in) {
    printf("1glob_callback %x", glob_callback);
    glob_callback = myTouchFunc;
    printf("2glob_callback %x", glob_callback);
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(touch_touch_setcallback_obj, touch_touch_setcallback);

// map class touch methods
STATIC const mp_rom_map_elem_t touch_touch_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR_setcallback), MP_ROM_PTR(&touch_touch_setcallback_obj) },
};
STATIC MP_DEFINE_CONST_DICT(touch_touch_locals_dict, touch_touch_locals_dict_table);

const mp_obj_type_t touch_touchObj_type = {
    { &mp_type_type },
    .name = MP_QSTR_touchObj,
    .print = touch_touch_print,
    .make_new = touch_touch_make_new,
    .locals_dict = (mp_obj_dict_t*)&touch_touch_locals_dict,
};
// end of class touch


// END OF CLASSES


// MODULE GLOBAL TABLE (all classes, all static methods)
STATIC const mp_map_elem_t touch_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_touch) },
    { MP_OBJ_NEW_QSTR(MP_QSTR_touchObj), (mp_obj_t)&touch_touchObj_type },
};
STATIC MP_DEFINE_CONST_DICT(mp_module_touch_globals, touch_globals_table);


// ADD MODULE TO MODULE GLOBALS (do not edit)
const mp_obj_module_t mp_module_touch = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_touch_globals,
};

#endif