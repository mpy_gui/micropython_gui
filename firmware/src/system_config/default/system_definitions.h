/*******************************************************************************
  System Definitions

  File Name:
    system_definitions.h

  Summary:
    MPLAB Harmony project system definitions.

  Description:
    This file contains the system-wide prototypes and definitions for an MPLAB
    Harmony project.
 *******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
//DOM-IGNORE-END
#ifndef _SYS_DEFINITIONS_H
#define _SYS_DEFINITIONS_H

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "system/common/sys_common.h"
#include "system/common/sys_module.h"
#include "driver/i2c/drv_i2c.h"
         
     
 
 
#include "system/devcon/sys_devcon.h"
#include "framework/system/memory/sys_memory.h"
#include "system/clk/sys_clk.h"
#include "peripheral/devcon/plib_devcon.h"
#include "system/int/sys_int.h"
#include "system/dma/sys_dma.h"
#include "system/fs/sys_fs.h"
#include "system/fs/sys_fs_media_manager.h"
#include "system/console/sys_console.h"
#include "system/fs/fat_fs/src/file_system/ff.h"
#include "system/fs/fat_fs/src/file_system/ffconf.h"
#include "system/fs/fat_fs/src/hardware_access/diskio.h"
#include "system/tmr/sys_tmr.h"
#include "driver/tmr/drv_tmr.h"
#include "driver/usart/drv_usart.h"
#include "system/ports/sys_ports.h"
#include "driver/sdhc/drv_sdhc.h"
#include "peripheral/power/plib_power.h"
#include "driver/usb/usbhs/drv_usbhs.h"
#include "usb/usb_device.h"
#include "usb/usb_device_cdc.h"
#include "system/touch/sys_touch.h"
#include "driver/touch/mtch6301/drv_mtch6301.h"
#include "FreeRTOS.h"
#include "task.h"
#include "gfx/hal/gfx.h"
#include "app.h"
#include "hwapp.h"
#include "dlmalloc.h"

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

extern "C" {

#endif
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* System Objects

  Summary:
    Structure holding the system's object handles

  Description:
    This structure contains the object handles for all objects in the
    MPLAB Harmony project's system configuration.

  Remarks:
    These handles are returned from the "Initialize" functions for each module
    and must be passed into the "Tasks" function for each module.
*/
 
typedef struct
{
    SYS_MODULE_OBJ  sysTmr;
    SYS_MODULE_OBJ  sysDma;
    SYS_MODULE_OBJ  drvTmr0;
    SYS_MODULE_OBJ  drvUsart0;
    SYS_MODULE_OBJ  drvI2C0;
    SYS_MODULE_OBJ  drvSDHC;
    SYS_MODULE_OBJ  sysConsole0;
    SYS_MODULE_OBJ  drvUSBObject;
    
    SYS_MODULE_OBJ  usbDevObject0;


    SYS_MODULE_OBJ  sysTouchObject0;
    SYS_MODULE_OBJ  drvMtch6301;

} SYSTEM_OBJECTS;

// *****************************************************************************
// *****************************************************************************
// Section: extern declarations
// *****************************************************************************
// *****************************************************************************

extern SYSTEM_OBJECTS sysObj;


struct AMessage
 {
    char ucMessageID;
    char ucData[ 50 ];
 } xMessage;
QueueHandle_t  xQueue;




#if 0
struct BMessage
{
    uint8_t * cur_framebuffer;
}BMessage_t;
QueueHandle_t xQueueFramebuf;
#endif

// FreeRTOS task handles for task control
TaskHandle_t xReadTouchTaskHandle;
SemaphoreHandle_t xTouchSem;
SemaphoreHandle_t xCBFuncSem;
SemaphoreHandle_t xReplSem;
SemaphoreHandle_t xLogbufSem;

bool logbuf_enabled;
//timer to handle switch bounce effects
uint32_t mpyStartTime;

#if 0
//framebuffer management
#define FRAMEBUFFER_SIZE  522240
#define NUM_OF_FRAMEBUFFERS  64
const uint8_t * ddr2_base = (uint8_t*) 0x88000000; //points to the start of ddr2 sdram in kseg0
uint8_t * framebuffer[NUM_OF_FRAMEBUFFERS];
#endif

//DOM-IGNORE-BEGIN
#ifdef __cplusplus
}
#endif
//DOM-IGNORE-END

#endif /* _SYS_DEFINITIONS_H */
/*******************************************************************************
 End of File
*/

