/*
 File Created by Module and Class Generator for Micropython
 Author: Adam Lang 
 More information about Micropython modules: 
 https://forum.micropython.org/viewtopic.php?t=3274
 http://micropython-dev-docs.readthedocs.io/en/latest/adding-module.html
*/

#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
#include "system_definitions.h"

#include "../micropython_gui.X/lib/mp-readline/readline.h"
#include "py/parse.h"
#include "py/misc.h"
#include "../micropython_gui.X/lib/utils/pyexec.h"
#include "py/lexer.h"
#include "py/compile.h"
#include "logbuffer.h"
#define EXEC_FLAG_PRINT_EOF (1)
#define EXEC_FLAG_ALLOW_DEBUGGING (2)
#define EXEC_FLAG_IS_REPL (4)
#define EXEC_FLAG_SOURCE_IS_RAW_CODE (8)
#define EXEC_FLAG_SOURCE_IS_VSTR (16)
#define EXEC_FLAG_SOURCE_IS_FILENAME (32)
//simple ringbuffer for characters
char replData [32];
static int writePos = 0;
static int available = 0;
static int capacity = 32;

vstr_t * line;

STATIC int parse_compile_execute_2(const void *source, mp_parse_input_kind_t input_kind, int exec_flags) {
    int ret = 0;
    // by default a SystemExit exception returns 0
    pyexec_system_exit = 0;

    nlr_buf_t nlr;
    if (nlr_push(&nlr) == 0) {
        mp_obj_t module_fun;
        {
            mp_lexer_t *lex;
            if (exec_flags & EXEC_FLAG_SOURCE_IS_VSTR) {
                const vstr_t *vstr = source;
                //printf("\n\rBUFFER: %s", vstr->buf);
                lex = mp_lexer_new_from_str_len(MP_QSTR__lt_stdin_gt_, vstr->buf, vstr->len, 0);
            } else if (exec_flags & EXEC_FLAG_SOURCE_IS_FILENAME) {
                lex = mp_lexer_new_from_file(source);
            } else {
                lex = (mp_lexer_t*)source;
            }
            // source is a lexer, parse and compile the script
            qstr source_name = lex->source_name;
            mp_parse_tree_t parse_tree = mp_parse(lex, input_kind);
            module_fun = mp_compile(&parse_tree, source_name, MP_EMIT_OPT_NONE, exec_flags & EXEC_FLAG_IS_REPL);
        }
        // execute code
        mp_call_function_0(module_fun);
        nlr_pop();
        ret = 1;
    } else {
        // uncaught exception
        // FIXME it could be that an interrupt happens just before we disable it here
        // check for SystemExit
        if (mp_obj_is_subclass_fast(mp_obj_get_type((mp_obj_t)nlr.ret_val), &mp_type_SystemExit)) {
            // at the moment, the value of SystemExit is unused
            ret = pyexec_system_exit;
        } else {
            mp_obj_print_exception(&mp_plat_print, (mp_obj_t)nlr.ret_val);
            ret = 0;
        }
    }
    return ret;
}

int resetBuf()
{
    if ( xSemaphoreTake( xReplSem, ( TickType_t )1 ) == pdTRUE ){
        memset(replData, '\0', 32);
        writePos = 0;
        available = 0;
        xSemaphoreGive(xReplSem);
        return 0;
    }else{
        //semaphore not obtained
        return -1;
    }
}


// VIRTUAL FUNCTIONS
// virtual function putc
mp_obj_t demo_putc(mp_obj_t ch) {
    int val = MP_OBJ_SMALL_INT_VALUE(ch);
    int ret = -1;
    readline_process_char((char)val);
    /*
    if ( xSemaphoreTake( xReplSem, ( TickType_t )1 ) == pdTRUE ){
        if(available < capacity){
            if(writePos >= capacity){
                writePos = 0;
            }
            replData[writePos] = val;
            
            writePos++;
            available++;
            ret = 0;
        }
        xSemaphoreGive(xReplSem);
    }else{
        //semaphore not obtained
        //printf("=");
    }*/
    return mp_obj_new_int(ret);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(demo_putc_obj, demo_putc);


// virtual function getc
STATIC mp_obj_t demo_getc() {
    char val = logbuf_getc();
    
    /*
    if ((val = logbuf_getc()) <0){
    
            if ( xSemaphoreTake( xReplSem, ( TickType_t )1 ) == pdTRUE ){
                if(available != 0){
                    int nextSlot = writePos - available;
                        if(nextSlot < 0){
                            nextSlot += capacity;
                        }
                        val = replData[nextSlot];
                        available--;
                }
                    //now release Semaphore to give access to shared ringbuffer
                    xSemaphoreGive(xReplSem);
                }else{
                    //semaphore not obtained
                    //printf("_");
                }
    }
     */
    return MP_OBJ_NEW_SMALL_INT(val);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(demo_getc_obj, demo_getc);

// virtual function init
STATIC mp_obj_t demo_init() {
    logbuf_enabled = true;
    free(line);
    //resetBuf();    
    line = malloc(sizeof(vstr_t));
    line->alloc = 32;
    line->len = 0;
    line->buf = m_new(char, line->alloc);
    line->fixed_buf = false;
    readline_init(line, ">>> ");
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(demo_init_obj, demo_init);

STATIC mp_obj_t demo_delc() {
    readline_process_char(8);
    //execute demo_getc() four times to clear "\x1b[K" from logbuffer
    demo_getc();
    demo_getc();
    demo_getc();
    demo_getc();
    //test
//    printf("TEST:");
//    char ch;
//    while ((ch=logbuf_getc()) >= 0){
//        printf("%c",ch);
//    }
    
    
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(demo_delc_obj, demo_delc);

// virtual function parse_compile_execute
STATIC mp_obj_t demo_parse_compile_execute() {
    mp_parse_input_kind_t parse_input_kind = MP_PARSE_SINGLE_INPUT;
    int ret = 0;
    readline_process_char(10);
    readline_process_char(13);
    ret = parse_compile_execute_2(line, parse_input_kind, EXEC_FLAG_ALLOW_DEBUGGING | EXEC_FLAG_IS_REPL | EXEC_FLAG_SOURCE_IS_VSTR);
    if (ret)
        BSP_LEDToggle(BSP_LED_D7);
    demo_init();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(demo_parse_compile_execute_obj, demo_parse_compile_execute);


// virtual function led_on
STATIC mp_obj_t demo_led_on(mp_obj_t lednum) {
    BSP_LEDOn(MP_OBJ_SMALL_INT_VALUE(lednum));
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(demo_led_on_obj, demo_led_on);

// virtual function led_off
STATIC mp_obj_t demo_led_off(mp_obj_t lednum) {
    BSP_LEDOff(MP_OBJ_SMALL_INT_VALUE(lednum));
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(demo_led_off_obj, demo_led_off);
// END OF VIRTUAL FUNCTIONS

// MODULE GLOBAL TABLE (all classes, all static methods)
STATIC const mp_map_elem_t demo_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_demo) },
    { MP_OBJ_NEW_QSTR(MP_QSTR_putc), (mp_obj_t)&demo_putc_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_led_on), (mp_obj_t)&demo_led_on_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_led_off), (mp_obj_t)&demo_led_off_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_getc), (mp_obj_t)&demo_getc_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_init), (mp_obj_t)&demo_init_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_parse_compile_execute), (mp_obj_t)&demo_parse_compile_execute_obj},
    { MP_OBJ_NEW_QSTR(MP_QSTR_delc), (mp_obj_t)&demo_delc_obj},
    
};
STATIC MP_DEFINE_CONST_DICT(mp_module_demo_globals, demo_globals_table);


// ADD MODULE TO MODULE GLOBALS (do not edit)
const mp_obj_module_t mp_module_demo = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_demo_globals,
};
