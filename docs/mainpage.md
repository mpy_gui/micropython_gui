# Welcome to the documentation of the MICROPYTHON GUI (C-code) Project

Generated documentation of micropyhton_gui powered by doxygen.

micropython_gui is a project to enable easy GUI programming with the power of Python. micropyhton_gui uses the PIC32 port of MicroPython.

For running this project following hardware is needed:

* DM320010 - PIC32MZ Embedded Graphics with Stacked DRAM (DA) Starter Kit. [info](https://www.microchip.com/DevelopmentTools/ProductDetails/DM320010).
* PIC32MZ2064DAx169 Daughter Card with PIC32MZ2064DAG169 MCU
* MEB II (Multimedia Expansion Board II). [info] (http://ww1.microchip.com/downloads/en/DeviceDoc/70005148B.pdf)
* 4.3" WQVGA Display Board with PCAP touch(Daughter Board) (included in the MEB II Kit)
* MicroSD card
* USB Cable for programming and Power Supply
* USB Cable for serial console


To prepare the hardware for easy GUI programming with Python, load the project into MPLABX, compile the project and program the hardware with an USB cable.

The project "demo_system" contains the necessary Python libraries and a demo-GUI to get started with GUI programing. 
The files of the project "demo_system" which are written in Python, have to be copied to the MicroSD card. 

To start the GUI, press SW1 on the Starter Kit.

Contact: